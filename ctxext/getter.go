package ctxext

import "context"

func Value[T any](ctx context.Context, key any) (T, bool) {
	v := ctx.Value(key)
	if v == nil {
		return *new(T), false
	}
	if tv, ok := v.(T); !ok {
		return *new(T), false
	} else {
		return tv, true
	}
}

func ValueOrDefault[T any](ctx context.Context, key any, def T) T {
	v := ctx.Value(key)
	if v == nil {
		return def
	}
	if tv, ok := v.(T); !ok {
		return def
	} else {
		return tv
	}
}
