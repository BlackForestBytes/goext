package mongoext

import (
	"go.mongodb.org/mongo-driver/bson"
	"reflect"
	"strings"
)

// ProjectionFromStruct automatically generated a mongodb projection for a struct
// This way you can pretty much always write
// `options.FindOne().SetProjection(mongoutils.ProjectionFromStruct(...your_model...))`
// to only get the data from mongodb that you will actually use in the later decode step
func ProjectionFromStruct(obj interface{}) bson.M {
	v := reflect.ValueOf(obj)
	t := v.Type()

	result := bson.M{}

	for i := 0; i < v.NumField(); i++ {
		tag := t.Field(i).Tag.Get("bson")
		if tag == "" {
			continue
		}
		tag = strings.Split(tag, ",")[0]

		result[tag] = 1
	}

	return result
}
