
run:
	echo "This is a library - can't be run" && false

test:
	# go test ./...
	which gotestsum || go install gotest.tools/gotestsum@latest
	gotestsum --format "testname" -- -tags="timetzdata sqlite_fts5 sqlite_foreign_keys" "./..."

test-in-docker:
	tag="goext_temp_test_image:$(shell uuidgen | tr -d '-')";        \
	docker build --tag $$tag . -f .gitea/workflows/Dockerfile_tests; \
	docker run --rm $$tag;                                           \
	docker rmi $$tag

version:
	_data/version.sh