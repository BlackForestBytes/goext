package exerr

import (
	"errors"
	"gogs.mikescher.com/BlackForestBytes/goext/langext"
	"gogs.mikescher.com/BlackForestBytes/goext/tst"
	"os"
	"testing"
)

func TestMain(m *testing.M) {
	if !Initialized() {
		Init(ErrorPackageConfigInit{ZeroLogErrTraces: langext.PFalse, ZeroLogAllTraces: langext.PFalse})
	}
	os.Exit(m.Run())
}

type golangErr struct {
	Message string
}

func (g golangErr) Error() string {
	return g.Message
}

type golangErr2 struct {
	Message string
}

func (g golangErr2) Error() string {
	return g.Message
}

type simpleError struct {
}

func (g simpleError) Error() string {
	return "Something simple went wroong"
}

type simpleError2 struct {
}

func (g simpleError2) Error() string {
	return "Something simple went wroong"
}

func TestExErrIs1(t *testing.T) {
	e0 := simpleError{}

	wrap := Wrap(e0, "something went wrong").Str("test", "123").Build()

	tst.AssertTrue(t, errors.Is(wrap, simpleError{}))
	tst.AssertFalse(t, errors.Is(wrap, golangErr{}))
	tst.AssertFalse(t, errors.Is(wrap, golangErr{"error1"}))
}

func TestExErrIs2(t *testing.T) {
	e0 := golangErr{"error1"}

	wrap := Wrap(e0, "something went wrong").Str("test", "123").Build()

	tst.AssertTrue(t, errors.Is(wrap, e0))
	tst.AssertTrue(t, errors.Is(wrap, golangErr{"error1"}))
	tst.AssertFalse(t, errors.Is(wrap, golangErr{"error2"}))
	tst.AssertFalse(t, errors.Is(wrap, simpleError{}))
}

func TestExErrAs(t *testing.T) {

	e0 := golangErr{"error1"}

	w0 := Wrap(e0, "something went wrong").Str("test", "123").Build()

	{
		out := golangErr{}
		ok := errors.As(w0, &out)
		tst.AssertTrue(t, ok)
		tst.AssertEqual(t, out.Message, "error1")
	}

	w1 := Wrap(w0, "outher error").Build()

	{
		out := golangErr{}
		ok := errors.As(w1, &out)
		tst.AssertTrue(t, ok)
		tst.AssertEqual(t, out.Message, "error1")
	}

	{
		out := golangErr2{}
		ok := errors.As(w1, &out)
		tst.AssertFalse(t, ok)
	}

	{
		out := simpleError2{}
		ok := errors.As(w1, &out)
		tst.AssertFalse(t, ok)
	}
}
