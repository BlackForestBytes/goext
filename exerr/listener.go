package exerr

import (
	"sync"
)

type ListenerOpt struct {
	NoLog bool
}

type Listener = func(method Method, v *ExErr, opt ListenerOpt)

var listenerLock = sync.Mutex{}
var listener = make([]Listener, 0)

func RegisterListener(l Listener) {
	listenerLock.Lock()
	defer listenerLock.Unlock()

	listener = append(listener, l)
}

func (ee *ExErr) CallListener(m Method, opt ListenerOpt) {
	listenerLock.Lock()
	defer listenerLock.Unlock()

	for _, v := range listener {
		v(m, ee, opt)
	}
}
