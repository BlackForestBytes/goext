package exerr

type Proxy struct {
	v ExErr
}

func (p *Proxy) UniqueID() string {
	return p.v.UniqueID
}

func (p *Proxy) Get() ExErr {
	return p.v
}
