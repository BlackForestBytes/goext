package exerr

type Method string

const (
	MethodOutput Method = "OUTPUT"
	MethodPrint  Method = "PRINT"
	MethodBuild  Method = "BUILD"
	MethodFatal  Method = "FATAL"
)

type LogPrintLevel string

const (
	LogPrintFull     LogPrintLevel = "Full"
	LogPrintOverview LogPrintLevel = "Overview"
	LogPrintShort    LogPrintLevel = "Short"
)
