package wmo

import (
	"context"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"gogs.mikescher.com/BlackForestBytes/goext/exerr"
	"gogs.mikescher.com/BlackForestBytes/goext/langext"
)

func (c *Coll[TData]) Aggregate(ctx context.Context, pipeline mongo.Pipeline, opts ...*options.AggregateOptions) ([]TData, error) {

	for _, ppl := range c.extraModPipeline {
		pipeline = langext.ArrConcat(pipeline, ppl(ctx))
	}

	cursor, err := c.coll.Aggregate(ctx, pipeline, opts...)
	if err != nil {
		return nil, exerr.Wrap(err, "mongo-aggregation failed").Any("pipeline", pipeline).Any("options", opts).Str("collection", c.Name()).Build()
	}

	defer func() { _ = cursor.Close(ctx) }()

	res, err := c.decodeAll(ctx, cursor)
	if err != nil {
		return nil, exerr.Wrap(err, "failed to decode values").Build()
	}

	return res, nil
}

func (c *Coll[TData]) AggregateOneOpt(ctx context.Context, pipeline mongo.Pipeline, opts ...*options.AggregateOptions) (*TData, error) {

	for _, ppl := range c.extraModPipeline {
		pipeline = langext.ArrConcat(pipeline, ppl(ctx))
	}

	cursor, err := c.coll.Aggregate(ctx, pipeline, opts...)
	if err != nil {
		return nil, exerr.Wrap(err, "mongo-aggregation failed").Any("pipeline", pipeline).Any("options", opts).Str("collection", c.Name()).Build()
	}

	defer func() { _ = cursor.Close(ctx) }()

	if cursor.Next(ctx) {
		v, err := c.decodeSingle(ctx, cursor)
		if err != nil {
			return nil, exerr.Wrap(err, "failed to decode single value").Build()
		}
		return &v, nil
	}

	return nil, nil
}

func (c *Coll[TData]) AggregateOne(ctx context.Context, pipeline mongo.Pipeline, opts ...*options.AggregateOptions) (TData, error) {

	for _, ppl := range c.extraModPipeline {
		pipeline = langext.ArrConcat(pipeline, ppl(ctx))
	}

	cursor, err := c.coll.Aggregate(ctx, pipeline, opts...)
	if err != nil {
		return *new(TData), exerr.Wrap(err, "mongo-aggregation failed").Any("pipeline", pipeline).Any("options", opts).Str("collection", c.Name()).Build()
	}

	defer func() { _ = cursor.Close(ctx) }()

	if cursor.Next(ctx) {
		v, err := c.decodeSingle(ctx, cursor)
		if err != nil {
			return *new(TData), exerr.Wrap(err, "failed to decode single value").Build()
		}
		return v, nil
	}

	return *new(TData), exerr.Wrap(mongo.ErrNoDocuments, "no document in result").Build()
}
