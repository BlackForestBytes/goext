package wmo

import (
	"context"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"gogs.mikescher.com/BlackForestBytes/goext/exerr"
	"gogs.mikescher.com/BlackForestBytes/goext/langext"
)

func (c *Coll[TData]) InsertOne(ctx context.Context, valueIn TData) (TData, error) {
	for _, hook := range c.marshalHooks {
		valueIn = hook(valueIn)
	}

	insRes, err := c.coll.InsertOne(ctx, valueIn)
	if err != nil {
		return *new(TData), exerr.Wrap(err, "mongo-query[insert-one] failed").Str("collection", c.Name()).Build()
	}

	r, err := c.findOneInternal(ctx, bson.M{"_id": insRes.InsertedID}, false)
	if err != nil {
		return *new(TData), exerr.Wrap(err, "mongo-query[insert-one] failed").Str("collection", c.Name()).Build()
	}
	return *r, nil
}

// InsertOneUnchecked behaves the same as InsertOne, but allows arbitrary data to be inserted (valueIn is any instead of TData)
func (c *Coll[TData]) InsertOneUnchecked(ctx context.Context, valueIn any) (TData, error) {
	insRes, err := c.coll.InsertOne(ctx, valueIn)
	if err != nil {
		return *new(TData), exerr.Wrap(err, "mongo-query[insert-one] failed").Str("collection", c.Name()).Build()
	}

	r, err := c.findOneInternal(ctx, bson.M{"_id": insRes.InsertedID}, false)
	if err != nil {
		return *new(TData), exerr.Wrap(err, "mongo-query[insert-one] failed").Str("collection", c.Name()).Build()
	}
	return *r, nil
}

func (c *Coll[TData]) InsertMany(ctx context.Context, valueIn []TData) (*mongo.InsertManyResult, error) {
	for _, hook := range c.marshalHooks {
		for i := 0; i < len(valueIn); i++ {
			valueIn[i] = hook(valueIn[i])
		}
	}

	insRes, err := c.coll.InsertMany(ctx, langext.ArrayToInterface(valueIn))
	if err != nil {
		return nil, exerr.Wrap(err, "mongo-query[insert-many] failed").Int("len(valueIn)", len(valueIn)).Str("collection", c.Name()).Build()
	}

	return insRes, nil
}

// InsertManyUnchecked behaves the same as InsertOne, but allows arbitrary data to be inserted (valueIn is []any instead of []TData)
func (c *Coll[TData]) InsertManyUnchecked(ctx context.Context, valueIn []any) (*mongo.InsertManyResult, error) {
	insRes, err := c.coll.InsertMany(ctx, langext.ArrayToInterface(valueIn))
	if err != nil {
		return nil, exerr.Wrap(err, "mongo-query[insert-many] failed").Int("len(valueIn)", len(valueIn)).Str("collection", c.Name()).Build()
	}

	return insRes, nil
}
