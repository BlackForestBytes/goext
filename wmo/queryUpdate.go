package wmo

import (
	"context"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"gogs.mikescher.com/BlackForestBytes/goext/exerr"
)

func (c *Coll[TData]) FindOneAndUpdate(ctx context.Context, filterQuery bson.M, updateQuery bson.M) (TData, error) {
	mongoRes := c.coll.FindOneAndUpdate(ctx, filterQuery, updateQuery, options.FindOneAndUpdate().SetReturnDocument(options.After))
	if err := mongoRes.Err(); err != nil {
		return *new(TData), exerr.Wrap(err, "mongo-query[find-one-and-update] failed").
			Str("collection", c.Name()).
			Any("filterQuery", filterQuery).
			Any("updateQuery", updateQuery).
			Build()
	}

	return c.decodeSingleOrRequery(ctx, mongoRes)
}

func (c *Coll[TData]) UpdateOne(ctx context.Context, filterQuery bson.M, updateQuery bson.M) error {
	_, err := c.coll.UpdateOne(ctx, filterQuery, updateQuery)
	if err != nil {
		return exerr.Wrap(err, "mongo-query[update-one] failed").
			Str("collection", c.Name()).
			Any("filterQuery", filterQuery).
			Any("updateQuery", updateQuery).
			Build()
	}

	return nil
}

func (c *Coll[TData]) UpdateOneByID(ctx context.Context, id EntityID, updateQuery bson.M) error {
	_, err := c.coll.UpdateOne(ctx, bson.M{"_id": id}, updateQuery)
	if err != nil {
		return exerr.Wrap(err, "mongo-query[update-one-by-id] failed").
			Str("collection", c.Name()).
			Id("id", id).
			Any("updateQuery", updateQuery).
			Build()
	}

	return nil
}

func (c *Coll[TData]) UpdateMany(ctx context.Context, filterQuery bson.M, updateQuery bson.M) (*mongo.UpdateResult, error) {
	res, err := c.coll.UpdateMany(ctx, filterQuery, updateQuery)
	if err != nil {
		return nil, exerr.Wrap(err, "mongo-query[update-many] failed").
			Str("collection", c.Name()).
			Any("filterQuery", filterQuery).
			Any("updateQuery", updateQuery).
			Build()
	}

	return res, nil
}

func (c *Coll[TData]) ReplaceOne(ctx context.Context, filterQuery bson.M, value TData) error {
	for _, hook := range c.marshalHooks {
		value = hook(value)
	}

	_, err := c.coll.UpdateOne(ctx, filterQuery, bson.M{"$set": value})
	if err != nil {
		return exerr.Wrap(err, "mongo-query[replace-one] failed").
			Str("collection", c.Name()).
			Any("filterQuery", filterQuery).
			Build()
	}

	return nil
}

func (c *Coll[TData]) FindOneAndReplace(ctx context.Context, filterQuery bson.M, value TData) (TData, error) {
	for _, hook := range c.marshalHooks {
		value = hook(value)
	}

	mongoRes := c.coll.FindOneAndReplace(ctx, filterQuery, value, options.FindOneAndReplace().SetReturnDocument(options.After))
	if err := mongoRes.Err(); err != nil {
		return *new(TData), exerr.Wrap(err, "mongo-query[find-one-and-update] failed").
			Str("collection", c.Name()).
			Any("filterQuery", filterQuery).
			Build()
	}

	return c.decodeSingleOrRequery(ctx, mongoRes)
}
