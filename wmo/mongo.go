package wmo

import "go.mongodb.org/mongo-driver/mongo"

func W[TData any](collection *mongo.Collection) *Coll[TData] {
	c := Coll[TData]{coll: collection}

	c.init()

	return &c
}
