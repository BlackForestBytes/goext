package wmo

import (
	"context"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"gogs.mikescher.com/BlackForestBytes/goext/exerr"
)

func (c *Coll[TData]) DeleteOneByID(ctx context.Context, id EntityID) error {
	_, err := c.coll.DeleteOne(ctx, bson.M{"_id": id})
	if err != nil {
		return exerr.Wrap(err, "mongo-query[delete-one-by-id] failed").Id("id", id).Str("collection", c.Name()).Build()
	}

	return nil
}

func (c *Coll[TData]) DeleteOne(ctx context.Context, filterQuery bson.M) error {
	_, err := c.coll.DeleteOne(ctx, filterQuery)
	if err != nil {
		return exerr.Wrap(err, "mongo-query[delete-one] failed").Any("filterQuery", filterQuery).Str("collection", c.Name()).Build()
	}

	return nil
}

func (c *Coll[TData]) DeleteMany(ctx context.Context, filterQuery bson.M) (*mongo.DeleteResult, error) {
	res, err := c.coll.DeleteMany(ctx, filterQuery)
	if err != nil {
		return nil, exerr.Wrap(err, "mongo-query[delete-many] failed").Any("filterQuery", filterQuery).Str("collection", c.Name()).Build()
	}

	return res, nil
}
