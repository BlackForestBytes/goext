#!/bin/bash

set -o nounset   # disallow usage of unset vars  ( set -u )
set -o errexit   # Exit immediately if a pipeline returns non-zero.  ( set -e )
set -o errtrace  # Allow the above trap be inherited by all functions in the script.  ( set -E )
set -o pipefail  # Return value of a pipeline is the value of the last (rightmost) command to exit with a non-zero status
IFS=$'\n\t'      # Set $IFS to only newline and tab.


function black() { echo -e "\x1B[30m $1 \x1B[0m"; }
function red()   { echo -e "\x1B[31m $1 \x1B[0m"; }
function green() { echo -e "\x1B[32m $1 \x1B[0m"; }
function yellow(){ echo -e "\x1B[33m $1 \x1B[0m"; }
function blue()  { echo -e "\x1B[34m $1 \x1B[0m"; }
function purple(){ echo -e "\x1B[35m $1 \x1B[0m"; }
function cyan()  { echo -e "\x1B[36m $1 \x1B[0m"; }
function white() { echo -e "\x1B[37m $1 \x1B[0m"; }

if [ "$( git rev-parse --abbrev-ref HEAD )" != "master" ]; then
  >&2 red "[ERROR] Can only create versions of <master>"
  exit 1
fi

echo ""
echo -n "Insert optional commit message: "
read commitMessage
echo ""

git pull --ff

go get -u ./...

curr_vers=$(git describe --tags --abbrev=0 | sed 's/v//g')

next_ver=$(echo "$curr_vers" | awk -F. -v OFS=. 'NF==1{print ++$NF}; NF>1{if(length($NF+1)>length($NF))$(NF-1)++; $NF=sprintf("%0*d", length($NF), ($NF+1)%(10^length($NF))); print}')

echo ""
echo "> Current Version: ${curr_vers}"
echo "> Next    Version: ${next_ver}"
echo ""

printf "package goext\n\nconst GoextVersion = \"%s\"\n\nconst GoextVersionTimestamp = \"%s\"\n" "${next_ver}" "$( date +"%Y-%m-%dT%H:%M:%S%z" )" > "goextVersion.go"

git add --verbose .

msg="v${next_ver}"

if [[ "$commitMessage" != "" ]]; then
  msg="${msg} ${commitMessage}"
fi


if [ $# -gt 0 ]; then
  msg="$1"
fi

git commit -a -m "${msg}"

git tag "v${next_ver}"

git push
git push --tags

