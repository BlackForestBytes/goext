package timeext

import (
	"testing"
	"time"
)

func TestTimeToDayStart(t *testing.T) {
	tz := TimezoneBerlin
	tm := time.Date(2022, 1, 1, 13, 14, 15, 0, tz)
	expected := time.Date(2022, 1, 1, 0, 0, 0, 0, tz)
	result := TimeToDayStart(tm, tz)
	if !result.Equal(expected) {
		t.Errorf("Expected %v but got %v", expected, result)
	}
}

func TestTimeToDayEnd(t *testing.T) {
	tz := TimezoneBerlin
	tm := time.Date(2022, 1, 1, 13, 14, 15, 0, tz)
	expected := time.Date(2022, 1, 2, 0, 0, 0, 0, tz).Add(-1)
	result := TimeToDayEnd(tm, tz)
	if !result.Equal(expected) {
		t.Errorf("Expected %v but got %v", expected, result)
	}
}

func TestIsSameDayIncludingDateBoundaries(t *testing.T) {
	tz := TimezoneBerlin
	t1 := time.Date(2022, 1, 1, 23, 59, 59, 0, tz)
	t2 := time.Date(2022, 1, 2, 0, 0, 0, 0, tz)
	if !IsSameDayIncludingDateBoundaries(t1, t2, tz) {
		t.Errorf("Expected %v and %v to be the same day", t1, t2)
	}
}

func TestIsDatePartEqual(t *testing.T) {
	tz := TimezoneBerlin
	t1 := time.Date(2022, 1, 1, 23, 59, 59, 0, tz)
	t2 := time.Date(2022, 1, 1, 0, 0, 0, 0, tz)
	if !IsDatePartEqual(t1, t2, tz) {
		t.Errorf("Expected %v and %v to have the same date part", t1, t2)
	}
}

func TestWithTimePart(t *testing.T) {
	tz := TimezoneBerlin
	base := time.Date(2022, 1, 1, 0, 0, 0, 0, tz)
	expected := time.Date(2022, 1, 1, 13, 14, 15, 0, tz)
	result := WithTimePart(base, 13, 14, 15)
	if !result.Equal(expected) {
		t.Errorf("Expected %v but got %v", expected, result)
	}
}

func TestCombineDateAndTime(t *testing.T) {
	tz := TimezoneBerlin
	d := time.Date(2022, 1, 1, 0, 0, 0, 0, tz)
	tm := time.Date(0, 0, 0, 13, 14, 15, 0, tz)
	expected := time.Date(2022, 1, 1, 13, 14, 15, 0, tz)
	result := CombineDateAndTime(d, tm)
	if !result.Equal(expected) {
		t.Errorf("Expected %v but got %v", expected, result)
	}
}

func TestIsSunday(t *testing.T) {
	tz := TimezoneBerlin
	tm := time.Date(2022, 1, 2, 0, 0, 0, 0, tz) // 2nd January 2022 is a Sunday
	if !IsSunday(tm, tz) {
		t.Errorf("Expected %v to be a Sunday", tm)
	}
}

func TestIsSunday_OnSunday(t *testing.T) {
	sunday := time.Date(2022, 5, 15, 0, 0, 0, 0, TimezoneBerlin) // A Sunday
	if !IsSunday(sunday, TimezoneBerlin) {
		t.Errorf("Expected true for Sunday")
	}
}

func TestDurationFromTime(t *testing.T) {
	expected := time.Duration(13*time.Hour + 14*time.Minute + 15*time.Second)
	result := DurationFromTime(13, 14, 15)
	if result != expected {
		t.Errorf("Expected %v but got %v", expected, result)
	}
}

func TestMin(t *testing.T) {
	t1 := time.Date(2022, 1, 1, 0, 0, 0, 0, time.UTC)
	t2 := time.Date(2022, 1, 2, 0, 0, 0, 0, time.UTC)
	expected := t1
	result := Min(t1, t2)
	if !result.Equal(expected) {
		t.Errorf("Expected %v but got %v", expected, result)
	}
}

func TestMax(t *testing.T) {
	t1 := time.Date(2022, 1, 1, 0, 0, 0, 0, time.UTC)
	t2 := time.Date(2022, 1, 2, 0, 0, 0, 0, time.UTC)
	expected := t2
	result := Max(t1, t2)
	if !result.Equal(expected) {
		t.Errorf("Expected %v but got %v", expected, result)
	}
}

func TestUnixFloatSeconds(t *testing.T) {
	v := 1640995200.0 // 1st January 2022 00:00:00 UTC in Unix timestamp
	expected := time.Date(2022, 1, 1, 0, 0, 0, 0, time.UTC)
	result := UnixFloatSeconds(v)
	if !result.Equal(expected) {
		t.Errorf("Expected %v but got %v", expected, result)
	}
}

func TestFloorTime(t *testing.T) {
	tm := time.Date(2022, 1, 1, 13, 14, 15, 0, time.UTC)
	expected := time.Date(2022, 1, 1, 0, 0, 0, 0, time.UTC)
	result := FloorTime(tm)
	if !result.Equal(expected) {
		t.Errorf("Expected %v but got %v", expected, result)
	}
}

func TestSubtractYears(t *testing.T) {
	tz := TimezoneBerlin
	tm := time.Date(2022, 1, 1, 0, 0, 0, 0, tz)

	expected := time.Date(2021, 1, 1, 0, 0, 0, 0, tz)
	result := SubtractYears(tm, 1, tz)
	if !result.Equal(expected) {
		t.Errorf("Expected %v but got %v", expected, result)
	}

	expected = time.Date(2020, 1, 1, 0, 0, 0, 0, tz)
	result = SubtractYears(tm, 2, tz)
	if !result.Equal(expected) {
		t.Errorf("Expected %v but got %v", expected, result)
	}

	expected = time.Date(2019, 1, 1, 0, 0, 0, 0, tz)
	result = SubtractYears(tm, 3, tz)
	if !result.Equal(expected) {
		t.Errorf("Expected %v but got %v", expected, result)
	}

	expected = time.Date(2025, 1, 1, 0, 0, 0, 0, tz)
	result = SubtractYears(tm, -3, tz)
	if !result.Equal(expected) {
		t.Errorf("Expected %v but got %v", expected, result)
	}
}

func TestAddYears(t *testing.T) {
	tz := TimezoneBerlin
	tm := time.Date(2022, 1, 1, 0, 0, 0, 0, tz)
	expected := time.Date(2023, 1, 1, 0, 0, 0, 0, tz)
	result := AddYears(tm, 1, tz)
	if !result.Equal(expected) {
		t.Errorf("Expected %v but got %v", expected, result)
	}
}

func TestIsDatePartEqual_SameDateDifferentTimes(t *testing.T) {
	tz := time.UTC
	t1 := time.Date(2022, 5, 18, 10, 30, 0, 0, tz)
	t2 := time.Date(2022, 5, 18, 20, 45, 0, 0, tz)
	if !IsDatePartEqual(t1, t2, tz) {
		t.Errorf("Expected dates to be equal")
	}
}

func TestWithTimePart_ChangeTime(t *testing.T) {
	base := time.Date(2022, 5, 18, 0, 0, 0, 0, time.UTC)
	result := WithTimePart(base, 15, 30, 45)
	expected := time.Date(2022, 5, 18, 15, 30, 45, 0, time.UTC)
	if !result.Equal(expected) {
		t.Errorf("Expected %v, got %v", expected, result)
	}
}

func TestCombineDateAndTime_CombineDifferentParts(t *testing.T) {
	date := time.Date(2022, 5, 18, 0, 0, 0, 0, time.UTC)
	timePart := time.Date(2000, 1, 1, 15, 30, 45, 0, time.UTC)
	result := CombineDateAndTime(date, timePart)
	expected := time.Date(2022, 5, 18, 15, 30, 45, 0, time.UTC)
	if !result.Equal(expected) {
		t.Errorf("Expected %v, got %v", expected, result)
	}
}

func TestDaysInMonth_31Days(t *testing.T) {
	date := time.Date(2022, 1, 1, 0, 0, 0, 0, time.UTC) // January
	expected := 31
	result := DaysInMonth(date)
	if result != expected {
		t.Errorf("Expected %d but got %d", expected, result)
	}
}

func TestDaysInMonth_30Days(t *testing.T) {
	date := time.Date(2022, 4, 1, 0, 0, 0, 0, time.UTC) // April
	expected := 30
	result := DaysInMonth(date)
	if result != expected {
		t.Errorf("Expected %d but got %d", expected, result)
	}
}

func TestDaysInMonth_FebruaryLeapYear(t *testing.T) {
	date := time.Date(2020, 2, 1, 0, 0, 0, 0, time.UTC) // February in a leap year
	expected := 29
	result := DaysInMonth(date)
	if result != expected {
		t.Errorf("Expected %d but got %d", expected, result)
	}
}

func TestDaysInMonth_FebruaryNonLeapYear(t *testing.T) {
	date := time.Date(2021, 2, 1, 0, 0, 0, 0, time.UTC) // February in a non-leap year
	expected := 28
	result := DaysInMonth(date)
	if result != expected {
		t.Errorf("Expected %d but got %d", expected, result)
	}
}
