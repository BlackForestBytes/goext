package timeext

import "time"

func WeekStart(year, week int) time.Time {

	// https://stackoverflow.com/a/52303730/1761622

	// Start from the middle of the year:
	t := time.Date(year, 7, 1, 0, 0, 0, 0, time.UTC)

	// Roll back to Monday:
	if wd := t.Weekday(); wd == time.Sunday {
		t = t.AddDate(0, 0, -6)
	} else {
		t = t.AddDate(0, 0, -int(wd)+1)
	}

	// Difference in weeks:
	_, w := t.ISOWeek()
	t = t.AddDate(0, 0, (week-w)*7)

	return t
}

func WeekEnd(year, week int) time.Time {
	return WeekStart(year, week).AddDate(0, 0, 7).Add(time.Duration(-1))
}
