package timeext

import (
	"strconv"
	"time"
)

var longDayNames = []string{
	"Sonntag",
	"Montag",
	"Dienstag",
	"Mittwoch", // meine Kerle
	"Donnerstag",
	"Freitag",
	"Samstag",
}

func WeekdayNameGerman(d time.Weekday) string {
	if time.Sunday <= d && d <= time.Saturday {
		return longDayNames[d]
	}
	return "%!Weekday(" + strconv.Itoa(int(d)) + ")"
}
