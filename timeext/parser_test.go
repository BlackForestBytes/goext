package timeext

import (
	"testing"
	"time"
)

func TestParseDurationShortString(t *testing.T) {

	assertPDSSEqual(t, FromSeconds(1), "1s")
	assertPDSSEqual(t, FromSeconds(1), "1sec")
	assertPDSSEqual(t, FromSeconds(1), "1second")
	assertPDSSEqual(t, FromSeconds(1), "1seconds")
	assertPDSSEqual(t, FromSeconds(100), "100second")
	assertPDSSEqual(t, FromSeconds(100), "100seconds")
	assertPDSSEqual(t, FromSeconds(1883639.77), "1883639.77second")
	assertPDSSEqual(t, FromSeconds(1883639.77), "1883639.77seconds")
	assertPDSSEqual(t, FromSeconds(50), "50s")
	assertPDSSEqual(t, FromSeconds(50), "50sec")
	assertPDSSEqual(t, FromSeconds(1), "1second")
	assertPDSSEqual(t, FromSeconds(50), "50seconds")

	assertPDSSEqual(t, FromMinutes(10), "10m")
	assertPDSSEqual(t, FromMinutes(10), "10min")
	assertPDSSEqual(t, FromMinutes(1), "1minute")
	assertPDSSEqual(t, FromMinutes(10), "10minutes")
	assertPDSSEqual(t, FromMinutes(10.5), "10.5minutes")

	assertPDSSEqual(t, FromMilliseconds(100), "100ms")
	assertPDSSEqual(t, FromMilliseconds(100), "100milliseconds")
	assertPDSSEqual(t, FromMilliseconds(100), "100millisecond")

	assertPDSSEqual(t, FromNanoseconds(99235), "99235ns")
	assertPDSSEqual(t, FromNanoseconds(99235), "99235nanoseconds")
	assertPDSSEqual(t, FromNanoseconds(99235), "99235nanosecond")

	assertPDSSEqual(t, FromMicroseconds(99235), "99235us")
	assertPDSSEqual(t, FromMicroseconds(99235), "99235microseconds")
	assertPDSSEqual(t, FromMicroseconds(99235), "99235microsecond")

	assertPDSSEqual(t, FromHours(1), "1h")
	assertPDSSEqual(t, FromHours(1), "1hour")
	assertPDSSEqual(t, FromHours(2), "2hours")

	assertPDSSEqual(t, FromDays(1), "1d")
	assertPDSSEqual(t, FromDays(1), "1day")
	assertPDSSEqual(t, FromDays(10), "10days")
	assertPDSSEqual(t, FromDays(1), "1days")
	assertPDSSEqual(t, FromDays(10), "10day")

	assertPDSSEqual(t, FromDays(1)+FromMinutes(10), "1d10m")
	assertPDSSEqual(t, FromDays(1)+FromMinutes(10)+FromSeconds(200), "1d10m200sec")
	assertPDSSEqual(t, FromDays(1)+FromMinutes(10), "1d:10m")
	assertPDSSEqual(t, FromDays(1)+FromMinutes(10), "1d 10m")
	assertPDSSEqual(t, FromDays(1)+FromMinutes(10), "1d,10m")
	assertPDSSEqual(t, FromDays(1)+FromMinutes(10), "1d, 10m")
	assertPDSSEqual(t, FromDays(1)+FromSeconds(1000), "1d   1000seconds")

	assertPDSSEqual(t, FromDays(1), "86400s")
}

func assertPDSSEqual(t *testing.T, expected time.Duration, fmt string) {
	actual, err := ParseDurationShortString(fmt)
	if err != nil {
		t.Errorf("ParseDurationShortString('%s') failed with %v", fmt, err)
	}
	if actual != expected {
		t.Errorf("values differ: Actual: '%v', Expected: '%v'", actual.String(), expected.String())
	}
}
