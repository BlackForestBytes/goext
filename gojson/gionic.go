package json

import (
	"net/http"
)

// Render interface is copied from github.com/gin-gonic/gin@v1.8.1/render/render.go
type Render interface {
	// Render writes data with custom ContentType.
	Render(http.ResponseWriter) error
	// WriteContentType writes custom ContentType.
	WriteContentType(w http.ResponseWriter)
}

type GoJsonRender struct {
	Data          any
	NilSafeSlices bool
	NilSafeMaps   bool
	Indent        *IndentOpt
	Filter        *string
}

func (r GoJsonRender) Render(w http.ResponseWriter) error {
	header := w.Header()
	if val := header["Content-Type"]; len(val) == 0 {
		header["Content-Type"] = []string{"application/json; charset=utf-8"}
	}
	jsonBytes, err := MarshalSafeCollections(r.Data, r.NilSafeSlices, r.NilSafeMaps, r.Indent, r.Filter)
	if err != nil {
		panic(err)
	}
	_, err = w.Write(jsonBytes)
	if err != nil {
		panic(err)
	}
	return nil
}

func (r GoJsonRender) RenderString() (string, error) {
	jsonBytes, err := MarshalSafeCollections(r.Data, r.NilSafeSlices, r.NilSafeMaps, r.Indent, r.Filter)
	if err != nil {
		panic(err)
	}
	return string(jsonBytes), nil
}

func (r GoJsonRender) WriteContentType(w http.ResponseWriter) {
	header := w.Header()
	if val := header["Content-Type"]; len(val) == 0 {
		header["Content-Type"] = []string{"application/json; charset=utf-8"}
	}
}
