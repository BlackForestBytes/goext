

JSON serializer which serializes nil-Arrays as `[]` and nil-maps als `{}`.

Idea from: https://github.com/homelight/json

Forked from https://github.com/golang/go/tree/194de8fbfaf4c3ed54e1a3c1b14fc67a830b8d95/src/encoding/json ( tag go1.23.4 )
  -> https://github.com/golang/go/tree/go1.23.4/src/encoding/json

Added:

 - `MarshalSafeCollections()` method
 - `Encoder.nilSafeSlices` and `Encoder.nilSafeMaps` fields
 - `Add 'tagkey' to use different key than json (set on Decoder struct)`
 - `Add 'jsonfilter' to filter printed fields (set via MarshalSafeCollections)`