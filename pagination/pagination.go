package pagination

type Pagination struct {
	Page             int `json:"page"`             // page (first page == 1)
	Limit            int `json:"limit"`            // max-page-size
	TotalPages       int `json:"totalPages"`       // total page-count
	TotalItems       int `json:"totalItems"`       // total items-count
	CurrentPageCount int `json:"currentPageCount"` // item-count in current page ( == len(data) )
}

func CalcPaginationTotalPages(totalItems int, limit int) int {
	if totalItems == 0 {
		return 0
	}
	if limit == 0 {
		return 0
	}
	return 1 + (totalItems-1)/limit
}
