package pagination

import (
	"context"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
)

type MongoFilter interface {
	FilterQuery(ctx context.Context) mongo.Pipeline
	Sort(ctx context.Context) bson.D
}

type dynamicFilter struct {
	pipeline mongo.Pipeline
	sort     bson.D
}

func (d dynamicFilter) FilterQuery(ctx context.Context) mongo.Pipeline {
	return d.pipeline
}

func (d dynamicFilter) Sort(ctx context.Context) bson.D {
	return d.sort
}

func CreateFilter(pipeline mongo.Pipeline, sort bson.D) MongoFilter {
	return dynamicFilter{pipeline: pipeline, sort: sort}
}
