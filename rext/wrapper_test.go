package rext

import (
	"gogs.mikescher.com/BlackForestBytes/goext/tst"
	"regexp"
	"testing"
)

func TestGroupByNameOrEmpty1(t *testing.T) {

	regex1 := W(regexp.MustCompile(`0(?P<group1>A+)B(?P<group2>C+)0`))

	match1, ok1 := regex1.MatchFirst("Hello 0AAAABCCC0 Bye.")

	tst.AssertTrue(t, ok1)

	tst.AssertFalse(t, match1.GroupByNameOrEmpty("group1").IsEmpty())
	tst.AssertEqual(t, match1.GroupByNameOrEmpty("group1").ValueOrEmpty(), "AAAA")
	tst.AssertEqual(t, *match1.GroupByNameOrEmpty("group1").ValueOrNil(), "AAAA")

	tst.AssertFalse(t, match1.GroupByNameOrEmpty("group2").IsEmpty())
	tst.AssertEqual(t, match1.GroupByNameOrEmpty("group2").ValueOrEmpty(), "CCC")
	tst.AssertEqual(t, *match1.GroupByNameOrEmpty("group2").ValueOrNil(), "CCC")

}

func TestGroupByNameOrEmpty2(t *testing.T) {

	regex1 := W(regexp.MustCompile(`0(?P<group1>A+)B(?P<group2>C+)(?P<group3>C+)?0`))

	match1, ok1 := regex1.MatchFirst("Hello 0AAAABCCC0 Bye.")

	tst.AssertTrue(t, ok1)

	tst.AssertFalse(t, match1.GroupByNameOrEmpty("group1").IsEmpty())
	tst.AssertEqual(t, match1.GroupByNameOrEmpty("group1").ValueOrEmpty(), "AAAA")
	tst.AssertEqual(t, *match1.GroupByNameOrEmpty("group1").ValueOrNil(), "AAAA")

	tst.AssertFalse(t, match1.GroupByNameOrEmpty("group2").IsEmpty())
	tst.AssertEqual(t, match1.GroupByNameOrEmpty("group2").ValueOrEmpty(), "CCC")
	tst.AssertEqual(t, *match1.GroupByNameOrEmpty("group2").ValueOrNil(), "CCC")

	tst.AssertTrue(t, match1.GroupByNameOrEmpty("group3").IsEmpty())
	tst.AssertEqual(t, match1.GroupByNameOrEmpty("group3").ValueOrEmpty(), "")
	tst.AssertPtrEqual(t, match1.GroupByNameOrEmpty("group3").ValueOrNil(), nil)

}
