package dataext

import (
	"gogs.mikescher.com/BlackForestBytes/goext/langext"
	"gogs.mikescher.com/BlackForestBytes/goext/tst"
	"testing"
)

func TestObjectMerge(t *testing.T) {
	type A struct {
		Field1   *int
		Field2   *string
		Field3   *float64
		Field4   *bool
		OnlyA    int64
		DiffType int
	}
	type B struct {
		Field1   *int
		Field2   *string
		Field3   *float64
		Field4   *bool
		OnlyB    int64
		DiffType string
	}

	valueA := A{
		Field1:   nil,
		Field2:   langext.Ptr("99"),
		Field3:   langext.Ptr(12.2),
		Field4:   nil,
		OnlyA:    1,
		DiffType: 2,
	}

	valueB := B{
		Field1:   langext.Ptr(12),
		Field2:   nil,
		Field3:   langext.Ptr(13.2),
		Field4:   nil,
		OnlyB:    1,
		DiffType: "X",
	}

	valueMerge := ObjectMerge(valueA, valueB)

	tst.AssertIdentPtrEqual(t, "Field1", valueMerge.Field1, valueB.Field1)
	tst.AssertIdentPtrEqual(t, "Field2", valueMerge.Field2, valueA.Field2)
	tst.AssertIdentPtrEqual(t, "Field3", valueMerge.Field3, valueB.Field3)
	tst.AssertIdentPtrEqual(t, "Field4", valueMerge.Field4, nil)

}

func assertPtrEqual[T1 comparable](t *testing.T, ident string, actual *T1, expected *T1) {
	if actual == nil && expected == nil {
		return
	}
	if actual != nil && expected != nil {
		if *actual != *expected {
			t.Errorf("[%s] values differ: Actual: '%v', Expected: '%v'", ident, *actual, *expected)
		} else {
			return
		}
	}
	if actual == nil && expected != nil {
		t.Errorf("[%s] values differ: Actual: nil, Expected: not-nil", ident)
	}
	if actual != nil && expected == nil {
		t.Errorf("[%s] values differ: Actual: not-nil, Expected: nil", ident)
	}
}
