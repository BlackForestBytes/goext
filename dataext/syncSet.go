package dataext

import "sync"

type SyncSet[TData comparable] struct {
	data map[TData]bool
	lock sync.Mutex
}

func NewSyncSet[TData comparable]() *SyncSet[TData] {
	return &SyncSet[TData]{data: make(map[TData]bool), lock: sync.Mutex{}}
}

// Add adds `value` to the set
// returns true  if the value was actually inserted (value did not exist beforehand)
// returns false if the value already existed
func (s *SyncSet[TData]) Add(value TData) bool {
	s.lock.Lock()
	defer s.lock.Unlock()

	if s.data == nil {
		s.data = make(map[TData]bool)
	}

	_, existsInPreState := s.data[value]
	if existsInPreState {
		return false
	}

	s.data[value] = true
	return true
}

func (s *SyncSet[TData]) AddAll(values []TData) {
	s.lock.Lock()
	defer s.lock.Unlock()

	if s.data == nil {
		s.data = make(map[TData]bool)
	}

	for _, value := range values {
		s.data[value] = true
	}
}

func (s *SyncSet[TData]) Remove(value TData) bool {
	s.lock.Lock()
	defer s.lock.Unlock()

	if s.data == nil {
		s.data = make(map[TData]bool)
	}

	_, existsInPreState := s.data[value]
	if !existsInPreState {
		return false
	}

	delete(s.data, value)
	return true
}

func (s *SyncSet[TData]) RemoveAll(values []TData) {
	s.lock.Lock()
	defer s.lock.Unlock()

	if s.data == nil {
		s.data = make(map[TData]bool)
	}

	for _, value := range values {
		delete(s.data, value)
	}
}

func (s *SyncSet[TData]) Contains(value TData) bool {
	s.lock.Lock()
	defer s.lock.Unlock()

	if s.data == nil {
		s.data = make(map[TData]bool)
	}

	_, ok := s.data[value]

	return ok
}

func (s *SyncSet[TData]) Get() []TData {
	s.lock.Lock()
	defer s.lock.Unlock()

	if s.data == nil {
		s.data = make(map[TData]bool)
	}

	r := make([]TData, 0, len(s.data))

	for k := range s.data {
		r = append(r, k)
	}

	return r
}

// AddIfNotContains
// returns true  if the value was actually added (value did not exist beforehand)
// returns false if the value already existed
func (s *SyncSet[TData]) AddIfNotContains(key TData) bool {
	return s.Add(key)
}

// RemoveIfContains
// returns true  if the value was actually removed (value did exist beforehand)
// returns false if the value did not exist in the set
func (s *SyncSet[TData]) RemoveIfContains(key TData) bool {
	return s.Remove(key)
}
