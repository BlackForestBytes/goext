package dataext

type ValueGroup interface {
	TupleLength() int
	TupleValues() []any
}

// ----------------------------------------------------------------------------

type Single[T1 any] struct {
	V1 T1
}

func (s Single[T1]) TupleLength() int {
	return 1
}

func (s Single[T1]) TupleValues() []any {
	return []any{s.V1}
}

func NewSingle[T1 any](v1 T1) Single[T1] {
	return Single[T1]{V1: v1}
}

func NewTuple1[T1 any](v1 T1) Single[T1] {
	return Single[T1]{V1: v1}
}

// ----------------------------------------------------------------------------

type Tuple[T1 any, T2 any] struct {
	V1 T1
	V2 T2
}

func (t Tuple[T1, T2]) TupleLength() int {
	return 2
}

func (t Tuple[T1, T2]) TupleValues() []any {
	return []any{t.V1, t.V2}
}

func NewTuple[T1 any, T2 any](v1 T1, v2 T2) Tuple[T1, T2] {
	return Tuple[T1, T2]{V1: v1, V2: v2}
}

func NewTuple2[T1 any, T2 any](v1 T1, v2 T2) Tuple[T1, T2] {
	return Tuple[T1, T2]{V1: v1, V2: v2}
}

// ----------------------------------------------------------------------------

type Triple[T1 any, T2 any, T3 any] struct {
	V1 T1
	V2 T2
	V3 T3
}

func (t Triple[T1, T2, T3]) TupleLength() int {
	return 3
}

func (t Triple[T1, T2, T3]) TupleValues() []any {
	return []any{t.V1, t.V2, t.V3}
}

func NewTriple[T1 any, T2 any, T3 any](v1 T1, v2 T2, v3 T3) Triple[T1, T2, T3] {
	return Triple[T1, T2, T3]{V1: v1, V2: v2, V3: v3}
}

func NewTuple3[T1 any, T2 any, T3 any](v1 T1, v2 T2, v3 T3) Triple[T1, T2, T3] {
	return Triple[T1, T2, T3]{V1: v1, V2: v2, V3: v3}
}

// ----------------------------------------------------------------------------

type Quadruple[T1 any, T2 any, T3 any, T4 any] struct {
	V1 T1
	V2 T2
	V3 T3
	V4 T4
}

func (t Quadruple[T1, T2, T3, T4]) TupleLength() int {
	return 4
}

func (t Quadruple[T1, T2, T3, T4]) TupleValues() []any {
	return []any{t.V1, t.V2, t.V3, t.V4}
}

func NewQuadruple[T1 any, T2 any, T3 any, T4 any](v1 T1, v2 T2, v3 T3, v4 T4) Quadruple[T1, T2, T3, T4] {
	return Quadruple[T1, T2, T3, T4]{V1: v1, V2: v2, V3: v3, V4: v4}
}

func NewTuple4[T1 any, T2 any, T3 any, T4 any](v1 T1, v2 T2, v3 T3, v4 T4) Quadruple[T1, T2, T3, T4] {
	return Quadruple[T1, T2, T3, T4]{V1: v1, V2: v2, V3: v3, V4: v4}
}

// ----------------------------------------------------------------------------

type Quintuple[T1 any, T2 any, T3 any, T4 any, T5 any] struct {
	V1 T1
	V2 T2
	V3 T3
	V4 T4
	V5 T5
}

func (t Quintuple[T1, T2, T3, T4, T5]) TupleLength() int {
	return 5
}

func (t Quintuple[T1, T2, T3, T4, T5]) TupleValues() []any {
	return []any{t.V1, t.V2, t.V3, t.V4, t.V5}

}

func NewQuintuple[T1 any, T2 any, T3 any, T4 any, T5 any](v1 T1, v2 T2, v3 T3, v4 T4, v5 T5) Quintuple[T1, T2, T3, T4, T5] {
	return Quintuple[T1, T2, T3, T4, T5]{V1: v1, V2: v2, V3: v3, V4: v4, V5: v5}
}

func NewTuple5[T1 any, T2 any, T3 any, T4 any, T5 any](v1 T1, v2 T2, v3 T3, v4 T4, v5 T5) Quintuple[T1, T2, T3, T4, T5] {
	return Quintuple[T1, T2, T3, T4, T5]{V1: v1, V2: v2, V3: v3, V4: v4, V5: v5}
}

// ----------------------------------------------------------------------------

type Sextuple[T1 any, T2 any, T3 any, T4 any, T5 any, T6 any] struct {
	V1 T1
	V2 T2
	V3 T3
	V4 T4
	V5 T5
	V6 T6
}

func (t Sextuple[T1, T2, T3, T4, T5, T6]) TupleLength() int {
	return 6
}

func (t Sextuple[T1, T2, T3, T4, T5, T6]) TupleValues() []any {
	return []any{t.V1, t.V2, t.V3, t.V4, t.V5, t.V6}

}

func NewSextuple[T1 any, T2 any, T3 any, T4 any, T5 any, T6 any](v1 T1, v2 T2, v3 T3, v4 T4, v5 T5, v6 T6) Sextuple[T1, T2, T3, T4, T5, T6] {
	return Sextuple[T1, T2, T3, T4, T5, T6]{V1: v1, V2: v2, V3: v3, V4: v4, V5: v5, V6: v6}
}

func NewTuple6[T1 any, T2 any, T3 any, T4 any, T5 any, T6 any](v1 T1, v2 T2, v3 T3, v4 T4, v5 T5, v6 T6) Sextuple[T1, T2, T3, T4, T5, T6] {
	return Sextuple[T1, T2, T3, T4, T5, T6]{V1: v1, V2: v2, V3: v3, V4: v4, V5: v5, V6: v6}
}

// ----------------------------------------------------------------------------

type Septuple[T1 any, T2 any, T3 any, T4 any, T5 any, T6 any, T7 any] struct {
	V1 T1
	V2 T2
	V3 T3
	V4 T4
	V5 T5
	V6 T6
	V7 T7
}

func (t Septuple[T1, T2, T3, T4, T5, T6, T7]) TupleLength() int {
	return 7
}

func (t Septuple[T1, T2, T3, T4, T5, T6, T7]) TupleValues() []any {
	return []any{t.V1, t.V2, t.V3, t.V4, t.V5, t.V6, t.V7}
}

func NewSeptuple[T1 any, T2 any, T3 any, T4 any, T5 any, T6 any, T7 any](v1 T1, v2 T2, v3 T3, v4 T4, v5 T5, v6 T6, v7 T7) Septuple[T1, T2, T3, T4, T5, T6, T7] {
	return Septuple[T1, T2, T3, T4, T5, T6, T7]{V1: v1, V2: v2, V3: v3, V4: v4, V5: v5, V6: v6, V7: v7}
}

func NewTuple7[T1 any, T2 any, T3 any, T4 any, T5 any, T6 any, T7 any](v1 T1, v2 T2, v3 T3, v4 T4, v5 T5, v6 T6, v7 T7) Septuple[T1, T2, T3, T4, T5, T6, T7] {
	return Septuple[T1, T2, T3, T4, T5, T6, T7]{V1: v1, V2: v2, V3: v3, V4: v4, V5: v5, V6: v6, V7: v7}
}

// ----------------------------------------------------------------------------

type Octuple[T1 any, T2 any, T3 any, T4 any, T5 any, T6 any, T7 any, T8 any] struct {
	V1 T1
	V2 T2
	V3 T3
	V4 T4
	V5 T5
	V6 T6
	V7 T7
	V8 T8
}

func (t Octuple[T1, T2, T3, T4, T5, T6, T7, T8]) TupleLength() int {
	return 8
}

func (t Octuple[T1, T2, T3, T4, T5, T6, T7, T8]) TupleValues() []any {
	return []any{t.V1, t.V2, t.V3, t.V4, t.V5, t.V6, t.V7, t.V8}
}

func NewOctuple[T1 any, T2 any, T3 any, T4 any, T5 any, T6 any, T7 any, T8 any](v1 T1, v2 T2, v3 T3, v4 T4, v5 T5, v6 T6, v7 T7, v8 T8) Octuple[T1, T2, T3, T4, T5, T6, T7, T8] {
	return Octuple[T1, T2, T3, T4, T5, T6, T7, T8]{V1: v1, V2: v2, V3: v3, V4: v4, V5: v5, V6: v6, V7: v7, V8: v8}
}

func NewTuple8[T1 any, T2 any, T3 any, T4 any, T5 any, T6 any, T7 any, T8 any](v1 T1, v2 T2, v3 T3, v4 T4, v5 T5, v6 T6, v7 T7, v8 T8) Octuple[T1, T2, T3, T4, T5, T6, T7, T8] {
	return Octuple[T1, T2, T3, T4, T5, T6, T7, T8]{V1: v1, V2: v2, V3: v3, V4: v4, V5: v5, V6: v6, V7: v7, V8: v8}
}

// ----------------------------------------------------------------------------

type Nonuple[T1 any, T2 any, T3 any, T4 any, T5 any, T6 any, T7 any, T8 any, T9 any] struct {
	V1 T1
	V2 T2
	V3 T3
	V4 T4
	V5 T5
	V6 T6
	V7 T7
	V8 T8
	V9 T9
}

func (t Nonuple[T1, T2, T3, T4, T5, T6, T7, T8, T9]) TupleLength() int {
	return 9
}

func (t Nonuple[T1, T2, T3, T4, T5, T6, T7, T8, T9]) TupleValues() []any {
	return []any{t.V1, t.V2, t.V3, t.V4, t.V5, t.V6, t.V7, t.V8, t.V9}
}

func NewNonuple[T1 any, T2 any, T3 any, T4 any, T5 any, T6 any, T7 any, T8 any, T9 any](v1 T1, v2 T2, v3 T3, v4 T4, v5 T5, v6 T6, v7 T7, v8 T8, v9 T9) Nonuple[T1, T2, T3, T4, T5, T6, T7, T8, T9] {
	return Nonuple[T1, T2, T3, T4, T5, T6, T7, T8, T9]{V1: v1, V2: v2, V3: v3, V4: v4, V5: v5, V6: v6, V7: v7, V8: v8, V9: v9}
}
func NewTuple9[T1 any, T2 any, T3 any, T4 any, T5 any, T6 any, T7 any, T8 any, T9 any](v1 T1, v2 T2, v3 T3, v4 T4, v5 T5, v6 T6, v7 T7, v8 T8, v9 T9) Nonuple[T1, T2, T3, T4, T5, T6, T7, T8, T9] {
	return Nonuple[T1, T2, T3, T4, T5, T6, T7, T8, T9]{V1: v1, V2: v2, V3: v3, V4: v4, V5: v5, V6: v6, V7: v7, V8: v8, V9: v9}
}
