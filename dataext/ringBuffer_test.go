package dataext

import "testing"

func TestRingBufferPushAddsItem(t *testing.T) {
	rb := NewRingBuffer[int](3)
	rb.Push(1)
	if rb.Size() != 1 {
		t.Errorf("Expected size 1, got %d", rb.Size())
	}
	if item, _ := rb.Peek(); item != 1 {
		t.Errorf("Expected item 1, got %d", item)
	}
}

func TestRingBufferPushPopReturnsOldestItem(t *testing.T) {
	rb := NewRingBuffer[int](3)
	rb.Push(1)
	rb.Push(2)
	rb.Push(3)
	if item := rb.PushPop(4); item == nil || *item != 1 {
		t.Errorf("Expected item 1, got %v", item)
	}
}

func TestRingBufferPeekReturnsLastPushedItem(t *testing.T) {
	rb := NewRingBuffer[int](3)
	rb.Push(1)
	rb.Push(2)
	if item, _ := rb.Peek(); item != 2 {
		t.Errorf("Expected item 2, got %d", item)
	}
}

func TestRingBufferOverflow1(t *testing.T) {
	rb := NewRingBuffer[int](5)
	rb.Push(1) // overriden
	rb.Push(2) // overriden
	rb.Push(3)
	rb.Push(9)
	rb.Push(4)
	rb.Push(5)
	rb.Push(7)
	if rb.Size() != 5 {
		t.Errorf("Expected size 4, got %d", rb.Size())
	}
	expected := []int{3, 9, 4, 5, 7}
	items := rb.Items()
	for i, item := range items {
		if item != expected[i] {
			t.Errorf("Expected item %d, got %d", expected[i], item)
		}
	}
}

func TestRingBufferItemsReturnsAllItems(t *testing.T) {
	rb := NewRingBuffer[int](3)
	rb.Push(1)
	rb.Push(2)
	rb.Push(3)
	items := rb.Items()
	expected := []int{1, 2, 3}
	for i, item := range items {
		if item != expected[i] {
			t.Errorf("Expected item %d, got %d", expected[i], item)
		}
	}
}

func TestRingBufferClearEmptiesBuffer(t *testing.T) {
	rb := NewRingBuffer[int](3)
	rb.Push(1)
	rb.Clear()
	if rb.Size() != 0 {
		t.Errorf("Expected size 0, got %d", rb.Size())
	}
}

func TestRingBufferIsFullReturnsTrueWhenFull(t *testing.T) {
	rb := NewRingBuffer[int](3)
	rb.Push(1)
	rb.Push(2)
	rb.Push(3)
	if !rb.IsFull() {
		t.Errorf("Expected buffer to be full")
	}
}

func TestRingBufferAtReturnsCorrectItem(t *testing.T) {
	rb := NewRingBuffer[int](3)
	rb.Push(1)
	rb.Push(2)
	rb.Push(3)
	if item := rb.At(1); item != 2 {
		t.Errorf("Expected item 2, got %d", item)
	}
}

func TestRingBufferGetReturnsCorrectItem(t *testing.T) {
	rb := NewRingBuffer[int](3)
	rb.Push(1)
	rb.Push(2)
	rb.Push(3)
	if item, ok := rb.Get(1); !ok || item != 2 {
		t.Errorf("Expected item 2, got %d", item)
	}
}

func TestRingBufferRemoveDeletesMatchingItems(t *testing.T) {
	rb := NewRingBuffer[int](5)
	rb.Push(1)
	rb.Push(2)
	rb.Push(3)
	rb.Push(2)
	rb.Push(4)
	removed := rb.Remove(func(v int) bool { return v == 2 })
	if removed != 2 {
		t.Errorf("Expected 2 items removed, got %d", removed)
	}
	if rb.Size() != 3 {
		t.Errorf("Expected size 3, got %d", rb.Size())
	}
	expected := []int{1, 3, 4}
	items := rb.Items()
	for i, item := range items {
		if item != expected[i] {
			t.Errorf("Expected item %d, got %d", expected[i], item)
		}
	}
}

func TestRingBufferRemoveDeletesMatchingItems2(t *testing.T) {
	rb := NewRingBuffer[int](5)
	rb.Push(1)
	rb.Push(2)
	rb.Push(3)
	rb.Push(2)
	rb.Push(4)
	removed := rb.Remove(func(v int) bool { return v == 3 })
	if removed != 1 {
		t.Errorf("Expected 2 items removed, got %d", removed)
	}
	if rb.Size() != 4 {
		t.Errorf("Expected size 3, got %d", rb.Size())
	}
	expected := []int{1, 2, 2, 4}
	items := rb.Items()
	for i, item := range items {
		if item != expected[i] {
			t.Errorf("Expected item %d, got %d", expected[i], item)
		}
	}
}

func TestRingBufferRemoveDeletesMatchingItems3(t *testing.T) {
	rb := NewRingBuffer[int](5)
	rb.Push(1)
	rb.Push(2)
	rb.Push(3)
	rb.Push(9)
	rb.Push(4)
	removed := rb.Remove(func(v int) bool { return v == 3 })
	if removed != 1 {
		t.Errorf("Expected 2 items removed, got %d", removed)
	}
	if rb.Size() != 4 {
		t.Errorf("Expected size 3, got %d", rb.Size())
	}
	expected := []int{1, 2, 9, 4}
	items := rb.Items()
	for i, item := range items {
		if item != expected[i] {
			t.Errorf("Expected item %d, got %d", expected[i], item)
		}
	}
}

func TestRingBufferRemoveDeletesMatchingItems4(t *testing.T) {
	rb := NewRingBuffer[int](5)
	rb.Push(1) // overriden
	rb.Push(2) // overriden
	rb.Push(3)
	rb.Push(9)
	rb.Push(4)
	rb.Push(5)
	rb.Push(7)
	removed := rb.Remove(func(v int) bool { return v == 7 })
	if removed != 1 {
		t.Errorf("Expected 1 items removed, got %d", removed)
	}
	if rb.Size() != 4 {
		t.Errorf("Expected size 4, got %d", rb.Size())
	}
	expected := []int{3, 9, 4, 5}
	items := rb.Items()
	for i, item := range items {
		if item != expected[i] {
			t.Errorf("Expected item %d, got %d", expected[i], item)
		}
	}
}

func TestRingBufferRemoveDeletesMatchingItems5(t *testing.T) {
	rb := NewRingBuffer[int](5)
	rb.Push(1) // overriden
	rb.Push(2) // overriden
	rb.Push(3)
	rb.Push(9)
	rb.Push(4)
	rb.Push(5)
	rb.Push(7)
	removed := rb.Remove(func(v int) bool { return v == 3 })
	if removed != 1 {
		t.Errorf("Expected 1 items removed, got %d", removed)
	}
	if rb.Size() != 4 {
		t.Errorf("Expected size 4, got %d", rb.Size())
	}
	expected := []int{9, 4, 5, 7}
	items := rb.Items()
	for i, item := range items {
		if item != expected[i] {
			t.Errorf("Expected item %d, got %d", expected[i], item)
		}
	}
}

func TestRingBufferRemoveDeletesMatchingItems6(t *testing.T) {
	rb := NewRingBuffer[int](5)
	rb.Push(1) // overriden
	rb.Push(2) // overriden
	rb.Push(3)
	rb.Push(9)
	rb.Push(4)
	rb.Push(5)
	rb.Push(7)
	removed := rb.Remove(func(v int) bool { return v == 1 })
	if removed != 0 {
		t.Errorf("Expected 0 items removed, got %d", removed)
	}
	if rb.Size() != 5 {
		t.Errorf("Expected size 5, got %d", rb.Size())
	}
	expected := []int{3, 9, 4, 5, 7}
	items := rb.Items()
	for i, item := range items {
		if item != expected[i] {
			t.Errorf("Expected item %d, got %d", expected[i], item)
		}
	}
	if !rb.IsFull() {
		t.Errorf("Expected buffer to not be full")
	}
}

func TestRingBufferRemoveDeletesMatchingItems7(t *testing.T) {
	rb := NewRingBuffer[int](5)
	rb.Push(1) // overriden
	rb.Push(2) // overriden
	rb.Push(3)
	rb.Push(9)
	rb.Push(4)
	rb.Push(5)
	rb.Push(7)
	removed := rb.Remove(func(v int) bool { return v == 9 })
	if removed != 1 {
		t.Errorf("Expected 1 items removed, got %d", removed)
	}
	if rb.Size() != 4 {
		t.Errorf("Expected size 4, got %d", rb.Size())
	}
	expected := []int{3, 4, 5, 7}
	items := rb.Items()
	for i, item := range items {
		if item != expected[i] {
			t.Errorf("Expected item %d, got %d", expected[i], item)
		}
	}
	if rb.IsFull() {
		t.Errorf("Expected buffer to not be full")
	}
}

func TestRingBufferAddItemsToFullRingBuffer(t *testing.T) {
	rb := NewRingBuffer[int](3)
	rb.Push(1)
	rb.Push(2)
	rb.Push(3)
	rb.Push(4)
	if rb.Size() != 3 {
		t.Errorf("Expected size 3, got %d", rb.Size())
	}
	expected := []int{2, 3, 4}
	items := rb.Items()
	for i, item := range items {
		if item != expected[i] {
			t.Errorf("Expected item %d, got %d", expected[i], item)
		}
	}
}

func TestRingBufferAddItemsToNonFullRingBuffer(t *testing.T) {
	rb := NewRingBuffer[int](3)
	rb.Push(1)
	rb.Push(2)
	if rb.Size() != 2 {
		t.Errorf("Expected size 2, got %d", rb.Size())
	}
	expected := []int{1, 2}
	items := rb.Items()
	for i, item := range items {
		if item != expected[i] {
			t.Errorf("Expected item %d, got %d", expected[i], item)
		}
	}
}

func TestRingBufferRemoveItemsFromNonFullRingBuffer(t *testing.T) {
	rb := NewRingBuffer[int](3)
	rb.Push(1)
	rb.Push(2)
	removed := rb.Remove(func(v int) bool { return v == 1 })
	if removed != 1 {
		t.Errorf("Expected 1 item removed, got %d", removed)
	}
	if rb.Size() != 1 {
		t.Errorf("Expected size 1, got %d", rb.Size())
	}
	expected := []int{2}
	items := rb.Items()
	for i, item := range items {
		if item != expected[i] {
			t.Errorf("Expected item %d, got %d", expected[i], item)
		}
	}
}

func TestRingBufferRemoveItemsFromFullRingBuffer(t *testing.T) {
	rb := NewRingBuffer[int](3)
	rb.Push(1)
	rb.Push(2)
	rb.Push(3)
	removed := rb.Remove(func(v int) bool { return v == 2 })
	if removed != 1 {
		t.Errorf("Expected 1 item removed, got %d", removed)
	}
	if rb.Size() != 2 {
		t.Errorf("Expected size 2, got %d", rb.Size())
	}
	expected := []int{1, 3}
	items := rb.Items()
	for i, item := range items {
		if item != expected[i] {
			t.Errorf("Expected item %d, got %d", expected[i], item)
		}
	}
}

func TestRingBufferRemoveMultipleItemsFromRingBuffer(t *testing.T) {
	rb := NewRingBuffer[int](5)
	rb.Push(1)
	rb.Push(2)
	rb.Push(3)
	rb.Push(2)
	rb.Push(4)
	removed := rb.Remove(func(v int) bool { return v == 2 })
	if removed != 2 {
		t.Errorf("Expected 2 items removed, got %d", removed)
	}
	if rb.Size() != 3 {
		t.Errorf("Expected size 3, got %d", rb.Size())
	}
	expected := []int{1, 3, 4}
	items := rb.Items()
	for i, item := range items {
		if item != expected[i] {
			t.Errorf("Expected item %d, got %d", expected[i], item)
		}
	}
}

func TestRingBufferRemoveAllItemsFromRingBuffer(t *testing.T) {
	rb := NewRingBuffer[int](3)
	rb.Push(1)
	rb.Push(2)
	rb.Push(3)
	removed := rb.Remove(func(v int) bool { return true })
	if removed != 3 {
		t.Errorf("Expected 3 items removed, got %d", removed)
	}
	if rb.Size() != 0 {
		t.Errorf("Expected size 0, got %d", rb.Size())
	}
}

func TestRingBufferRemoveNoItemsFromRingBuffer(t *testing.T) {
	rb := NewRingBuffer[int](3)
	rb.Push(1)
	rb.Push(2)
	rb.Push(3)
	removed := rb.Remove(func(v int) bool { return false })
	if removed != 0 {
		t.Errorf("Expected 0 items removed, got %d", removed)
	}
	if rb.Size() != 3 {
		t.Errorf("Expected size 3, got %d", rb.Size())
	}
}

func TestRingBufferIteratesOverAllItems(t *testing.T) {
	rb := NewRingBuffer[int](3)
	rb.Push(1)
	rb.Push(2)
	rb.Push(3)
	expected := []int{1, 2, 3}
	i := 0
	for item := range rb.Iter() {
		if item != expected[i] {
			t.Errorf("Expected item %d, got %d", expected[i], item)
		}
		i++
	}
	if i != len(expected) {
		t.Errorf("Expected to iterate over %d items, but iterated over %d", len(expected), i)
	}
}

func TestRingBufferIter2IteratesOverAllItemsWithIndices(t *testing.T) {
	rb := NewRingBuffer[int](3)
	rb.Push(1)
	rb.Push(2)
	rb.Push(3)
	expected := []int{1, 2, 3}
	i := 0
	for index, item := range rb.Iter2() {
		if index != i {
			t.Errorf("Expected index %d, got %d", i, index)
		}
		if item != expected[i] {
			t.Errorf("Expected item %d, got %d", expected[i], item)
		}
		i++
	}
	if i != len(expected) {
		t.Errorf("Expected to iterate over %d items, but iterated over %d", len(expected), i)
	}
}
