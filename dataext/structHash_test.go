package dataext

import (
	"gogs.mikescher.com/BlackForestBytes/goext/langext"
	"gogs.mikescher.com/BlackForestBytes/goext/tst"
	"testing"
)

func noErrStructHash(t *testing.T, dat any, opt ...StructHashOptions) []byte {
	res, err := StructHash(dat, opt...)
	if err != nil {
		t.Error(err)
		t.FailNow()
		return nil
	}
	return res
}

func TestStructHashSimple(t *testing.T) {

	tst.AssertHexEqual(t, "209bf774af36cc3a045c152d9f1269ef3684ad819c1359ee73ff0283a308fefa", noErrStructHash(t, "Hello"))
	tst.AssertHexEqual(t, "c32f3626b981ae2997db656f3acad3f1dc9d30ef6b6d14296c023e391b25f71a", noErrStructHash(t, 0))
	tst.AssertHexEqual(t, "01b781b03e9586b257d387057dfc70d9f06051e7d3c1e709a57e13cc8daf3e35", noErrStructHash(t, []byte{}))
	tst.AssertHexEqual(t, "93e1dcd45c732fe0079b0fb3204c7c803f0921835f6bfee2e6ff263e73eed53c", noErrStructHash(t, []int{}))
	tst.AssertHexEqual(t, "54f637a376aad55b3160d98ebbcae8099b70d91b9400df23fb3709855d59800a", noErrStructHash(t, []int{1, 2, 3}))
	tst.AssertHexEqual(t, "e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855", noErrStructHash(t, nil))
	tst.AssertHexEqual(t, "349a7db91aa78fd30bbaa7c7f9c7bfb2fcfe72869b4861162a96713a852f60d3", noErrStructHash(t, []any{1, "", nil}))
	tst.AssertHexEqual(t, "ca51aab87808bf0062a4a024de6aac0c2bad54275cc857a4944569f89fd245ad", noErrStructHash(t, struct{}{}))

}

func TestStructHashSimpleStruct(t *testing.T) {

	type t0 struct {
		F1 int
		F2 []string
		F3 *int
	}

	tst.AssertHexEqual(t, "a90bff751c70c738bb5cfc9b108e783fa9c19c0bc9273458e0aaee6e74aa1b92", noErrStructHash(t, t0{
		F1: 10,
		F2: []string{"1", "2", "3"},
		F3: nil,
	}))

	tst.AssertHexEqual(t, "5d09090dc34ac59dd645f197a255f653387723de3afa1b614721ea5a081c675f", noErrStructHash(t, t0{
		F1: 10,
		F2: []string{"1", "2", "3"},
		F3: langext.Ptr(99),
	}))

}

func TestStructHashLayeredStruct(t *testing.T) {

	type t1_1 struct {
		F10 float32
		F12 float64
		F15 bool
	}
	type t1_2 struct {
		SV1 *t1_1
		SV2 *t1_1
		SV3 t1_1
	}

	tst.AssertHexEqual(t, "fd4ca071fb40a288fee4b7a3dfdaab577b30cb8f80f81ec511e7afd72dc3b469", noErrStructHash(t, t1_2{
		SV1: nil,
		SV2: nil,
		SV3: t1_1{
			F10: 1,
			F12: 2,
			F15: false,
		},
	}))
	tst.AssertHexEqual(t, "3fbf7c67d8121deda075cc86319a4e32d71744feb2cebf89b43bc682f072a029", noErrStructHash(t, t1_2{
		SV1: nil,
		SV2: &t1_1{},
		SV3: t1_1{
			F10: 3,
			F12: 4,
			F15: true,
		},
	}))
	tst.AssertHexEqual(t, "b1791ccd1b346c3ede5bbffda85555adcd8216b93ffca23f14fe175ec47c5104", noErrStructHash(t, t1_2{
		SV1: &t1_1{},
		SV2: &t1_1{},
		SV3: t1_1{
			F10: 5,
			F12: 6,
			F15: false,
		},
	}))

}

func TestStructHashMap(t *testing.T) {

	type t0 struct {
		F1 int
		F2 map[string]int
	}

	tst.AssertHexEqual(t, "d50c53ad1fafb448c33fddd5aca01a86a2edf669ce2ecab07ba6fe877951d824", noErrStructHash(t, t0{
		F1: 10,
		F2: map[string]int{
			"x": 1,
			"0": 2,
			"a": 99,
		},
	}))

	tst.AssertHexEqual(t, "d50c53ad1fafb448c33fddd5aca01a86a2edf669ce2ecab07ba6fe877951d824", noErrStructHash(t, t0{
		F1: 10,
		F2: map[string]int{
			"a": 99,
			"x": 1,
			"0": 2,
		},
	}))

	m3 := make(map[string]int, 99)
	m3["a"] = 0
	m3["x"] = 0
	m3["0"] = 0

	m3["0"] = 99
	m3["x"] = 1
	m3["a"] = 2

	tst.AssertHexEqual(t, "d50c53ad1fafb448c33fddd5aca01a86a2edf669ce2ecab07ba6fe877951d824", noErrStructHash(t, t0{
		F1: 10,
		F2: m3,
	}))

}
