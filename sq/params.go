package sq

import "gogs.mikescher.com/BlackForestBytes/goext/langext"

type PP map[string]any

func Join(pps ...PP) PP {
	r := PP{}
	for _, add := range pps {
		for k, v := range add {
			r[k] = v
		}
	}
	return r
}

func (pp *PP) Add(v any) string {
	id := PPID()
	(*pp)[id] = v
	return id
}

func (pp *PP) AddAll(other PP) {
	for id, v := range other {
		(*pp)[id] = v
	}
}

func PPID() string {
	return "p_" + langext.RandBase62(8)
}
