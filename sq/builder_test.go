package sq

import (
	"context"
	"database/sql"
	"errors"
	"fmt"
	"github.com/glebarez/go-sqlite"
	"github.com/jmoiron/sqlx"
	"gogs.mikescher.com/BlackForestBytes/goext/langext"
	"gogs.mikescher.com/BlackForestBytes/goext/tst"
	"path/filepath"
	"testing"
)

type dummyQueryable struct {
}

func (d dummyQueryable) Exec(ctx context.Context, sql string, prep PP) (sql.Result, error) {
	return nil, errors.New("err")
}

func (d dummyQueryable) Query(ctx context.Context, sql string, prep PP) (*sqlx.Rows, error) {
	return nil, errors.New("err")
}

func (d dummyQueryable) ListConverter() []DBTypeConverter {
	return nil
}

func TestCreateUpdateStatement(t *testing.T) {

	type request struct {
		ID        string  `json:"id"        db:"id"`
		Timestamp int     `json:"timestamp" db:"timestamp"`
		StrVal    string  `json:"strVal"    db:"str_val"`
		FloatVal  float64 `json:"floatVal"    db:"float_val"`
		Dummy     bool    `json:"dummyBool" db:"dummy_bool"`
		JsonVal   JsonObj `json:"jsonVal" db:"json_val"`
	}

	if !langext.InArray("sqlite3", sql.Drivers()) {
		sqlite.RegisterAsSQLITE3()
	}

	ctx := context.Background()

	dbdir := t.TempDir()
	dbfile1 := filepath.Join(dbdir, langext.MustHexUUID()+".sqlite3")

	url := fmt.Sprintf("file:%s?_pragma=journal_mode(%s)&_pragma=timeout(%d)&_pragma=foreign_keys(%s)&_pragma=busy_timeout(%d)", dbfile1, "DELETE", 1000, "true", 1000)

	xdb := tst.Must(sqlx.Open("sqlite", url))(t)

	db := NewDB(xdb, DBOptions{RegisterDefaultConverter: langext.PTrue})

	_, err := db.Exec(ctx, "CREATE TABLE `requests` ( id TEXT NOT NULL, timestamp INTEGER NOT NULL, PRIMARY KEY (id) ) STRICT", PP{})
	tst.AssertNoErr(t, err)

	sqlStr, pp, err := BuildUpdateStatement(db, "requests", request{
		ID:        "9927",
		Timestamp: 12321,
		StrVal:    "hello world",
		Dummy:     true,
		FloatVal:  3.14159,
		JsonVal: JsonObj{
			"firs":   1,
			"second": true,
		},
	}, "id")

	tst.AssertNoErr(t, err)

	fmt.Printf("\n\n")

	fmt.Printf("######## PP:\n")
	for k, v := range pp {
		fmt.Printf("['%s']       =>       %+v\n", k, v)
	}
	fmt.Printf("\n\n")

	fmt.Printf("######## SQL:\n%+v\n\n", sqlStr)

	fmt.Printf("\n\n")

}
