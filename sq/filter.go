package sq

import ct "gogs.mikescher.com/BlackForestBytes/goext/cursortoken"

type FilterSort struct {
	Field     string
	Direction ct.SortDirection
}

type PaginateFilter interface {
	SQL(params PP) (filterClause string, joinClause string, joinTables []string)
	Sort() []FilterSort
}

type genericPaginateFilter struct {
	sql  func(params PP) (filterClause string, joinClause string, joinTables []string)
	sort func() []FilterSort
}

func (g genericPaginateFilter) SQL(params PP) (filterClause string, joinClause string, joinTables []string) {
	return g.sql(params)
}

func (g genericPaginateFilter) Sort() []FilterSort {
	return g.sort()
}

func NewPaginateFilter(sql func(params PP) (filterClause string, joinClause string, joinTables []string), sort []FilterSort) PaginateFilter {
	return genericPaginateFilter{
		sql: func(params PP) (filterClause string, joinClause string, joinTables []string) {
			return sql(params)
		},
		sort: func() []FilterSort {
			return sort
		},
	}
}

func NewSimplePaginateFilter(filterClause string, filterParams PP, sort []FilterSort) PaginateFilter {
	return genericPaginateFilter{
		sql: func(params PP) (string, string, []string) {
			params.AddAll(filterParams)
			return filterClause, "", nil
		},
		sort: func() []FilterSort {
			return sort
		},
	}
}

func NewEmptyPaginateFilter() PaginateFilter {
	return genericPaginateFilter{
		sql:  func(params PP) (string, string, []string) { return "1=1", "", nil },
		sort: func() []FilterSort { return make([]FilterSort, 0) },
	}
}
