package sq

import (
	"context"
	"strings"
)

var CommentTrimmer = NewPreListener(fnTrimComments)

func fnTrimComments(ctx context.Context, cmdtype string, id *uint16, sql *string, params *PP) error {

	res := make([]string, 0)

	for _, s := range strings.Split(*sql, "\n") {
		if strings.HasPrefix(strings.TrimSpace(s), "--") {
			continue
		}

		idx := strings.Index(s, "--")
		if idx != -1 {
			s = s[:idx]
		}

		s = strings.TrimRight(s, " \t\r\n")

		res = append(res, s)
	}

	*sql = strings.Join(res, "\n")

	return nil
}
