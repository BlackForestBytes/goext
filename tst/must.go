package tst

import (
	"runtime/debug"
	"testing"
)

// Must can b used to AssertNoErr of an (T, err) function
//
// Usage:
//
// input := "123.8"
// value := tst.Must(strconv.Atoi(input))(t)
func Must[T any](v T, anerr error) func(t *testing.T) T {
	return func(t *testing.T) T {
		if anerr != nil {
			t.Error("Function returned an error: " + anerr.Error() + "\n" + string(debug.Stack()))
		}
		return v
	}
}
