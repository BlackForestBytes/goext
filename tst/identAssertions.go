package tst

import (
	"testing"
)

func AssertIdentEqual[T comparable](t *testing.T, ident string, actual T, expected T) {
	if actual != expected {
		t.Errorf("[%s] values differ: Actual: '%v', Expected: '%v'", ident, actual, expected)
	}
}

func AssertIdentNotEqual[T comparable](t *testing.T, ident string, actual T, expected T) {
	if actual == expected {
		t.Errorf("[%s] values do not differ: Actual: '%v', Expected: '%v'", ident, actual, expected)
	}
}

func AssertIdentPtrEqual[T comparable](t *testing.T, ident string, actual *T, expected *T) {
	if actual == nil && expected == nil {
		return
	}
	if actual != nil && expected != nil {
		if *actual != *expected {
			t.Errorf("[%s] values differ: Actual: '%v', Expected: '%v'", ident, *actual, *expected)
		} else {
			return
		}
	}
	if actual == nil && expected != nil {
		t.Errorf("[%s] values differ: Actual: nil, Expected: not-nil", ident)
	}
	if actual != nil && expected == nil {
		t.Errorf("[%s] values differ: Actual: not-nil, Expected: nil", ident)
	}
}

func AssertIdentTrue(t *testing.T, ident string, value bool) {
	if !value {
		t.Errorf("[%s] value should be true", ident)
	}
}

func AssertIdentFalse(t *testing.T, ident string, value bool) {
	if !value {
		t.Errorf("[%s] value should be false", ident)
	}
}
