package tst

import (
	"encoding/hex"
	"fmt"
	"reflect"
	"runtime/debug"
	"testing"
)

func AssertEqual[T comparable](t *testing.T, actual T, expected T) {
	t.Helper()
	if actual != expected {
		t.Errorf("values differ: Actual: '%v', Expected: '%v'", actual, expected)
	}
}

func AssertArrayEqual[T comparable](t *testing.T, actual []T, expected []T) {
	t.Helper()
	if len(actual) != len(expected) {
		t.Errorf("values differ: Actual: '%v', Expected: '%v' (len %d <> %d)", actual, expected, len(actual), len(expected))
		return
	}
	for i := 0; i < len(actual); i++ {
		if actual[i] != expected[i] {
			t.Errorf("values differ: Actual: '%v', Expected: '%v' (at index %d)", actual, expected, i)
			return
		}
	}
}

func AssertNotEqual[T comparable](t *testing.T, actual T, expected T) {
	t.Helper()
	if actual == expected {
		t.Errorf("values do not differ: Actual: '%v', Expected: '%v'", actual, expected)
	}
}

func AssertDeepEqual[T any](t *testing.T, actual T, expected T) {
	t.Helper()
	if !reflect.DeepEqual(actual, expected) {
		t.Errorf("values differ: Actual: '%v', Expected: '%v'", actual, expected)
	}
}

func AssertSetDeepEqual[T any](t *testing.T, actual []T, expected []T) {
	t.Helper()
	if len(actual) != len(expected) {
		t.Errorf("values differ in length: Actual (n=%d): '%v', Expected (n=%d): '%v'", len(actual), actual, len(expected), expected)
	}

	for _, a := range expected {
		found := false
		for _, b := range actual {
			found = found || reflect.DeepEqual(a, b)
		}
		if !found {
			t.Errorf("values differ: Element '%v' not found. Actual: '%v', Expected: '%v'", a, actual, expected)
			return
		}
	}
}

func AssertNotDeepEqual[T any](t *testing.T, actual T, expected T) {
	t.Helper()
	if reflect.DeepEqual(actual, expected) {
		t.Errorf("values do not differ: Actual: '%v', Expected: '%v'", actual, expected)
	}
}

func AssertDeRefEqual[T comparable](t *testing.T, actual *T, expected T) {
	t.Helper()
	if actual == nil {
		t.Errorf("values differ: Actual: NIL, Expected: '%v'", expected)
	}
	if *actual != expected {
		t.Errorf("values differ: Actual: '%v', Expected: '%v'", actual, expected)
	}
}

func AssertPtrEqual[T comparable](t *testing.T, actual *T, expected *T) {
	t.Helper()
	if actual == nil && expected == nil {
		return
	}
	if actual != nil && expected != nil {
		if *actual != *expected {
			t.Errorf("values differ: Actual: '%v', Expected: '%v'", *actual, *expected)
		} else {
			return
		}
	}
	if actual == nil && expected != nil {
		t.Errorf("values differ: Actual: nil, Expected: not-nil")
	}
	if actual != nil && expected == nil {
		t.Errorf("values differ: Actual: not-nil, Expected: nil")
	}
}

func AssertHexEqual(t *testing.T, expected string, actual []byte) {
	t.Helper()
	actualStr := hex.EncodeToString(actual)
	if actualStr != expected {
		t.Errorf("values differ: Actual: '%v', Expected: '%v'", actualStr, expected)
	}
}

func AssertTrue(t *testing.T, value bool) {
	t.Helper()
	if !value {
		t.Error("value should be true\n" + string(debug.Stack()))
	}
}

func AssertFalse(t *testing.T, value bool) {
	t.Helper()
	if value {
		t.Error("value should be false\n" + string(debug.Stack()))
	}
}

func AssertNoErr(t *testing.T, anerr error) {
	t.Helper()
	if anerr != nil {
		t.Error("Function returned an error: " + anerr.Error() + "\n" + string(debug.Stack()))
	}
}

func AssertStrRepEqual(t *testing.T, actual any, expected any) {
	t.Helper()
	if fmt.Sprintf("%v", actual) != fmt.Sprintf("%v", expected) {
		t.Errorf("values differ: Actual: '%v', Expected: '%v'", actual, expected)
	}
}

func AssertStrRepNotEqual(t *testing.T, actual any, expected any) {
	t.Helper()
	if fmt.Sprintf("%v", actual) == fmt.Sprintf("%v", expected) {
		t.Errorf("values do not differ: Actual: '%v', Expected: '%v'", actual, expected)
	}
}
