package reflectext

import (
	"fmt"
	"gogs.mikescher.com/BlackForestBytes/goext/langext"
	"testing"
	"time"
)

func TestConvertStructToMap(t *testing.T) {

	type tst struct {
		FieldA  int
		FieldB  string
		FieldC  time.Time
		FieldD  []float64
		FieldE1 *int
		FieldE2 **int
		FieldE3 *int
		FieldE4 **int
		FieldE5 *int
		FieldE6 **int
	}

	value := tst{
		FieldA:  123,
		FieldB:  "hello",
		FieldC:  time.Date(2020, 05, 12, 8, 30, 0, 0, time.UTC),
		FieldD:  []float64{1, 2, 3, 4, 5, 6, 7},
		FieldE1: nil,
		FieldE2: nil,
		FieldE3: langext.Ptr(12),
		FieldE4: langext.DblPtr(12),
		FieldE5: nil,
		FieldE6: langext.DblPtrNil[int](),
	}

	valueOut := ConvertStructToMap(value, ConvertStructToMapOpt{KeepJsonMarshalTypes: true})

	fmt.Printf("%+v\n", valueOut)

}
