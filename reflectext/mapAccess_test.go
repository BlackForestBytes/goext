package reflectext

import (
	"fmt"
	"gogs.mikescher.com/BlackForestBytes/goext/tst"
	"testing"
)

func TestGetMapPath(t *testing.T) {
	type PseudoInt = int64

	mymap2 := map[string]map[string]any{"Test": {"Second": 3}}

	var maany2 any = mymap2

	tst.AssertEqual(t, fmt.Sprint(GetMapPath[int](maany2, "Test.Second")), "3 true")
	tst.AssertEqual(t, fmt.Sprint(GetMapPath[int](maany2, "Test2.Second")), "0 false")
	tst.AssertEqual(t, fmt.Sprint(GetMapPath[int](maany2, "Test.Second2")), "0 false")
	tst.AssertEqual(t, fmt.Sprint(GetMapPath[string](maany2, "Test.Second")), "false")
	tst.AssertEqual(t, fmt.Sprint(GetMapPath[string](maany2, "Test2.Second")), "false")
	tst.AssertEqual(t, fmt.Sprint(GetMapPath[string](maany2, "Test.Second2")), "false")
	tst.AssertEqual(t, fmt.Sprint(GetMapPath[PseudoInt](maany2, "Test.Second")), "3 true")
	tst.AssertEqual(t, fmt.Sprint(GetMapPath[PseudoInt](maany2, "Test2.Second")), "0 false")
	tst.AssertEqual(t, fmt.Sprint(GetMapPath[PseudoInt](maany2, "Test.Second2")), "0 false")
}

func TestGetMapField(t *testing.T) {
	type PseudoInt = int64

	mymap1 := map[string]any{"Test": 12}
	mymap2 := map[string]int{"Test": 12}

	var maany1 any = mymap1
	var maany2 any = mymap2

	tst.AssertEqual(t, fmt.Sprint(GetMapField[int](maany1, "Test")), "12 true")
	tst.AssertEqual(t, fmt.Sprint(GetMapField[int](maany1, "Test2")), "0 false")
	tst.AssertEqual(t, fmt.Sprint(GetMapField[string](maany1, "Test")), "false")
	tst.AssertEqual(t, fmt.Sprint(GetMapField[string](maany1, "Test2")), "false")
	tst.AssertEqual(t, fmt.Sprint(GetMapField[PseudoInt](maany1, "Test")), "12 true")
	tst.AssertEqual(t, fmt.Sprint(GetMapField[PseudoInt](maany1, "Test2")), "0 false")

	tst.AssertEqual(t, fmt.Sprint(GetMapField[int](maany2, "Test")), "12 true")
	tst.AssertEqual(t, fmt.Sprint(GetMapField[int](maany2, "Test2")), "0 false")
	tst.AssertEqual(t, fmt.Sprint(GetMapField[string](maany2, "Test")), "false")
	tst.AssertEqual(t, fmt.Sprint(GetMapField[string](maany2, "Test2")), "false")
	tst.AssertEqual(t, fmt.Sprint(GetMapField[PseudoInt](maany2, "Test")), "12 true")
	tst.AssertEqual(t, fmt.Sprint(GetMapField[PseudoInt](maany2, "Test2")), "0 false")
}
