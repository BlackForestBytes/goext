package ginext

import (
	"github.com/gin-gonic/gin"
	"gogs.mikescher.com/BlackForestBytes/goext/dataext"
)

func BodyBuffer(g *gin.Context) {
	if g.Request.Body != nil {
		g.Request.Body = dataext.NewBufferedReadCloser(g.Request.Body)
	}
}
