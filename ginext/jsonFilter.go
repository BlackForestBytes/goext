package ginext

import "github.com/gin-gonic/gin"

var jsonFilterKey = "goext.jsonfilter"

func SetJSONFilter(g *gin.Context, filter string) {
	g.Set(jsonFilterKey, filter)
}
