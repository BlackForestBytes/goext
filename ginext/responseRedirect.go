package ginext

import (
	"github.com/gin-gonic/gin"
	"gogs.mikescher.com/BlackForestBytes/goext/langext"
)

type redirectHTTPResponse struct {
	statusCode int
	url        string
	headers    []headerval
	cookies    []cookieval
}

func (j redirectHTTPResponse) Write(g *gin.Context) {
	for _, v := range j.headers {
		g.Header(v.Key, v.Val)
	}
	for _, v := range j.cookies {
		g.SetCookie(v.name, v.value, v.maxAge, v.path, v.domain, v.secure, v.httpOnly)
	}
	g.Redirect(j.statusCode, j.url)
}

func (j redirectHTTPResponse) WithHeader(k string, v string) HTTPResponse {
	j.headers = append(j.headers, headerval{k, v})
	return j
}

func (j redirectHTTPResponse) WithCookie(name string, value string, maxAge int, path string, domain string, secure bool, httpOnly bool) HTTPResponse {
	j.cookies = append(j.cookies, cookieval{name, value, maxAge, path, domain, secure, httpOnly})
	return j
}

func (j redirectHTTPResponse) IsSuccess() bool {
	return j.statusCode >= 200 && j.statusCode <= 399
}

func (j redirectHTTPResponse) Statuscode() int {
	return j.statusCode
}

func (j redirectHTTPResponse) BodyString(*gin.Context) *string {
	return nil
}

func (j redirectHTTPResponse) ContentType() string {
	return ""
}

func (j redirectHTTPResponse) Headers() []string {
	return langext.ArrMap(j.headers, func(v headerval) string { return v.Key + "=" + v.Val })
}

func Redirect(sc int, newURL string) HTTPResponse {
	return &redirectHTTPResponse{statusCode: sc, url: newURL}
}
