package ginext

import (
	"context"
	"github.com/gin-gonic/gin"
)

type SessionObject interface {
	Init(g *gin.Context, ctx *AppContext) error
	Finish(ctx context.Context, resp HTTPResponse) error
}
