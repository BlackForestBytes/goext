package ginext

import (
	"github.com/gin-gonic/gin"
	"gogs.mikescher.com/BlackForestBytes/goext/exerr"
)

type cookieval struct {
	name     string
	value    string
	maxAge   int
	path     string
	domain   string
	secure   bool
	httpOnly bool
}

type headerval struct {
	Key string
	Val string
}

type HTTPResponse interface {
	Write(g *gin.Context)
	WithHeader(k string, v string) HTTPResponse
	WithCookie(name string, value string, maxAge int, path string, domain string, secure bool, httpOnly bool) HTTPResponse
	IsSuccess() bool
}

type InspectableHTTPResponse interface {
	HTTPResponse

	Statuscode() int
	BodyString(g *gin.Context) *string
	ContentType() string
	Headers() []string
}

type HTTPErrorResponse interface {
	HTTPResponse

	Error() error
}

func NotImplemented() HTTPResponse {
	return Error(exerr.New(exerr.TypeNotImplemented, "").Build())
}
