package ginext

import (
	"net/http"
)

func RedirectFound(newuri string) WHandlerFunc {
	return func(pctx PreContext) HTTPResponse {
		return Redirect(http.StatusFound, newuri)
	}
}

func RedirectTemporary(newuri string) WHandlerFunc {
	return func(pctx PreContext) HTTPResponse {
		return Redirect(http.StatusTemporaryRedirect, newuri)
	}
}

func RedirectPermanent(newuri string) WHandlerFunc {
	return func(pctx PreContext) HTTPResponse {
		return Redirect(http.StatusPermanentRedirect, newuri)
	}
}
