package ginext

import (
	"github.com/gin-gonic/gin"
	"gogs.mikescher.com/BlackForestBytes/goext/langext"
)

type emptyHTTPResponse struct {
	statusCode int
	headers    []headerval
	cookies    []cookieval
}

func (j emptyHTTPResponse) Write(g *gin.Context) {
	for _, v := range j.headers {
		g.Header(v.Key, v.Val)
	}
	for _, v := range j.cookies {
		g.SetCookie(v.name, v.value, v.maxAge, v.path, v.domain, v.secure, v.httpOnly)
	}
	g.Status(j.statusCode)
}

func (j emptyHTTPResponse) WithHeader(k string, v string) HTTPResponse {
	j.headers = append(j.headers, headerval{k, v})
	return j
}

func (j emptyHTTPResponse) WithCookie(name string, value string, maxAge int, path string, domain string, secure bool, httpOnly bool) HTTPResponse {
	j.cookies = append(j.cookies, cookieval{name, value, maxAge, path, domain, secure, httpOnly})
	return j
}

func (j emptyHTTPResponse) IsSuccess() bool {
	return j.statusCode >= 200 && j.statusCode <= 399
}

func (j emptyHTTPResponse) Statuscode() int {
	return j.statusCode
}

func (j emptyHTTPResponse) BodyString(*gin.Context) *string {
	return nil
}

func (j emptyHTTPResponse) ContentType() string {
	return ""
}

func (j emptyHTTPResponse) Headers() []string {
	return langext.ArrMap(j.headers, func(v headerval) string { return v.Key + "=" + v.Val })
}

func Status(sc int) HTTPResponse {
	return &emptyHTTPResponse{statusCode: sc}
}
