package ginext

import (
	"github.com/gin-gonic/gin"
	"net/http"
	"strings"
)

func CorsMiddleware(allowheader []string, exposeheader []string) gin.HandlerFunc {
	return func(c *gin.Context) {
		c.Writer.Header().Set("Access-Control-Allow-Origin", "*")
		c.Writer.Header().Set("Access-Control-Allow-Credentials", "true")
		c.Writer.Header().Set("Access-Control-Allow-Headers", strings.Join(allowheader, ", "))
		if len(exposeheader) > 0 {
			c.Writer.Header().Set("Access-Control-Expose-Headers", strings.Join(exposeheader, ", "))
		}
		c.Writer.Header().Set("Access-Control-Allow-Methods", "OPTIONS, GET, POST, PUT, PATCH, DELETE, COUNT")

		if c.Request.Method == "OPTIONS" {
			c.AbortWithStatus(http.StatusOK)
		} else {
			c.Next()
		}
	}
}
