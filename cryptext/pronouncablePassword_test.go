package cryptext

import (
	"fmt"
	"math/rand"
	"testing"
)

func TestPronouncablePasswordExt(t *testing.T) {
	for i := 0; i < 20; i++ {
		pw, entropy := PronouncablePasswordExt(rand.New(rand.NewSource(int64(i))), 16)
		fmt.Printf("[%.2f] => %s\n", entropy, pw)
	}
}

func TestPronouncablePasswordSeeded(t *testing.T) {
	for i := 0; i < 20; i++ {
		pw := PronouncablePasswordSeeded(int64(i), 8)
		fmt.Printf("%s\n", pw)
	}
}

func TestPronouncablePassword(t *testing.T) {
	for i := 0; i < 20; i++ {
		pw := PronouncablePassword(i + 1)
		fmt.Printf("%s\n", pw)
	}
}

func TestPronouncablePasswordWrongLen(t *testing.T) {
	PronouncablePassword(0)
	PronouncablePassword(-1)
	PronouncablePassword(-2)
	PronouncablePassword(-3)
}
