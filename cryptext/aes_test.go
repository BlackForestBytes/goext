package cryptext

import (
	"fmt"
	"gogs.mikescher.com/BlackForestBytes/goext/tst"
	"testing"
)

func TestEncryptAESSimple(t *testing.T) {

	pw := []byte("hunter12")

	str1 := []byte("Hello World")

	str2, err := EncryptAESSimple(pw, str1, 512)
	if err != nil {
		panic(err)
	}

	fmt.Printf("%s\n", str2)

	str3, err := DecryptAESSimple(pw, str2)
	if err != nil {
		panic(err)
	}

	tst.AssertEqual(t, string(str1), string(str3))

	str4, err := EncryptAESSimple(pw, str3, 512)
	if err != nil {
		panic(err)
	}

	tst.AssertNotEqual(t, string(str2), string(str4))

}
