package cryptext

import (
	"crypto/sha256"
	"fmt"
)

func StrSha256(v string) string {
	h := sha256.New()
	h.Write([]byte(v))
	bs := h.Sum(nil)
	sh := fmt.Sprintf("%x", bs)
	return sh
}

func BytesSha256(v []byte) string {
	h := sha256.New()
	h.Write(v)
	bs := h.Sum(nil)
	sh := fmt.Sprintf("%x", bs)
	return sh
}
