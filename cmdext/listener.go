package cmdext

type CommandListener interface {
	ReadRawStdout([]byte)
	ReadRawStderr([]byte)

	ReadStdoutLine(string)
	ReadStderrLine(string)

	Finished(int)
	Timeout()
}

type genericCommandListener struct {
	_readRawStdout  *func([]byte)
	_readRawStderr  *func([]byte)
	_readStdoutLine *func(string)
	_readStderrLine *func(string)
	_finished       *func(int)
	_timeout        *func()
}

func (g genericCommandListener) ReadRawStdout(v []byte) {
	if g._readRawStdout != nil {
		(*g._readRawStdout)(v)
	}
}

func (g genericCommandListener) ReadRawStderr(v []byte) {
	if g._readRawStderr != nil {
		(*g._readRawStderr)(v)
	}
}

func (g genericCommandListener) ReadStdoutLine(v string) {
	if g._readStdoutLine != nil {
		(*g._readStdoutLine)(v)
	}
}

func (g genericCommandListener) ReadStderrLine(v string) {
	if g._readStderrLine != nil {
		(*g._readStderrLine)(v)
	}
}

func (g genericCommandListener) Finished(v int) {
	if g._finished != nil {
		(*g._finished)(v)
	}
}

func (g genericCommandListener) Timeout() {
	if g._timeout != nil {
		(*g._timeout)()
	}
}
