package cmdext

import "time"

func RunCommand(program string, args []string, timeout *time.Duration) (CommandResult, error) {
	b := Runner(program)
	b = b.Args(args)
	if timeout != nil {
		b = b.Timeout(*timeout)
	}
	return b.Run()
}
