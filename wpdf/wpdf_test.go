package wpdf

import (
	_ "embed"
	"fmt"
	"gogs.mikescher.com/BlackForestBytes/goext/imageext"
	"gogs.mikescher.com/BlackForestBytes/goext/langext"
	"os"
	"path"
	"testing"
)

//go:embed logo.png
var logoData []byte

func TestPDFBuilder(t *testing.T) {
	builder := NewPDFBuilder(Portrait, SizeA4, true)

	builder.Debug(true)

	logoRef := builder.RegisterImage(logoData)

	builder.SetMargins(PDFMargins{Left: 15, Top: 40, Right: 10})
	builder.AddPage()

	builder.SetFont(FontHelvetica, Normal, 10)
	builder.Cell("Neueinrichtung deiner Entgeltumwandlung", NewPDFCellOpt().Bold().FontSize(20))
	builder.Ln(10)

	builder.SetFont(FontHelvetica, Normal, 10)
	builder.Cell("Hello World", NewPDFCellOpt().Width(50).Align(AlignHorzCenter).LnPos(BreakToRight))
	builder.IncX(10)
	builder.Cell("Second Text", NewPDFCellOpt().AutoWidth().AutoWidthPaddingX(2).LnPos(BreakToRight))
	builder.Ln(10)

	builder.MultiCell("Im Fall einer individuellen Entgeltumwandlung ist die Zuschussverpflichtung auf der Grundlage des Betriebsrentenstärkungsgesetzes in der gesetzlich vorgeschriebenen Höhe (§ 1a Abs. 1a BetrAVG), über den arbeitgeberfinanzierten Zuschuss erfüllt.")
	builder.Ln(4)

	builder.Image(logoRef, NewPDFImageOpt().X(90).Y(160).Width(70).Height(30).ImageFit(imageext.ImageFitContainCenter))

	builder.Ln(4)

	cellStyleHeader := &TableCellStyleOpt{
		PDFCellOpt: *NewPDFCellOpt().
			FontSize(float64(8)).
			BorderColorHex(uint32(0x666666)).
			Border(BorderFull).
			FillColorHex(uint32(0xC0C0C0)).
			FillBackground(true).
			TextColorHex(uint32(0x000000)).
			Align(AlignHorzCenter).
			Bold(),
		minWidth:  langext.Ptr(float64(5)),
		ellipsize: langext.PTrue,
		multiCell: langext.PFalse,
	}

	cellStyleMulti := &TableCellStyleOpt{
		PDFCellOpt: *NewPDFCellOpt().
			FontSize(float64(8)).
			BorderColorHex(uint32(0x666666)).
			Border(BorderFull).
			FillColorHex(uint32(0xC060C0)).
			FillBackground(true).
			TextColorHex(uint32(0x000000)),
		minWidth:  langext.Ptr(float64(5)),
		ellipsize: langext.PFalse,
		multiCell: langext.PTrue,
	}

	builder.Table().
		Widths("auto", "20", "1fr", "20").
		PadX(2).
		PadY(2).
		AddRowWithStyle(cellStyleHeader, "test", "hello", "123", "end").
		AddRowDefaultStyle("test", "hello", "123", "end").
		AddRowDefaultStyle("123", "helasdsalo", "a", "enwqad").
		AddRowDefaultStyle("123asd", "TrimMeTrimMeTrimMeTrimMe", "a", "enwqad").
		AddRowWithStyle(cellStyleMulti, "123", "helasdsalo", "a", "MultiCell: enwqad enw\nqad enwqad enwqad enwqad enwqad").
		AddRowDefaultStyle("123", "helasdsalo", "a", "enwqad").
		Debug(false).
		Build()

	builder.Ln(8)

	builder.Table().
		Widths("auto", "20", "1fr", "20").
		PadX(2).
		PadY(2).
		BuildRow().RowStyle(cellStyleHeader).Cells("test", "hello", "123", "end").BuildRow().
		BuildRow().Cells("test", "hello", "123", "end").BuildRow().
		BuildRow().RowStyle(cellStyleMulti.FillHeight(true)).Cell("123").Cell("helasdsalo").Cell("a").Cell("MultiCell: enwqad enw\nqad enwqad enwqad enwqad enwqad").BuildRow().
		AddRowDefaultStyle("123", "helasdsalo", "a", "enwqad").
		Debug(false).
		Build()

	bin, err := builder.Build()
	if err != nil {
		t.Fatal(err)
	}

	fn := "wpdf_test.pdf"
	_ = os.WriteFile(fn, bin, 0644)
	fmt.Println("file://" + path.Join(langext.Must(os.Getwd()), fn))
}
