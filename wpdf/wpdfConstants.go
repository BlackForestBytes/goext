package wpdf

type PDFOrientation string

const (
	Portrait  PDFOrientation = "P"
	Landscape PDFOrientation = "L"
)

type PDFSize string

const (
	SizeA3      PDFSize = "A3"
	SizeA4      PDFSize = "A4"
	SizeA5      PDFSize = "A4"
	SizeLetter  PDFSize = "Letter"
	SizeLegal   PDFSize = "Legal"
	SizeTabloid PDFSize = "Tabloid"
)

type PDFFontFamily string

const (
	FontCourier      PDFFontFamily = "courier"
	FontHelvetica    PDFFontFamily = "helvetica"
	FontTimes        PDFFontFamily = "times"
	FontZapfDingbats PDFFontFamily = "zapfdingbats"
	FontSymbol       PDFFontFamily = "symbol"
)

type PDFFontStyle string

const (
	Normal     PDFFontStyle = ""
	Bold       PDFFontStyle = "B"
	Italic     PDFFontStyle = "I"
	BoldItalic PDFFontStyle = "IB"
)

type PDFBorder string

const (
	BorderNone   PDFBorder = ""
	BorderFull   PDFBorder = "1"
	BorderLeft   PDFBorder = "L"
	BorderTop    PDFBorder = "T"
	BorderRight  PDFBorder = "R"
	BorderBottom PDFBorder = "B"
	BorderTLR    PDFBorder = "TLR"
	BorderLR     PDFBorder = "LR"
)

type PDFTextBreak int

const (
	BreakToRight    PDFTextBreak = 0
	BreakToNextLine PDFTextBreak = 1
	BreakToBelow    PDFTextBreak = 2
)

type PDFTextAlign string

const (
	AlignLeft       PDFTextAlign = "L"
	AlignHorzCenter PDFTextAlign = "C"
	AlignRight      PDFTextAlign = "R"
)

type PDFRectStyle string

const (
	RectFill        PDFRectStyle = "F"
	RectOutline     PDFRectStyle = "D"
	RectFillOutline PDFRectStyle = "FD"
)

type PDFBlendMode string

const (
	BlendNormal     PDFBlendMode = "Normal"
	BlendMultiply   PDFBlendMode = "Multiply"
	BlendScreen     PDFBlendMode = "Screen"
	BlendOverlay    PDFBlendMode = "Overlay"
	BlendDarken     PDFBlendMode = "Darken"
	BlendLighten    PDFBlendMode = "Lighten"
	BlendColorDodge PDFBlendMode = "ColorDodge"
	BlendColorBurn  PDFBlendMode = "ColorBurn"
	BlendHardLight  PDFBlendMode = "HardLight"
	BlendSoftLight  PDFBlendMode = "SoftLight"
	BlendDifference PDFBlendMode = "Difference"
	BlendExclusion  PDFBlendMode = "Exclusion"
	BlendHue        PDFBlendMode = "Hue"
	BlendSaturation PDFBlendMode = "Saturation"
	BlendColor      PDFBlendMode = "Color"
	BlendLuminosity PDFBlendMode = "Luminosity"
)

type PDFLineCapStyle string

const (
	CapButt   PDFLineCapStyle = "butt"
	CapRound  PDFLineCapStyle = "round"
	CapSquare PDFLineCapStyle = "square"
)

const (
	BackgroundFill        = true
	BackgroundTransparent = false
)
