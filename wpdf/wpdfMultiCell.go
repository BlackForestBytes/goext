package wpdf

import (
	"gogs.mikescher.com/BlackForestBytes/goext/dataext"
	"gogs.mikescher.com/BlackForestBytes/goext/langext"
)

type PDFMultiCellOpt struct {
	width             *float64
	height            *float64
	border            *PDFBorder
	align             *PDFTextAlign
	fill              *bool
	fontNameOverride  *PDFFontFamily
	fontStyleOverride *PDFFontStyle
	fontSizeOverride  *float64
	alphaOverride     *dataext.Tuple[float64, PDFBlendMode]
	extraLn           *float64
	x                 *float64
	textColor         *PDFColor
	borderColor       *PDFColor
	fillColor         *PDFColor
	debug             *bool
}

func NewPDFMultiCellOpt() *PDFMultiCellOpt {
	return &PDFMultiCellOpt{}
}

func (opt *PDFMultiCellOpt) Width(v float64) *PDFMultiCellOpt {
	opt.width = &v
	return opt
}

func (opt *PDFMultiCellOpt) Height(v float64) *PDFMultiCellOpt {
	opt.height = &v
	return opt
}

func (opt *PDFMultiCellOpt) Border(v PDFBorder) *PDFMultiCellOpt {
	opt.border = &v
	return opt
}

func (opt *PDFMultiCellOpt) Align(v PDFTextAlign) *PDFMultiCellOpt {
	opt.align = &v
	return opt
}

func (opt *PDFMultiCellOpt) FillBackground(v bool) *PDFMultiCellOpt {
	opt.fill = &v
	return opt
}

func (opt *PDFMultiCellOpt) Font(fontName PDFFontFamily, fontStyle PDFFontStyle, fontSize float64) *PDFMultiCellOpt {
	opt.fontNameOverride = &fontName
	opt.fontStyleOverride = &fontStyle
	opt.fontSizeOverride = &fontSize
	return opt
}

func (opt *PDFMultiCellOpt) FontName(v PDFFontFamily) *PDFMultiCellOpt {
	opt.fontNameOverride = &v
	return opt
}

func (opt *PDFMultiCellOpt) FontStyle(v PDFFontStyle) *PDFMultiCellOpt {
	opt.fontStyleOverride = &v
	return opt
}

func (opt *PDFMultiCellOpt) FontSize(v float64) *PDFMultiCellOpt {
	opt.fontSizeOverride = &v
	return opt
}

func (opt *PDFMultiCellOpt) Bold() *PDFMultiCellOpt {
	opt.fontStyleOverride = langext.Ptr(Bold)
	return opt
}

func (opt *PDFMultiCellOpt) Italic() *PDFMultiCellOpt {
	opt.fontStyleOverride = langext.Ptr(Italic)
	return opt
}

func (opt *PDFMultiCellOpt) LnAfter(v float64) *PDFMultiCellOpt {
	opt.extraLn = &v
	return opt
}

func (opt *PDFMultiCellOpt) X(v float64) *PDFMultiCellOpt {
	opt.x = &v
	return opt
}

func (opt *PDFMultiCellOpt) TextColor(cr, cg, cb int) *PDFMultiCellOpt {
	opt.textColor = langext.Ptr(rgbToColor(cr, cg, cb))
	return opt
}

func (opt *PDFMultiCellOpt) TextColorHex(c uint32) *PDFMultiCellOpt {
	opt.textColor = langext.Ptr(hexToColor(c))
	return opt
}

func (opt *PDFMultiCellOpt) BorderColor(cr, cg, cb int) *PDFMultiCellOpt {
	opt.borderColor = langext.Ptr(rgbToColor(cr, cg, cb))
	return opt
}

func (opt *PDFMultiCellOpt) BorderColorHex(c uint32) *PDFMultiCellOpt {
	opt.borderColor = langext.Ptr(hexToColor(c))
	return opt
}

func (opt *PDFMultiCellOpt) FillColor(cr, cg, cb int) *PDFMultiCellOpt {
	opt.fillColor = langext.Ptr(rgbToColor(cr, cg, cb))
	return opt
}

func (opt *PDFMultiCellOpt) FillColorHex(c uint32) *PDFMultiCellOpt {
	opt.fillColor = langext.Ptr(hexToColor(c))
	return opt
}

func (opt *PDFMultiCellOpt) Alpha(alpha float64, blendMode PDFBlendMode) *PDFMultiCellOpt {
	opt.alphaOverride = &dataext.Tuple[float64, PDFBlendMode]{V1: alpha, V2: blendMode}
	return opt
}

func (opt *PDFMultiCellOpt) Debug(v bool) *PDFMultiCellOpt {
	opt.debug = &v
	return opt
}

func (opt *PDFMultiCellOpt) Copy() *PDFMultiCellOpt {
	c := *opt
	return &c
}

func (b *WPDFBuilder) MultiCell(txt string, opts ...*PDFMultiCellOpt) {

	txtTR := b.tr(txt)

	width := float64(0)
	height := b.cellHeight + b.cellSpacing
	border := BorderNone
	align := AlignLeft
	fill := false
	var fontNameOverride *PDFFontFamily
	var fontStyleOverride *PDFFontStyle
	var fontSizeOverride *float64
	var alphaOverride *dataext.Tuple[float64, PDFBlendMode]
	extraLn := float64(0)
	var x *float64
	var textColor *PDFColor
	var borderColor *PDFColor
	var fillColor *PDFColor
	debug := b.debug

	for _, opt := range opts {
		width = langext.Coalesce(opt.width, width)
		height = langext.Coalesce(opt.height, height)
		border = langext.Coalesce(opt.border, border)
		align = langext.Coalesce(opt.align, align)
		fill = langext.Coalesce(opt.fill, fill)
		fontNameOverride = langext.CoalesceOpt(opt.fontNameOverride, fontNameOverride)
		fontStyleOverride = langext.CoalesceOpt(opt.fontStyleOverride, fontStyleOverride)
		fontSizeOverride = langext.CoalesceOpt(opt.fontSizeOverride, fontSizeOverride)
		alphaOverride = langext.CoalesceOpt(opt.alphaOverride, alphaOverride)
		extraLn = langext.Coalesce(opt.extraLn, extraLn)
		x = langext.CoalesceOpt(opt.x, x)
		textColor = langext.CoalesceOpt(opt.textColor, textColor)
		borderColor = langext.CoalesceOpt(opt.borderColor, borderColor)
		fillColor = langext.CoalesceOpt(opt.fillColor, fillColor)
		debug = langext.Coalesce(opt.debug, debug)
	}

	if fontNameOverride != nil || fontStyleOverride != nil || fontSizeOverride != nil {
		oldFontName := b.fontName
		oldFontStyle := b.fontStyle
		oldFontSize := b.fontSize
		newFontName := langext.Coalesce(fontNameOverride, oldFontName)
		newFontStyle := langext.Coalesce(fontStyleOverride, oldFontStyle)
		newFontSize := langext.Coalesce(fontSizeOverride, oldFontSize)
		b.SetFont(newFontName, newFontStyle, newFontSize)
		defer func() { b.SetFont(oldFontName, oldFontStyle, oldFontSize) }()
	}

	if textColor != nil {
		oldColorR, oldColorG, oldColorB := b.b.GetTextColor()
		b.SetTextColor(textColor.R, textColor.G, textColor.B)
		defer func() { b.SetTextColor(oldColorR, oldColorG, oldColorB) }()
	}

	if borderColor != nil {
		oldColorR, oldColorG, oldColorB := b.b.GetDrawColor()
		b.SetDrawColor(borderColor.R, borderColor.G, borderColor.B)
		defer func() { b.SetDrawColor(oldColorR, oldColorG, oldColorB) }()
	}

	if fillColor != nil {
		oldColorR, oldColorG, oldColorB := b.b.GetFillColor()
		b.SetFillColor(fillColor.R, fillColor.G, fillColor.B)
		defer func() { b.SetFillColor(oldColorR, oldColorG, oldColorB) }()
	}

	if alphaOverride != nil {
		oldA, oldBMS := b.b.GetAlpha()
		b.b.SetAlpha(alphaOverride.V1, string(alphaOverride.V2))
		defer func() { b.b.SetAlpha(oldA, oldBMS) }()
	}

	if x != nil {
		b.b.SetX(*x)
	}

	xBefore, yBefore := b.b.GetXY()

	b.b.MultiCell(width, height, txtTR, string(border), string(align), fill)

	if debug {
		b.Rect(b.GetPageWidth()-xBefore-b.GetMarginRight(), b.GetY()-yBefore, RectOutline, NewPDFRectOpt().X(xBefore).Y(yBefore).LineWidth(0.25).DrawColor(0, 128, 0))
	}

	if extraLn != 0 {
		b.b.Ln(extraLn)
	}
}
