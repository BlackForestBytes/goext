package wpdf

import (
	"gogs.mikescher.com/BlackForestBytes/goext/dataext"
	"gogs.mikescher.com/BlackForestBytes/goext/langext"
)

type PDFCellOpt struct {
	width             *float64
	height            *float64
	border            *PDFBorder
	ln                *PDFTextBreak
	align             *PDFTextAlign
	fill              *bool
	link              *int
	linkStr           *string
	fontNameOverride  *PDFFontFamily
	fontStyleOverride *PDFFontStyle
	fontSizeOverride  *float64
	alphaOverride     *dataext.Tuple[float64, PDFBlendMode]
	extraLn           *float64
	x                 *float64
	autoWidth         *bool
	textColor         *PDFColor
	borderColor       *PDFColor
	fillColor         *PDFColor
	autoWidthPaddingX *float64
	debug             *bool
}

func NewPDFCellOpt() *PDFCellOpt {
	return &PDFCellOpt{}
}

func (opt *PDFCellOpt) Width(v float64) *PDFCellOpt {
	opt.width = &v
	return opt
}

func (opt *PDFCellOpt) Height(v float64) *PDFCellOpt {
	opt.height = &v
	return opt
}

func (opt *PDFCellOpt) Border(v PDFBorder) *PDFCellOpt {
	opt.border = &v
	return opt
}

func (opt *PDFCellOpt) LnPos(v PDFTextBreak) *PDFCellOpt {
	opt.ln = &v
	return opt
}

func (opt *PDFCellOpt) Align(v PDFTextAlign) *PDFCellOpt {
	opt.align = &v
	return opt
}

func (opt *PDFCellOpt) FillBackground(v bool) *PDFCellOpt {
	opt.fill = &v
	return opt
}

func (opt *PDFCellOpt) Link(v int) *PDFCellOpt {
	opt.link = &v
	return opt
}

func (opt *PDFCellOpt) LinkStr(v string) *PDFCellOpt {
	opt.linkStr = &v
	return opt
}

func (opt *PDFCellOpt) Font(fontName PDFFontFamily, fontStyle PDFFontStyle, fontSize float64) *PDFCellOpt {
	opt.fontNameOverride = &fontName
	opt.fontStyleOverride = &fontStyle
	opt.fontSizeOverride = &fontSize
	return opt
}

func (opt *PDFCellOpt) FontName(v PDFFontFamily) *PDFCellOpt {
	opt.fontNameOverride = &v
	return opt
}

func (opt *PDFCellOpt) FontStyle(v PDFFontStyle) *PDFCellOpt {
	opt.fontStyleOverride = &v
	return opt
}

func (opt *PDFCellOpt) FontSize(v float64) *PDFCellOpt {
	opt.fontSizeOverride = &v
	return opt
}

func (opt *PDFCellOpt) Bold() *PDFCellOpt {
	opt.fontStyleOverride = langext.Ptr(Bold)
	return opt
}

func (opt *PDFCellOpt) Italic() *PDFCellOpt {
	opt.fontStyleOverride = langext.Ptr(Italic)
	return opt
}

func (opt *PDFCellOpt) LnAfter(v float64) *PDFCellOpt {
	opt.extraLn = &v
	return opt
}

func (opt *PDFCellOpt) X(v float64) *PDFCellOpt {
	opt.x = &v
	return opt
}

func (opt *PDFCellOpt) AutoWidth() *PDFCellOpt {
	opt.autoWidth = langext.PTrue
	return opt
}

func (opt *PDFCellOpt) AutoWidthPaddingX(v float64) *PDFCellOpt {
	opt.autoWidthPaddingX = &v
	return opt
}

func (opt *PDFCellOpt) TextColor(cr, cg, cb int) *PDFCellOpt {
	opt.textColor = langext.Ptr(rgbToColor(cr, cg, cb))
	return opt
}

func (opt *PDFCellOpt) TextColorHex(c uint32) *PDFCellOpt {
	opt.textColor = langext.Ptr(hexToColor(c))
	return opt
}

func (opt *PDFCellOpt) BorderColor(cr, cg, cb int) *PDFCellOpt {
	opt.borderColor = langext.Ptr(rgbToColor(cr, cg, cb))
	return opt
}

func (opt *PDFCellOpt) BorderColorHex(c uint32) *PDFCellOpt {
	opt.borderColor = langext.Ptr(hexToColor(c))
	return opt
}

func (opt *PDFCellOpt) FillColor(cr, cg, cb int) *PDFCellOpt {
	opt.fillColor = langext.Ptr(rgbToColor(cr, cg, cb))
	return opt
}

func (opt *PDFCellOpt) FillColorHex(c uint32) *PDFCellOpt {
	opt.fillColor = langext.Ptr(hexToColor(c))
	return opt
}

func (opt *PDFCellOpt) Alpha(alpha float64, blendMode PDFBlendMode) *PDFCellOpt {
	opt.alphaOverride = &dataext.Tuple[float64, PDFBlendMode]{V1: alpha, V2: blendMode}
	return opt
}

func (opt *PDFCellOpt) Debug(v bool) *PDFCellOpt {
	opt.debug = &v
	return opt
}

func (opt *PDFCellOpt) Copy() *PDFCellOpt {
	c := *opt
	return &c
}

func (opt *PDFCellOpt) ToMulti() *PDFMultiCellOpt {
	return &PDFMultiCellOpt{
		width:             opt.width,
		height:            opt.height,
		border:            opt.border,
		align:             opt.align,
		fill:              opt.fill,
		fontNameOverride:  opt.fontNameOverride,
		fontStyleOverride: opt.fontStyleOverride,
		fontSizeOverride:  opt.fontSizeOverride,
		extraLn:           opt.extraLn,
		x:                 opt.x,
		textColor:         opt.textColor,
		borderColor:       opt.borderColor,
		fillColor:         opt.fillColor,
	}
}

func (b *WPDFBuilder) Cell(txt string, opts ...*PDFCellOpt) {

	txtTR := b.tr(txt)

	width := float64(0)
	var height *float64 = nil
	border := BorderNone
	ln := BreakToNextLine
	align := AlignLeft
	fill := false
	link := 0
	linkStr := ""
	var fontNameOverride *PDFFontFamily
	var fontStyleOverride *PDFFontStyle
	var fontSizeOverride *float64
	var alphaOverride *dataext.Tuple[float64, PDFBlendMode]
	extraLn := float64(0)
	var x *float64
	autoWidth := false
	var textColor *PDFColor
	var borderColor *PDFColor
	var fillColor *PDFColor
	autoWidthPaddingX := float64(0)
	debug := b.debug

	for _, opt := range opts {
		width = langext.Coalesce(opt.width, width)
		height = langext.CoalesceOpt(opt.height, height)
		border = langext.Coalesce(opt.border, border)
		ln = langext.Coalesce(opt.ln, ln)
		align = langext.Coalesce(opt.align, align)
		fill = langext.Coalesce(opt.fill, fill)
		link = langext.Coalesce(opt.link, link)
		linkStr = langext.Coalesce(opt.linkStr, linkStr)
		fontNameOverride = langext.CoalesceOpt(opt.fontNameOverride, fontNameOverride)
		fontStyleOverride = langext.CoalesceOpt(opt.fontStyleOverride, fontStyleOverride)
		fontSizeOverride = langext.CoalesceOpt(opt.fontSizeOverride, fontSizeOverride)
		alphaOverride = langext.CoalesceOpt(opt.alphaOverride, alphaOverride)
		extraLn = langext.Coalesce(opt.extraLn, extraLn)
		x = langext.CoalesceOpt(opt.x, x)
		autoWidth = langext.Coalesce(opt.autoWidth, autoWidth)
		textColor = langext.CoalesceOpt(opt.textColor, textColor)
		borderColor = langext.CoalesceOpt(opt.borderColor, borderColor)
		fillColor = langext.CoalesceOpt(opt.fillColor, fillColor)
		autoWidthPaddingX = langext.Coalesce(opt.autoWidthPaddingX, autoWidthPaddingX)
		debug = langext.Coalesce(opt.debug, debug)
	}

	if fontNameOverride != nil || fontStyleOverride != nil || fontSizeOverride != nil {
		oldFontName := b.fontName
		oldFontStyle := b.fontStyle
		oldFontSize := b.fontSize
		newFontName := langext.Coalesce(fontNameOverride, oldFontName)
		newFontStyle := langext.Coalesce(fontStyleOverride, oldFontStyle)
		newFontSize := langext.Coalesce(fontSizeOverride, oldFontSize)
		b.SetFont(newFontName, newFontStyle, newFontSize)
		defer func() { b.SetFont(oldFontName, oldFontStyle, oldFontSize) }()
	}

	if height == nil {
		// (do after SetFont, so that b.cellHeight is correctly set to fontOverride)
		height = langext.Ptr(b.cellHeight + b.cellSpacing)
	}

	if textColor != nil {
		oldColorR, oldColorG, oldColorB := b.b.GetTextColor()
		b.SetTextColor(textColor.R, textColor.G, textColor.B)
		defer func() { b.SetTextColor(oldColorR, oldColorG, oldColorB) }()
	}

	if borderColor != nil {
		oldColorR, oldColorG, oldColorB := b.b.GetDrawColor()
		b.SetDrawColor(borderColor.R, borderColor.G, borderColor.B)
		defer func() { b.SetDrawColor(oldColorR, oldColorG, oldColorB) }()
	}

	if fillColor != nil {
		oldColorR, oldColorG, oldColorB := b.b.GetFillColor()
		b.SetFillColor(fillColor.R, fillColor.G, fillColor.B)
		defer func() { b.SetFillColor(oldColorR, oldColorG, oldColorB) }()
	}

	if alphaOverride != nil {
		oldA, oldBMS := b.b.GetAlpha()
		b.b.SetAlpha(alphaOverride.V1, string(alphaOverride.V2))
		defer func() { b.b.SetAlpha(oldA, oldBMS) }()
	}

	if x != nil {
		b.b.SetX(*x)
	}

	if autoWidth {
		width = b.GetStringWidth(txtTR, langext.ArrDeRef(opts)...) + autoWidthPaddingX
	}

	xBefore, yBefore := b.b.GetXY()

	b.b.CellFormat(width, *height, txtTR, string(border), int(ln), string(align), fill, link, linkStr)

	if debug {
		if ln == BreakToNextLine {
			b.Rect(b.GetPageWidth()-xBefore-b.GetMarginRight(), *height, RectOutline, NewPDFRectOpt().X(xBefore).Y(yBefore).LineWidth(0.25).DrawColor(0, 128, 0))
		} else if ln == BreakToRight {
			b.Rect(b.GetX()-xBefore, *height, RectOutline, NewPDFRectOpt().X(xBefore).Y(yBefore).LineWidth(0.25).DrawColor(0, 128, 0))
		} else if ln == BreakToBelow {
			b.Rect(b.GetPageWidth()-xBefore-b.GetMarginRight(), *height, RectOutline, NewPDFRectOpt().X(xBefore).Y(yBefore).LineWidth(0.25).DrawColor(0, 128, 0))
		}
	}

	if extraLn != 0 {
		b.b.Ln(extraLn)
	}
}
