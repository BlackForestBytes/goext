package wpdf

import "gogs.mikescher.com/BlackForestBytes/goext/langext"

type tableRow struct {
	cells []TableCell
}

type TableRowBuilder struct {
	tabbuilder   *TableBuilder
	defaultStyle *TableCellStyleOpt
	cells        []TableCell
}

func (r *TableRowBuilder) RowStyle(style *TableCellStyleOpt) *TableRowBuilder {
	r.defaultStyle = style
	return r
}

func (r *TableRowBuilder) Cell(cell string) *TableRowBuilder {
	r.cells = append(r.cells, TableCell{Content: cell, Style: langext.Coalesce3(r.defaultStyle, r.tabbuilder.defaultCellStyle, TableCellStyleOpt{})})
	return r
}

func (r *TableRowBuilder) Cells(cells ...string) *TableRowBuilder {
	for _, cell := range cells {
		r.cells = append(r.cells, TableCell{Content: cell, Style: langext.Coalesce3(r.defaultStyle, r.tabbuilder.defaultCellStyle, TableCellStyleOpt{})})
	}
	return r
}

func (r *TableRowBuilder) CellObject(cell TableCell) *TableRowBuilder {
	r.cells = append(r.cells, cell)
	return r
}

func (r *TableRowBuilder) CellObjects(cells ...TableCell) *TableRowBuilder {
	for _, cell := range cells {
		r.cells = append(r.cells, cell)
	}
	return r
}

func (r *TableRowBuilder) CellWithStyle(cell string, style *TableCellStyleOpt) *TableRowBuilder {
	r.cells = append(r.cells, TableCell{Content: cell, Style: *style})
	return r
}

func (r *TableRowBuilder) BuildRow() *TableBuilder {
	r.tabbuilder.AddRow(r.cells...)
	return r.tabbuilder
}
