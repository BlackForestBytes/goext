package wpdf

import (
	"gogs.mikescher.com/BlackForestBytes/goext/dataext"
	"gogs.mikescher.com/BlackForestBytes/goext/langext"
)

type TableCell struct {
	Content string
	Style   TableCellStyleOpt
}

type TableCellStyleOpt struct {
	multiCell   *bool
	ellipsize   *bool
	paddingHorz *float64
	minWidth    *float64
	fillHeight  *bool

	PDFCellOpt
}

func NewTableCellStyleOpt() *TableCellStyleOpt {
	return &TableCellStyleOpt{}
}

func (o *TableCellStyleOpt) FillHeight(b bool) *TableCellStyleOpt {
	o.fillHeight = &b
	return o
}

func (o *TableCellStyleOpt) MultiCell(v bool) *TableCellStyleOpt {
	o.multiCell = &v
	return o
}

func (o *TableCellStyleOpt) Ellipsize(v bool) *TableCellStyleOpt {
	o.ellipsize = &v
	return o
}

func (o *TableCellStyleOpt) PaddingHorz(v float64) *TableCellStyleOpt {
	o.paddingHorz = &v
	return o
}

func (o *TableCellStyleOpt) MinWidth(v float64) *TableCellStyleOpt {
	o.minWidth = &v
	return o
}

func (o *TableCellStyleOpt) CellStyle(v PDFCellOpt) *TableCellStyleOpt {
	o.PDFCellOpt = v
	return o
}

func (o *TableCellStyleOpt) Width(v float64) *TableCellStyleOpt {
	o.PDFCellOpt.width = &v
	return o
}

func (o *TableCellStyleOpt) Height(v float64) *TableCellStyleOpt {
	o.PDFCellOpt.height = &v
	return o
}

func (o *TableCellStyleOpt) Border(v PDFBorder) *TableCellStyleOpt {
	o.PDFCellOpt.border = &v
	return o
}

func (o *TableCellStyleOpt) LnPos(v PDFTextBreak) *TableCellStyleOpt {
	o.PDFCellOpt.ln = &v
	return o
}

func (o *TableCellStyleOpt) Align(v PDFTextAlign) *TableCellStyleOpt {
	o.PDFCellOpt.align = &v
	return o
}

func (o *TableCellStyleOpt) FillBackground(v bool) *TableCellStyleOpt {
	o.PDFCellOpt.fill = &v
	return o
}

func (o *TableCellStyleOpt) Link(v int) *TableCellStyleOpt {
	o.PDFCellOpt.link = &v
	return o
}

func (o *TableCellStyleOpt) LinkStr(v string) *TableCellStyleOpt {
	o.PDFCellOpt.linkStr = &v
	return o
}

func (o *TableCellStyleOpt) Font(fontName PDFFontFamily, fontStyle PDFFontStyle, fontSize float64) *TableCellStyleOpt {
	o.PDFCellOpt.fontNameOverride = &fontName
	o.PDFCellOpt.fontStyleOverride = &fontStyle
	o.PDFCellOpt.fontSizeOverride = &fontSize
	return o
}

func (o *TableCellStyleOpt) FontName(v PDFFontFamily) *TableCellStyleOpt {
	o.PDFCellOpt.fontNameOverride = &v
	return o
}

func (o *TableCellStyleOpt) FontStyle(v PDFFontStyle) *TableCellStyleOpt {
	o.PDFCellOpt.fontStyleOverride = &v
	return o
}

func (o *TableCellStyleOpt) FontSize(v float64) *TableCellStyleOpt {
	o.PDFCellOpt.fontSizeOverride = &v
	return o
}

func (o *TableCellStyleOpt) Bold() *TableCellStyleOpt {
	o.PDFCellOpt.fontStyleOverride = langext.Ptr(Bold)
	return o
}

func (o *TableCellStyleOpt) Italic() *TableCellStyleOpt {
	o.PDFCellOpt.fontStyleOverride = langext.Ptr(Italic)
	return o
}

func (o *TableCellStyleOpt) LnAfter(v float64) *TableCellStyleOpt {
	o.PDFCellOpt.extraLn = &v
	return o
}

func (o *TableCellStyleOpt) X(v float64) *TableCellStyleOpt {
	o.PDFCellOpt.x = &v
	return o
}

func (o *TableCellStyleOpt) AutoWidth() *TableCellStyleOpt {
	o.PDFCellOpt.autoWidth = langext.PTrue
	return o
}

func (o *TableCellStyleOpt) AutoWidthPaddingX(v float64) *TableCellStyleOpt {
	o.PDFCellOpt.autoWidthPaddingX = &v
	return o
}

func (o *TableCellStyleOpt) TextColor(cr, cg, cb int) *TableCellStyleOpt {
	o.PDFCellOpt.textColor = langext.Ptr(rgbToColor(cr, cg, cb))
	return o
}

func (o *TableCellStyleOpt) TextColorHex(c uint32) *TableCellStyleOpt {
	o.PDFCellOpt.textColor = langext.Ptr(hexToColor(c))
	return o
}

func (o *TableCellStyleOpt) BorderColor(cr, cg, cb int) *TableCellStyleOpt {
	o.PDFCellOpt.borderColor = langext.Ptr(rgbToColor(cr, cg, cb))
	return o
}

func (o *TableCellStyleOpt) BorderColorHex(c uint32) *TableCellStyleOpt {
	o.PDFCellOpt.borderColor = langext.Ptr(hexToColor(c))
	return o
}

func (o *TableCellStyleOpt) FillColor(cr, cg, cb int) *TableCellStyleOpt {
	o.PDFCellOpt.fillColor = langext.Ptr(rgbToColor(cr, cg, cb))
	return o
}

func (o *TableCellStyleOpt) FillColorHex(c uint32) *TableCellStyleOpt {
	o.PDFCellOpt.fillColor = langext.Ptr(hexToColor(c))
	return o
}

func (o *TableCellStyleOpt) Alpha(alpha float64, blendMode PDFBlendMode) *TableCellStyleOpt {
	o.PDFCellOpt.alphaOverride = &dataext.Tuple[float64, PDFBlendMode]{V1: alpha, V2: blendMode}
	return o
}

func (o *TableCellStyleOpt) Debug(v bool) *TableCellStyleOpt {
	o.PDFCellOpt.debug = &v
	return o
}
