package wpdf

func hexToColor(c uint32) PDFColor {
	return PDFColor{R: int((c >> 16) & 0xFF), G: int((c >> 8) & 0xFF), B: int((c >> 0) & 0xFF)}
}

func rgbToColor(r, g, b int) PDFColor {
	return PDFColor{R: r, G: g, B: b}
}
