package wpdf

import (
	"gogs.mikescher.com/BlackForestBytes/goext/dataext"
	"gogs.mikescher.com/BlackForestBytes/goext/langext"
)

type PDFRectOpt struct {
	x         *float64
	y         *float64
	lineWidth *float64
	drawColor *PDFColor
	fillColor *PDFColor
	alpha     *dataext.Tuple[float64, PDFBlendMode]
	radiusTL  *float64
	radiusTR  *float64
	radiusBR  *float64
	radiusBL  *float64
	debug     *bool
}

func NewPDFRectOpt() *PDFRectOpt {
	return &PDFRectOpt{}
}

func (opt *PDFRectOpt) X(v float64) *PDFRectOpt {
	opt.x = &v
	return opt
}

func (opt *PDFRectOpt) Y(v float64) *PDFRectOpt {
	opt.y = &v
	return opt
}

func (opt *PDFRectOpt) LineWidth(v float64) *PDFRectOpt {
	opt.lineWidth = &v
	return opt
}

func (opt *PDFRectOpt) DrawColor(cr, cg, cb int) *PDFRectOpt {
	opt.drawColor = langext.Ptr(rgbToColor(cr, cg, cb))
	return opt
}

func (opt *PDFRectOpt) DrawColorHex(c uint32) *PDFRectOpt {
	opt.drawColor = langext.Ptr(hexToColor(c))
	return opt
}

func (opt *PDFRectOpt) FillColor(cr, cg, cb int) *PDFRectOpt {
	opt.fillColor = langext.Ptr(rgbToColor(cr, cg, cb))
	return opt
}

func (opt *PDFRectOpt) FillColorHex(c uint32) *PDFRectOpt {
	opt.fillColor = langext.Ptr(hexToColor(c))
	return opt
}

func (opt *PDFRectOpt) Rounded(radius float64) *PDFRectOpt {
	opt.radiusTL = &radius
	opt.radiusTR = &radius
	opt.radiusBR = &radius
	opt.radiusBL = &radius
	return opt
}

func (opt *PDFRectOpt) RadiusTL(radius float64) *PDFRectOpt {
	opt.radiusTL = &radius
	return opt
}

func (opt *PDFRectOpt) RadiusTR(radius float64) *PDFRectOpt {
	opt.radiusTR = &radius
	return opt
}

func (opt *PDFRectOpt) RadiusBL(radius float64) *PDFRectOpt {
	opt.radiusBL = &radius
	return opt
}

func (opt *PDFRectOpt) RadiusBR(radius float64) *PDFRectOpt {
	opt.radiusBR = &radius
	return opt
}

func (opt *PDFRectOpt) Alpha(alpha float64, blendMode PDFBlendMode) *PDFRectOpt {
	opt.alpha = &dataext.Tuple[float64, PDFBlendMode]{V1: alpha, V2: blendMode}
	return opt
}

func (opt *PDFRectOpt) Debug(v bool) *PDFRectOpt {
	opt.debug = &v
	return opt
}

func (b *WPDFBuilder) Rect(w float64, h float64, styleStr PDFRectStyle, opts ...*PDFRectOpt) {
	x := b.GetX()
	y := b.GetY()
	var lineWidth *float64
	var drawColor *PDFColor
	var fillColor *PDFColor
	var alphaOverride *dataext.Tuple[float64, PDFBlendMode]
	radiusTL := float64(0)
	radiusTR := float64(0)
	radiusBR := float64(0)
	radiusBL := float64(0)
	debug := b.debug

	for _, opt := range opts {
		x = langext.Coalesce(opt.x, x)
		y = langext.Coalesce(opt.y, y)
		lineWidth = langext.CoalesceOpt(opt.lineWidth, lineWidth)
		drawColor = langext.CoalesceOpt(opt.drawColor, drawColor)
		fillColor = langext.CoalesceOpt(opt.fillColor, fillColor)
		alphaOverride = langext.CoalesceOpt(opt.alpha, alphaOverride)
		radiusTL = langext.Coalesce(opt.radiusTL, radiusTL)
		radiusTR = langext.Coalesce(opt.radiusTR, radiusTR)
		radiusBR = langext.Coalesce(opt.radiusBR, radiusBR)
		radiusBL = langext.Coalesce(opt.radiusBL, radiusBL)
		debug = langext.Coalesce(opt.debug, debug)
	}

	if lineWidth != nil {
		old := b.GetLineWidth()
		b.SetLineWidth(*lineWidth)
		defer func() { b.SetLineWidth(old) }()
	}

	if drawColor != nil {
		oldR, oldG, oldB := b.GetDrawColor()
		b.SetDrawColor(drawColor.R, drawColor.G, drawColor.B)
		defer func() { b.SetDrawColor(oldR, oldG, oldB) }()
	}

	if fillColor != nil {
		oldR, oldG, oldB := b.GetFillColor()
		b.SetFillColor(fillColor.R, fillColor.G, fillColor.B)
		defer func() { b.SetFillColor(oldR, oldG, oldB) }()
	}

	if alphaOverride != nil {
		oldA, oldBMS := b.b.GetAlpha()
		b.b.SetAlpha(alphaOverride.V1, string(alphaOverride.V2))
		defer func() { b.b.SetAlpha(oldA, oldBMS) }()
	}

	b.b.RoundedRectExt(x, y, w, h, radiusTL, radiusTR, radiusBR, radiusBL, string(styleStr))
}
