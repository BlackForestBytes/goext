package cursortoken

import "strconv"

type CTPaginated struct {
	Mode Mode
	Page int
}

func Page(p int) CursorToken {
	return CTPaginated{
		Mode: CTMNormal,
		Page: p,
	}
}

func PageEnd() CursorToken {
	return CTPaginated{
		Mode: CTMEnd,
		Page: 0,
	}
}

func (c CTPaginated) Token() string {
	if c.Mode == CTMStart {
		return "$1"
	}
	if c.Mode == CTMEnd {
		return "$end"
	}

	return "$" + strconv.Itoa(c.Page)
}

func (c CTPaginated) IsEnd() bool {
	return c.Mode == CTMEnd
}

func (c CTPaginated) IsStart() bool {
	return c.Mode == CTMStart || c.Page == 1
}
