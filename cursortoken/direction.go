package cursortoken

type SortDirection string //@enum:type

const (
	SortASC  SortDirection = "ASC"
	SortDESC SortDirection = "DESC"
)

func (sd SortDirection) ToMongo() int {
	if sd == SortASC {
		return 1
	} else if sd == SortDESC {
		return -1
	} else {
		return 0
	}
}
