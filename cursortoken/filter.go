package cursortoken

import (
	"context"
	"go.mongodb.org/mongo-driver/mongo"
)

type RawFilter interface {
	FilterQuery(ctx context.Context) mongo.Pipeline
}

type Filter interface {
	FilterQuery(ctx context.Context) mongo.Pipeline
	Pagination(ctx context.Context) (string, SortDirection, string, SortDirection)
}
