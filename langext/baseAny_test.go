package langext

import (
	"gogs.mikescher.com/BlackForestBytes/goext/tst"
	"testing"
)

func _anyEncStr(bc AnyBaseConverter, v string) string {
	vr := bc.Encode([]byte(v))
	return vr
}

func _anyDecStr(bc AnyBaseConverter, v string) string {
	vr, err := bc.Decode(v)
	if err != nil {
		panic(err)
	}
	return string(vr)
}

func TestAnyBase58DefaultEncoding(t *testing.T) {
	tst.AssertEqual(t, _anyEncStr(NewAnyBaseConverter("123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz"), "Hello"), "9Ajdvzr")
	tst.AssertEqual(t, _anyEncStr(NewAnyBaseConverter("123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz"), "If debugging is the process of removing software bugs, then programming must be the process of putting them in."), "48638SMcJuah5okqPx4kCVf5d8QAdgbdNf28g7ReY13prUENNbMyssjq5GjsrJHF5zeZfqs4uJMUJHr7VbrU4XBUZ2Fw9DVtqtn9N1eXucEWSEZahXV6w4ysGSWqGdpeYTJf1MdDzTg8vfcQViifJjZX")
}

func TestAnyBase58DefaultDecoding(t *testing.T) {
	tst.AssertEqual(t, _anyDecStr(NewAnyBaseConverter("123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz"), "9Ajdvzr"), "Hello")
	tst.AssertEqual(t, _anyDecStr(NewAnyBaseConverter("123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz"), "48638SMcJuah5okqPx4kCVf5d8QAdgbdNf28g7ReY13prUENNbMyssjq5GjsrJHF5zeZfqs4uJMUJHr7VbrU4XBUZ2Fw9DVtqtn9N1eXucEWSEZahXV6w4ysGSWqGdpeYTJf1MdDzTg8vfcQViifJjZX"), "If debugging is the process of removing software bugs, then programming must be the process of putting them in.")
}

func TestAnyBaseDecode(t *testing.T) {

	const (
		Binary  = "01"
		Decimal = "0123456789"
		Hex     = "0123456789ABCDEF"
		DNA     = "ACGT"
		Base32  = "ABCDEFGHIJKLMNOPQRSTUVWXYZ234567"
		Base58  = "123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz"
		Base62  = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"
		Base64  = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/"
		Base256 = "🚀🪐☄🛰🌌🌑🌒🌓🌔🌕🌖🌗🌘🌍🌏🌎🐉☀💻🖥💾💿😂❤😍🤣😊🙏💕😭😘👍😅👏😁🔥🥰💔💖💙😢🤔😆🙄💪😉☺👌🤗💜😔😎😇🌹🤦🎉💞✌✨🤷😱😌🌸🙌😋💗💚😏💛🙂💓🤩😄😀🖤😃💯🙈👇🎶😒🤭❣😜💋👀😪😑💥🙋😞😩😡🤪👊🥳😥🤤👉💃😳✋😚😝😴🌟😬🙃🍀🌷😻😓⭐✅🥺🌈😈🤘💦✔😣🏃💐☹🎊💘😠☝😕🌺🎂🌻😐🖕💝🙊😹🗣💫💀👑🎵🤞😛🔴😤🌼😫⚽🤙☕🏆🤫👈😮🙆🍻🍃🐶💁😲🌿🧡🎁⚡🌞🎈❌✊👋😰🤨😶🤝🚶💰🍓💢🤟🙁🚨💨🤬✈🎀🍺🤓😙💟🌱😖👶🥴▶➡❓💎💸⬇😨🌚🦋😷🕺⚠🙅😟😵👎🤲🤠🤧📌🔵💅🧐🐾🍒😗🤑🌊🤯🐷☎💧😯💆👆🎤🙇🍑❄🌴💣🐸💌📍🥀🤢👅💡💩👐📸👻🤐🤮🎼🥵🚩🍎🍊👼💍📣🥂"
	)

	type TestDef struct {
		FromCS  string
		FromVal string
		ToCS    string
		ToVal   string
	}

	defs := []TestDef{
		{Binary, "10100101011100000101010", Decimal, "5421098"},
		{Decimal, "5421098", DNA, "CCAGGTGAAGGG"},
		{Decimal, "5421098", DNA, "CCAGGTGAAGGG"},
		{Decimal, "80085", Base256, "🪐💞🔵"},
		{Hex, "48656C6C6C20576F526C5421", Base64, "SGVsbGwgV29SbFQh"},
		{Base64, "SGVsbGw/gV29SbF+Qh", Base32, "CIMVWGY3B7QFO32SNRPZBB"},
		{Base64, "SGVsbGw/gV29SbF+Qh", Base58, "2fUsGKQUcgQcwSqpvy6"},
		{Base64, "SGVsbGw/gV29SbF+Qh", Base62, "V34nvybdQ3m3RHk9Sr"},
	}

	for _, def := range defs {

		d1 := NewAnyBaseConverter(def.FromCS)
		d2 := NewAnyBaseConverter(def.ToCS)

		v1 := tst.Must(d1.Decode(def.FromVal))(t)
		v2 := tst.Must(d2.Decode(def.ToVal))(t)

		tst.AssertArrayEqual(t, v1, v2)

		str2 := d2.Encode(v1)
		tst.AssertEqual(t, str2, def.ToVal)

		str1 := d1.Encode(v2)
		tst.AssertEqual(t, str1, def.FromVal)

	}
}
