package langext

import "encoding/json"

func DeepCopyByJson[T any](v T) (T, error) {

	bin, err := json.Marshal(v)
	if err != nil {
		return *new(T), err
	}

	var result T
	err = json.Unmarshal(bin, &result)
	if err != nil {
		return *new(T), err
	}

	return result, nil
}
