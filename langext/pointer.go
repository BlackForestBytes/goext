package langext

import (
	"reflect"
)

// PTrue := &true
var PTrue = Ptr(true)

// PFalse := &false
var PFalse = Ptr(false)

// PNil := &nil
var PNil = Ptr[any](nil)

func Ptr[T any](v T) *T {
	return &v
}

func DblPtr[T any](v T) **T {
	v_ := &v
	return &v_
}

func DblPtrIfNotNil[T any](v *T) **T {
	if v == nil {
		return nil
	}
	return &v
}

func DblPtrNil[T any]() **T {
	var v *T = nil
	return &v
}

func ArrPtr[T any](v ...T) *[]T {
	return &v
}

func PtrInt32(v int32) *int32 {
	return &v
}

func PtrInt64(v int64) *int64 {
	return &v
}

func PtrFloat32(v float32) *float32 {
	return &v
}

func PtrFloat64(v float64) *float64 {
	return &v
}

func IsNil(i interface{}) bool {
	if i == nil {
		return true
	}
	switch reflect.TypeOf(i).Kind() {
	case reflect.Ptr, reflect.Map, reflect.Chan, reflect.Slice, reflect.Func, reflect.UnsafePointer:
		return reflect.ValueOf(i).IsNil()
	}
	return false
}

func PtrEquals[T comparable](v1 *T, v2 *T) bool {
	return (v1 == nil && v2 == nil) || (v1 != nil && v2 != nil && *v1 == *v2)
}
