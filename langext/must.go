package langext

// Must returns a value and panics on error
//
// Usage: Must(methodWithError(...))
func Must[T any](v T, err error) T {
	if err != nil {
		panic(err)
	}
	return v
}

// MustBool returns a value and panics on missing
//
// Usage: MustBool(methodWithOkayReturn(...))
func MustBool[T any](v T, ok bool) T {
	if !ok {
		panic("not ok")
	}
	return v
}
