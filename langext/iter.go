package langext

import (
	"iter"
)

func IterSingleValueSeq[T any](value T) iter.Seq[T] {
	return func(yield func(T) bool) {
		if !yield(value) {
			return
		}
	}
}

func IterSingleValueSeq2[T1 any, T2 any](v1 T1, v2 T2) iter.Seq2[T1, T2] {
	return func(yield func(T1, T2) bool) {
		if !yield(v1, v2) {
			return
		}
	}
}
