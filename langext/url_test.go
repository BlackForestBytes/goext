package langext

import (
	"gogs.mikescher.com/BlackForestBytes/goext/tst"
	"testing"
)

func TestBuildUrl(t *testing.T) {
	tests := []struct {
		Url    string
		Path   string
		Params *map[string]string
		Want   string
	}{
		{
			Url:    "https://test.heydyno.de/",
			Path:   "/testing-01",
			Params: &map[string]string{"param1": "value1"},
			Want:   "https://test.heydyno.de/testing-01?param1=value1",
		},
		{
			Url:    "https://test.heydyno.de",
			Path:   "testing-01",
			Params: &map[string]string{"param1": "value1"},
			Want:   "https://test.heydyno.de/testing-01?param1=value1",
		},
		{
			Url:    "https://test.heydyno.de",
			Path:   "/testing-01",
			Params: nil,
			Want:   "https://test.heydyno.de/testing-01",
		},
		{
			Url:    "https://test.heydyno.de/",
			Path:   "testing-01",
			Params: nil,
			Want:   "https://test.heydyno.de/testing-01",
		},
	}

	for _, test := range tests {
		res := BuildUrl(test.Url, test.Path, test.Params)
		tst.AssertEqual(t, res, test.Want)
	}
}
