package langext

import "math"

func DegToRad(deg float64) float64 {
	return deg * (math.Pi / 180.0)
}

func RadToDeg(rad float64) float64 {
	return rad / (math.Pi * 180.0)
}

func GeoDistance(lon1 float64, lat1 float64, lon2 float64, lat2 float64) float64 {
	var d1 = DegToRad(lat1)
	var num1 = DegToRad(lon1)
	var d2 = DegToRad(lat2)
	var num2 = DegToRad(lon2) - num1
	var d3 = math.Pow(math.Sin((d2-d1)/2.0), 2.0) + math.Cos(d1)*math.Cos(d2)*math.Pow(math.Sin(num2/2.0), 2.0)

	return 6376500.0 * (2.0 * math.Atan2(math.Sqrt(d3), math.Sqrt(1.0-d3)))
}
