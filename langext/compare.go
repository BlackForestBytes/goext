package langext

func CompareIntArr(arr1 []int, arr2 []int) bool {

	for i := 0; i < len(arr1) || i < len(arr2); i++ {

		if i < len(arr1) && i < len(arr2) {

			if arr1[i] < arr2[i] {
				return true
			} else if arr1[i] > arr2[i] {
				return false
			} else {
				continue
			}

		}

		if i < len(arr1) {

			return true

		} else { // if i < len(arr2)

			return false

		}

	}

	return false
}

func CompareArr[T OrderedConstraint](arr1 []T, arr2 []T) int {

	for i := 0; i < len(arr1) || i < len(arr2); i++ {

		if i < len(arr1) && i < len(arr2) {

			if arr1[i] < arr2[i] {
				return -1
			} else if arr1[i] > arr2[i] {
				return +2
			} else {
				continue
			}

		}

		if i < len(arr1) {

			return +1

		} else { // if i < len(arr2)

			return -1

		}

	}

	return 0
}

func CompareString(a, b string) int {
	if a == b {
		return 0
	}
	if a < b {
		return -1
	}
	return +1
}

func CompareInt(a, b int) int {
	if a == b {
		return 0
	}
	if a < b {
		return -1
	}
	return +1
}

func CompareInt64(a, b int64) int {
	if a == b {
		return 0
	}
	if a < b {
		return -1
	}
	return +1
}

func Compare[T OrderedConstraint](a, b T) int {
	if a == b {
		return 0
	}
	if a < b {
		return -1
	}
	return +1
}
