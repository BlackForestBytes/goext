package langext

import "testing"

func TestStrLimitBehaviour(t *testing.T) {
	val := "Hello, World!"
	maxlen := 5
	suffix := "..."
	expected := "He..."
	result := StrLimit(val, maxlen, suffix)
	if result != expected {
		t.Errorf("Expected %v but got %v", expected, result)
	}
}

func TestStrSplitBehaviour1(t *testing.T) {
	val := "Hello,World,,"
	sep := ","
	expected := []string{"Hello", "World"}
	result := StrSplit(val, sep, false)
	if len(result) != len(expected) {
		t.Errorf("Expected %v but got %v", expected, result)
	}
}

func TestStrSplitBehaviour2(t *testing.T) {
	val := "Hello,World,,"
	sep := ","
	expected := []string{"Hello", "World", "", ""}
	result := StrSplit(val, sep, true)
	if len(result) != len(expected) {
		t.Errorf("Expected %v but got %v", expected, result)
	}
}

func TestStrPadRightBehaviour(t *testing.T) {
	str := "Hello"
	pad := "*"
	padlen := 10
	expected := "Hello*****"
	result := StrPadRight(str, pad, padlen)
	if result != expected {
		t.Errorf("Expected %v but got %v", expected, result)
	}
}

func TestStrPadLeftBehaviour(t *testing.T) {
	str := "Hello"
	pad := "*"
	padlen := 10
	expected := "*****Hello"
	result := StrPadLeft(str, pad, padlen)
	if result != expected {
		t.Errorf("Expected %v but got %v", expected, result)
	}
}

func TestStrRunePadLeftBehaviour(t *testing.T) {
	str := "Hello"
	pad := "*"
	padlen := 10
	expected := "*****Hello"
	result := StrRunePadLeft(str, pad, padlen)
	if result != expected {
		t.Errorf("Expected %v but got %v", expected, result)
	}
}

func TestStrRunePadRightBehaviour(t *testing.T) {
	str := "Hello"
	pad := "*"
	padlen := 10
	expected := "Hello*****"
	result := StrRunePadRight(str, pad, padlen)
	if result != expected {
		t.Errorf("Expected %v but got %v", expected, result)
	}
}

func TestIndentBehaviour1(t *testing.T) {
	str := "Hello\nWorld"
	pad := ".."
	expected := "..Hello\n..World"
	result := Indent(str, pad)
	if result != expected {
		t.Errorf("Expected %v but got %v", expected, result)
	}
}

func TestIndentBehaviour2(t *testing.T) {
	str := "Hello\nWorld\n"
	pad := ".."
	expected := "..Hello\n..World\n"
	result := Indent(str, pad)
	if result != expected {
		t.Errorf("Expected %v but got %v", expected, result)
	}
}

func TestStrRepeatBehaviour(t *testing.T) {
	val := "Hello"
	count := 3
	expected := "HelloHelloHello"
	result := StrRepeat(val, count)
	if result != expected {
		t.Errorf("Expected %v but got %v", expected, result)
	}
}

func TestStrWrapBehaviour1(t *testing.T) {
	val := "123456789"
	linelen := 5
	seperator := "\n"
	expected := "12345\n6789"
	result := StrWrap(val, linelen, seperator)
	if result != expected {
		t.Errorf("Expected %v but got %v", expected, result)
	}
}

func TestStrWrapBehaviour2(t *testing.T) {
	val := "1234567890"
	linelen := 5
	seperator := "\n"
	expected := "12345\n67890"
	result := StrWrap(val, linelen, seperator)
	if result != expected {
		t.Errorf("Expected %v but got %v", expected, result)
	}
}

func TestStrWrapBehaviour3(t *testing.T) {
	val := "****************"
	linelen := 4
	seperator := "\n"
	expected := "****\n****\n****\n****"
	result := StrWrap(val, linelen, seperator)
	if result != expected {
		t.Errorf("Expected %v but got %v", expected, result)
	}
}

func TestStrWrapBehaviour4(t *testing.T) {
	val := "*****************"
	linelen := 4
	seperator := "\n"
	expected := "****\n****\n****\n****\n*"
	result := StrWrap(val, linelen, seperator)
	if result != expected {
		t.Errorf("Expected %v but got %v", expected, result)
	}
}
