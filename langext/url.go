package langext

import (
	"fmt"
	"strings"
)

func BuildUrl(url, path string, params *map[string]string) string {
	if path[:1] == "/" && url[len(url)-1:] == "/" {
		url += path[1:]
	} else if path[:1] != "/" && url[len(url)-1:] != "/" {
		url += "/" + path
	} else {
		url += path
	}

	if params == nil {
		return url
	}

	for key, value := range *params {
		if strings.Contains(url, "?") {
			url += fmt.Sprintf("&%s=%s", key, value)
		} else {
			url += fmt.Sprintf("?%s=%s", key, value)
		}
	}
	return url
}
