package langext

type IntConstraint interface {
	int | int8 | int16 | int32 | int64 | uint | uint8 | uint16 | uint32 | uint64
}

type SignedConstraint interface {
	~int | ~int8 | ~int16 | ~int32 | ~int64
}

type UnsignedConstraint interface {
	~uint | ~uint8 | ~uint16 | ~uint32 | ~uint64 | ~uintptr
}

type IntegerConstraint interface {
	SignedConstraint | UnsignedConstraint
}

type FloatConstraint interface {
	~float32 | ~float64
}

type ComplexConstraint interface {
	~complex64 | ~complex128
}

type OrderedConstraint interface {
	IntegerConstraint | FloatConstraint | ~string
}

type NumberConstraint interface {
	IntegerConstraint | FloatConstraint
}
