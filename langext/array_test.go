package langext

import (
	"gogs.mikescher.com/BlackForestBytes/goext/tst"
	"strings"
	"testing"
)

func TestJoinString(t *testing.T) {
	ids := []string{"1", "2", "3"}
	res := JoinString(ids, ",")
	tst.AssertEqual(t, res, "1,2,3")
}

func TestArrPrepend(t *testing.T) {
	v1 := []string{"1", "2", "3"}

	v2 := ArrPrepend(v1, "4", "5", "6")

	tst.AssertEqual(t, strings.Join(v1, ""), "123")
	tst.AssertEqual(t, strings.Join(v2, ""), "654123")

}
