package langext

import "io"

type nopCloser struct {
	io.Writer
}

func (n nopCloser) Close() error {
	return nil // no op
}

func WriteNopCloser(w io.Writer) io.WriteCloser {
	return nopCloser{w}
}
