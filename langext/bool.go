package langext

func FormatBool(v bool, strTrue string, strFalse string) string {
	if v {
		return strTrue
	} else {
		return strFalse
	}
}

func Conditional[T any](v bool, resTrue T, resFalse T) T {
	if v {
		return resTrue
	} else {
		return resFalse
	}
}

func ConditionalFn00[T any](v bool, resTrue T, resFalse T) T {
	if v {
		return resTrue
	} else {
		return resFalse
	}
}

func ConditionalFn10[T any](v bool, resTrue func() T, resFalse T) T {
	if v {
		return resTrue()
	} else {
		return resFalse
	}
}

func ConditionalFn01[T any](v bool, resTrue T, resFalse func() T) T {
	if v {
		return resTrue
	} else {
		return resFalse()
	}
}

func ConditionalFn11[T any](v bool, resTrue func() T, resFalse func() T) T {
	if v {
		return resTrue()
	} else {
		return resFalse()
	}
}
