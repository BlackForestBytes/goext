package googleapi

import (
	"encoding/base64"
	"fmt"
)

type MailAttachment struct {
	IsInline    bool
	ContentType string
	Filename    string
	Data        []byte
}

func (a MailAttachment) dump() []string {
	res := make([]string, 0, 4)

	if a.ContentType != "" {
		res = append(res, "Content-Type: "+a.ContentType+"; charset=UTF-8")
	}

	res = append(res, "Content-Transfer-Encoding: base64")

	if a.IsInline {
		if a.Filename != "" {
			res = append(res, fmt.Sprintf("Content-Disposition: inline;filename=\"%s\"", a.Filename))
		} else {
			res = append(res, "Content-Disposition: inline")
		}
	} else {
		if a.Filename != "" {
			res = append(res, fmt.Sprintf("Content-Disposition: attachment;filename=\"%s\"", a.Filename))
		} else {
			res = append(res, "Content-Disposition: attachment")
		}
	}

	b64 := base64.StdEncoding.EncodeToString(a.Data)
	for i := 0; i < len(b64); i += 80 {
		res = append(res, b64[i:min(i+80, len(b64))])
	}

	res = append(res)

	return res
}
