
Google OAuth Setup (to send mails)
==================================


 - Login @ https://console.cloud.google.com

 - GMail API akivieren: https://console.cloud.google.com/apis/library/gmail.googleapis.com?

 - Create new Project (aka 'BackendMailAPI') @ https://console.cloud.google.com/projectcreate
   User Type: Intern
   Anwendungsname: 'BackendMailAPI'
   Support-Email: ...
   Authorisierte Domains: 'heydyno.de' (or project domain)
   Kontakt-Email: ...
   

 - Unter "Anmeldedaten" neuer OAuth Client erstellen @ https://console.cloud.google.com/apis/credentials
   Anwendungstyp: Web
   Name: 'BackendMailOAuth'
   Redirect-Uri: 'http://localhost/oauth'
   Client-ID und Client-Key merken

 - Open in Browser:
   https://accounts.google.com/o/oauth2/v2/auth?redirect_uri=http://localhost/oauth&prompt=consent&response_type=code&client_id={...}&scope=https://www.googleapis.com/auth/gmail.send&access_type=offline
   Code aus redirected URI merken

 - Code via request einlösen (und refresh_roken merken):

```
curl --request POST \
  --url https://oauth2.googleapis.com/token \
  --data code={...} \
  --data redirect_uri=http://localhost/oauth \
  --data client_id={...} \
  --data client_secret={...} \
  --data grant_type=authorization_code \
  --data scope=https://www.googleapis.com/auth/gmail.send
```

 - Fertig, mit `client_id`, `client_secret` und `refresh_token` kann das package benutzt werden













