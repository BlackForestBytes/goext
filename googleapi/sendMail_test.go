package googleapi

import (
	"context"
	"fmt"
	"gogs.mikescher.com/BlackForestBytes/goext/exerr"
	"gogs.mikescher.com/BlackForestBytes/goext/langext"
	"gogs.mikescher.com/BlackForestBytes/goext/tst"
	"os"
	"testing"
)

func TestMain(m *testing.M) {
	if !exerr.Initialized() {
		exerr.Init(exerr.ErrorPackageConfigInit{ZeroLogErrTraces: langext.PFalse, ZeroLogAllTraces: langext.PFalse})
	}
	os.Exit(m.Run())
}

func TestSendMail1(t *testing.T) {
	t.Skip()
	return

	auth := NewGoogleOAuth(
		"554617284247-8di0j6s5dcmlk4lmk4hdf9kdn8scss54.apps.googleusercontent.com",
		"TODO",
		"TODO")

	ctx := context.Background()

	gclient := NewGoogleClient(auth)

	mail, err := gclient.SendMail(
		ctx,
		"noreply@heydyno.de",
		[]string{"trash@mikescher.de"},
		nil,
		nil,
		"Hello Test Mail",
		MailBody{Plain: "Plain Text"},
		nil)

	tst.AssertNoErr(t, err)

	fmt.Printf("mail.ID        := %s\n", mail.ID)
	fmt.Printf("mail.ThreadID  := %s\n", mail.ThreadID)
	fmt.Printf("mail.LabelIDs  := %v\n", mail.LabelIDs)
}

func TestSendMail2(t *testing.T) {
	t.Skip()
	return

	auth := NewGoogleOAuth(
		"554617284247-8di0j6s5dcmlk4lmk4hdf9kdn8scss54.apps.googleusercontent.com",
		"TODO",
		"TODO")

	ctx := context.Background()

	gclient := NewGoogleClient(auth)

	mail, err := gclient.SendMail(
		ctx,
		"noreply@heydyno.de",
		[]string{"trash@mikescher.de"},
		nil,
		nil,
		"Hello Test Mail (alternative)",
		MailBody{
			Plain: "Plain Text",
			HTML:  "<html><body><u>Non</u> Pl<i>ai</i>n T<b>ex</b>t</body></html>",
		},
		nil)

	tst.AssertNoErr(t, err)

	fmt.Printf("mail.ID        := %s\n", mail.ID)
	fmt.Printf("mail.ThreadID  := %s\n", mail.ThreadID)
	fmt.Printf("mail.LabelIDs  := %v\n", mail.LabelIDs)
}

func TestSendMail3(t *testing.T) {
	t.Skip()
	return

	auth := NewGoogleOAuth(
		"554617284247-8di0j6s5dcmlk4lmk4hdf9kdn8scss54.apps.googleusercontent.com",
		"TODO",
		"TODO")

	ctx := context.Background()

	gclient := NewGoogleClient(auth)

	mail, err := gclient.SendMail(
		ctx,
		"noreply@heydyno.de",
		[]string{"trash@mikescher.de"},
		nil,
		nil,
		"Hello Test Mail (attach)",
		MailBody{
			HTML: "<html><body><u>Non</u> Pl<i>ai</i>n T<b>ex</b>t</body></html>",
		},
		[]MailAttachment{
			{Data: []byte("HelloWorld"), Filename: "test.txt", IsInline: false, ContentType: "text/plain"},
		})

	tst.AssertNoErr(t, err)

	fmt.Printf("mail.ID        := %s\n", mail.ID)
	fmt.Printf("mail.ThreadID  := %s\n", mail.ThreadID)
	fmt.Printf("mail.LabelIDs  := %v\n", mail.LabelIDs)
}

func TestSendMail4(t *testing.T) {
	t.Skip()
	return

	auth := NewGoogleOAuth(
		"554617284247-8di0j6s5dcmlk4lmk4hdf9kdn8scss54.apps.googleusercontent.com",
		"TODO",
		"TODO")

	ctx := context.Background()

	gclient := NewGoogleClient(auth)

	b := tst.Must(os.ReadFile("test_placeholder.png"))(t)

	mail, err := gclient.SendMail(
		ctx,
		"noreply@heydyno.de",
		[]string{"trash@mikescher.de"},
		nil,
		nil,
		"Hello Test Mail (inline)",
		MailBody{
			HTML: "<html><body><u>Non</u> Pl<i>ai</i>n T<b>ex</b>t</body></html>",
		},
		[]MailAttachment{
			{Data: b, Filename: "img.png", IsInline: true, ContentType: "image/png"},
		})

	tst.AssertNoErr(t, err)

	fmt.Printf("mail.ID        := %s\n", mail.ID)
	fmt.Printf("mail.ThreadID  := %s\n", mail.ThreadID)
	fmt.Printf("mail.LabelIDs  := %v\n", mail.LabelIDs)
}
