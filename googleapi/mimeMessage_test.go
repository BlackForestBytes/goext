package googleapi

import (
	"gogs.mikescher.com/BlackForestBytes/goext/tst"
	"os"
	"testing"
)

func TestEncodeMimeMail(t *testing.T) {

	mail := encodeMimeMail(
		"noreply@heydyno.de",
		[]string{"trash@mikescher.de"},
		nil,
		nil,
		"Hello Test Mail",
		MailBody{Plain: "Plain Text"},
		nil)

	verifyMime(mail)
}

func TestEncodeMimeMail2(t *testing.T) {

	mail := encodeMimeMail(
		"noreply@heydyno.de",
		[]string{"trash@mikescher.de"},
		nil,
		nil,
		"Hello Test Mail (alternative)",
		MailBody{
			Plain: "Plain Text",
			HTML:  "<html><body><u>Non</u> Pl<i>ai</i>n T<b>ex</b>t</body></html>",
		},
		nil)

	verifyMime(mail)
}

func TestEncodeMimeMail3(t *testing.T) {

	mail := encodeMimeMail(
		"noreply@heydyno.de",
		[]string{"trash@mikescher.de"},
		nil,
		nil,
		"Hello Test Mail (alternative)",
		MailBody{
			HTML: "<html><body><u>Non</u> Pl<i>ai</i>n T<b>ex</b>t</body></html>",
		},
		[]MailAttachment{
			{Data: []byte("HelloWorld"), Filename: "test.txt", IsInline: false, ContentType: "text/plain"},
		})

	verifyMime(mail)
}

func TestEncodeMimeMail4(t *testing.T) {

	b := tst.Must(os.ReadFile("test_placeholder.png"))(t)

	mail := encodeMimeMail(
		"noreply@heydyno.de",
		[]string{"trash@mikescher.de"},
		nil,
		nil,
		"Hello Test Mail (inline)",
		MailBody{
			HTML: "<html><body><u>Non</u> Pl<i>ai</i>n T<b>ex</b>t</body></html>",
		},
		[]MailAttachment{
			{Data: b, Filename: "img.png", IsInline: true, ContentType: "image/png"},
		})

	verifyMime(mail)
}

func verifyMime(mail string) {
	//fmt.Printf("%s\n\n", mail)
}
