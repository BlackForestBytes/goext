package googleapi

import (
	"context"
	"net/http"
)

type GoogleClient interface {
	SendMail(ctx context.Context, from string, recipients []string, cc []string, bcc []string, subject string, body MailBody, attachments []MailAttachment) (MailRef, error)
}

type client struct {
	oauth GoogleOAuth
	http  http.Client
}

func NewGoogleClient(oauth GoogleOAuth) GoogleClient {
	return &client{
		oauth: oauth,
		http:  http.Client{},
	}
}
