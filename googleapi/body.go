package googleapi

type MailBody struct {
	Plain string
	HTML  string
}
