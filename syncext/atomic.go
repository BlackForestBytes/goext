package syncext

import (
	"sync"
)

type Atomic[T any] struct {
	v    T
	lock sync.RWMutex
}

func NewAtomic[T any](value T) *Atomic[T] {
	return &Atomic[T]{
		v:    value,
		lock: sync.RWMutex{},
	}
}

func (a *Atomic[T]) Get() T {
	a.lock.RLock()
	defer a.lock.RUnlock()
	return a.v
}

func (a *Atomic[T]) Set(value T) T {
	a.lock.Lock()
	defer a.lock.Unlock()

	oldValue := a.v

	a.v = value

	return oldValue
}
