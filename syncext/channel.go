package syncext

import (
	"time"
)

// https://gobyexample.com/non-blocking-channel-operations
// https://gobyexample.com/timeouts
// https://groups.google.com/g/golang-nuts/c/Oth9CmJPoqo

func ReadChannelWithTimeout[T any](c chan T, timeout time.Duration) (T, bool) {
	select {
	case msg := <-c:
		return msg, true
	case <-time.After(timeout):
		return *new(T), false
	}
}

func WriteChannelWithTimeout[T any](c chan T, msg T, timeout time.Duration) bool {
	select {
	case c <- msg:
		return true
	case <-time.After(timeout):
		return false
	}
}

func ReadNonBlocking[T any](c chan T) (T, bool) {
	select {
	case msg := <-c:
		return msg, true
	default:
		return *new(T), false
	}
}

func WriteNonBlocking[T any](c chan T, msg T) bool {
	select {
	case c <- msg:
		return true
	default:
		return false
	}
}
