package bfcodegen

import (
	_ "embed"
	"fmt"
	"gogs.mikescher.com/BlackForestBytes/goext/cmdext"
	"gogs.mikescher.com/BlackForestBytes/goext/langext"
	"gogs.mikescher.com/BlackForestBytes/goext/tst"
	"os"
	"path/filepath"
	"testing"
	"time"
)

//go:embed _test_example_1.tgz
var CSIDExampleModels1 []byte

func TestGenerateCSIDSpecs(t *testing.T) {

	tmpFile := filepath.Join(t.TempDir(), langext.MustHexUUID()+".tgz")

	tmpDir := filepath.Join(t.TempDir(), langext.MustHexUUID())

	err := os.WriteFile(tmpFile, CSIDExampleModels1, 0o777)
	tst.AssertNoErr(t, err)

	t.Cleanup(func() { _ = os.Remove(tmpFile) })

	err = os.Mkdir(tmpDir, 0o777)
	tst.AssertNoErr(t, err)

	t.Cleanup(func() { _ = os.RemoveAll(tmpFile) })

	_, err = cmdext.Runner("tar").Arg("-xvzf").Arg(tmpFile).Arg("-C").Arg(tmpDir).FailOnExitCode().FailOnTimeout().Timeout(time.Minute).Run()
	tst.AssertNoErr(t, err)

	err = GenerateCharsetIDSpecs(tmpDir, tmpDir+"/csid_gen.go", CSIDGenOptions{DebugOutput: langext.PTrue})
	tst.AssertNoErr(t, err)

	err = GenerateCharsetIDSpecs(tmpDir, tmpDir+"/csid_gen.go", CSIDGenOptions{DebugOutput: langext.PTrue})
	tst.AssertNoErr(t, err)

	fmt.Println()
	fmt.Println()
	fmt.Println()
	fmt.Println("=====================================================================================================")
	fmt.Println(string(tst.Must(os.ReadFile(tmpDir + "/csid_gen.go"))(t)))
	fmt.Println("=====================================================================================================")
	fmt.Println()
	fmt.Println()
	fmt.Println()
}
