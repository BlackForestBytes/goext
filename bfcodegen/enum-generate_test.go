package bfcodegen

import (
	_ "embed"
	"fmt"
	"gogs.mikescher.com/BlackForestBytes/goext/cmdext"
	"gogs.mikescher.com/BlackForestBytes/goext/langext"
	"gogs.mikescher.com/BlackForestBytes/goext/tst"
	"os"
	"path/filepath"
	"testing"
	"time"
)

//go:embed _test_example_1.tgz
var EnumExampleModels1 []byte

//go:embed _test_example_2.tgz
var EnumExampleModels2 []byte

func TestGenerateEnumSpecs(t *testing.T) {

	tmpFile := filepath.Join(t.TempDir(), langext.MustHexUUID()+".tgz")

	tmpDir := filepath.Join(t.TempDir(), langext.MustHexUUID())

	err := os.WriteFile(tmpFile, EnumExampleModels1, 0o777)
	tst.AssertNoErr(t, err)

	t.Cleanup(func() { _ = os.Remove(tmpFile) })

	err = os.Mkdir(tmpDir, 0o777)
	tst.AssertNoErr(t, err)

	t.Cleanup(func() { _ = os.RemoveAll(tmpFile) })

	_, err = cmdext.Runner("tar").Arg("-xvzf").Arg(tmpFile).Arg("-C").Arg(tmpDir).FailOnExitCode().FailOnTimeout().Timeout(time.Minute).Run()
	tst.AssertNoErr(t, err)

	s1, cs1, _, err := _generateEnumSpecs(tmpDir, "", "N/A", true, true)
	tst.AssertNoErr(t, err)

	s2, cs2, _, err := _generateEnumSpecs(tmpDir, "", "N/A", true, true)
	tst.AssertNoErr(t, err)

	tst.AssertEqual(t, cs1, cs2)
	tst.AssertEqual(t, s1, s2)

	fmt.Println()
	fmt.Println()
	fmt.Println()
	fmt.Println("=====================================================================================================")
	fmt.Println(s1)
	fmt.Println("=====================================================================================================")
	fmt.Println()
	fmt.Println()
	fmt.Println()
}

func TestGenerateEnumSpecsData(t *testing.T) {

	tmpFile := filepath.Join(t.TempDir(), langext.MustHexUUID()+".tgz")

	tmpDir := filepath.Join(t.TempDir(), langext.MustHexUUID())

	err := os.WriteFile(tmpFile, EnumExampleModels2, 0o777)
	tst.AssertNoErr(t, err)

	t.Cleanup(func() { _ = os.Remove(tmpFile) })

	err = os.Mkdir(tmpDir, 0o777)
	tst.AssertNoErr(t, err)

	t.Cleanup(func() { _ = os.RemoveAll(tmpFile) })

	_, err = cmdext.Runner("tar").Arg("-xvzf").Arg(tmpFile).Arg("-C").Arg(tmpDir).FailOnExitCode().FailOnTimeout().Timeout(time.Minute).Run()
	tst.AssertNoErr(t, err)

	s1, _, _, err := _generateEnumSpecs(tmpDir, "", "", true, true)
	tst.AssertNoErr(t, err)

	fmt.Println()
	fmt.Println()
	fmt.Println()
	fmt.Println("=====================================================================================================")
	fmt.Println(s1)
	fmt.Println("=====================================================================================================")
	fmt.Println()
	fmt.Println()
	fmt.Println()
}
