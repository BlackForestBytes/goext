package mathext

import "gogs.mikescher.com/BlackForestBytes/goext/langext"

func ClampInt(v int, lo int, hi int) int {
	if v < lo {
		return lo
	} else if v > hi {
		return hi
	} else {
		return v
	}
}

func ClampInt32(v int32, lo int32, hi int32) int32 {
	if v < lo {
		return lo
	} else if v > hi {
		return hi
	} else {
		return v
	}
}

func ClampFloat32(v float32, lo float32, hi float32) float32 {
	if v < lo {
		return lo
	} else if v > hi {
		return hi
	} else {
		return v
	}
}

func ClampFloat64(v float64, lo float64, hi float64) float64 {
	if v < lo {
		return lo
	} else if v > hi {
		return hi
	} else {
		return v
	}
}

func Clamp[T langext.NumberConstraint](v T, min T, max T) T {
	if v < min {
		return min
	} else if v > max {
		return max
	} else {
		return v
	}
}

func ClampOpt[T langext.NumberConstraint](v *T, fallback T, min T, max T) T {
	if v == nil {
		return fallback
	} else if *v < min {
		return min
	} else if *v > max {
		return max
	} else {
		return *v
	}
}
