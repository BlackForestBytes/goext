package mathext

import (
	"math"
	"testing"
)

func TestSumIntsHappyPath(t *testing.T) {
	values := []int{1, 2, 3, 4, 5}
	expected := 15
	result := Sum(values)
	if result != expected {
		t.Errorf("Sum of %v; expected %v, got %v", values, expected, result)
	}
}

func TestSumFloatsHappyPath(t *testing.T) {
	values := []float64{1.1, 2.2, 3.3}
	expected := 6.6
	result := Sum(values)
	if result != expected {
		t.Errorf("Sum of %v; expected %v, got %v", values, expected, result)
	}
}

func TestMeanOfInts(t *testing.T) {
	values := []float64{1, 2, 3, 4, 5}
	expected := 3.0
	result := Mean(values)
	if result != expected {
		t.Errorf("Mean of %v; expected %v, got %v", values, expected, result)
	}
}

func TestMedianOddNumberOfElements(t *testing.T) {
	values := []float64{1, 2, 3, 4, 5}
	expected := 3.0
	result := Median(values)
	if result != expected {
		t.Errorf("Median of %v; expected %v, got %v", values, expected, result)
	}
}

func TestMedianEvenNumberOfElements(t *testing.T) {
	values := []float64{1, 2, 3, 4, 5, 6}
	expected := 3.5
	result := Median(values)
	if result != expected {
		t.Errorf("Median of %v; expected %v, got %v", values, expected, result)
	}
}

func TestArrMinInts(t *testing.T) {
	values := []int{5, 3, 9, 1, 4}
	expected := 1
	result := ArrMin(values)
	if result != expected {
		t.Errorf("ArrMin of %v; expected %v, got %v", values, expected, result)
	}
}

func TestArrMaxInts(t *testing.T) {
	values := []int{5, 3, 9, 1, 4}
	expected := 9
	result := ArrMax(values)
	if result != expected {
		t.Errorf("ArrMax of %v; expected %v, got %v", values, expected, result)
	}
}

func TestPercentileValidInput(t *testing.T) {
	values := []int{1, 2, 3, 4, 5}
	percentile := 50.0
	expected := 3
	result, err := Percentile(values, percentile)
	if err != nil || result != expected {
		t.Errorf("Percentile %v of %v; expected %v, got %v, err: %v", percentile, values, expected, result, err)
	}
}

func TestPercentileOutOfRange(t *testing.T) {
	values := []int{1, 2, 3, 4, 5}
	percentile := 150.0
	_, err := Percentile(values, percentile)
	if err == nil {
		t.Errorf("Expected error for percentile %v out of range, got nil", percentile)
	}
}

func TestPercentileValueInArray(t *testing.T) {
	values := []int{1, 3, 5, 7, 9}
	percentile := 40.0
	expected := 4
	result, err := Percentile(values, percentile)
	if err != nil || result != expected {
		t.Errorf("Percentile %v of %v; expected %v, got %v, err: %v", percentile, values, expected, result, err)
	}
}

func TestFloatPercentileValueInArray(t *testing.T) {
	values := []int{1, 3, 5, 7, 9}
	percentile := 40.0
	expected := 4.2
	result, err := FloatPercentile(values, percentile)
	if err != nil || result != expected {
		t.Errorf("Percentile %v of %v; expected %v, got %v, err: %v", percentile, values, expected, result, err)
	}
}

func TestPercentileInterpolation(t *testing.T) {
	values := []float64{1.0, 2.0, 3.0, 4.0, 5.0}
	percentile := 25.0
	expected := 2.0
	result, err := Percentile(values, percentile)
	if err != nil || result != expected {
		t.Errorf("Percentile %v of %v; expected %v, got %v, err: %v", percentile, values, expected, result, err)
	}
}

func TestPercentileSingleValue(t *testing.T) {
	values := []int{10}
	percentile := 50.0
	expected := 10
	result, err := Percentile(values, percentile)
	if err != nil || result != expected {
		t.Errorf("Percentile %v of %v; expected %v, got %v, err: %v", percentile, values, expected, result, err)
	}
}

func TestPercentileExactlyBetweenTwoValues(t *testing.T) {
	values := []float64{1, 2, 3, 4, 5}
	percentile := 62.5 // Exactly between 3 and 4
	expected := 3.5
	result, err := FloatPercentile(values, percentile)
	if err != nil || result != expected {
		t.Errorf("Percentile %v of %v; expected %v, got %v, err: %v", percentile, values, expected, result, err)
	}
}

func TestPercentileTwoThirdsBetweenTwoValues(t *testing.T) {
	values := []float64{0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10}
	percentile := 66.666666666666
	expected := 6.666666666666667 // Since 2/3 of the way between 6 and 7 is 6.666...
	result, err := Percentile(values, percentile)
	if err != nil || math.Abs(result-expected) > 1e-9 {
		t.Errorf("Percentile %v of %v; expected %v, got %v, err: %v", percentile, values, expected, result, err)
	}
}

func TestPercentileBetweenTwoValues1(t *testing.T) {
	values := []float64{0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10}
	percentile := 11.0
	expected := 1.1
	result, err := Percentile(values, percentile)
	if err != nil || math.Abs(result-expected) > 1e-9 {
		t.Errorf("Percentile %v of %v; expected %v, got %v, err: %v", percentile, values, expected, result, err)
	}
}

func TestPercentileBetweenTwoValues2(t *testing.T) {
	values := []float64{0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10}
	percentile := 9.0
	expected := 0.9
	result, err := Percentile(values, percentile)
	if err != nil || math.Abs(result-expected) > 1e-9 {
		t.Errorf("Percentile %v of %v; expected %v, got %v, err: %v", percentile, values, expected, result, err)
	}
}

func TestPercentileUnsortedInput(t *testing.T) {
	values := []float64{5, 1, 4, 2, 3} // Unsorted input
	percentile := 50.0
	expected := 3.0
	result, err := Percentile(values, percentile)
	if err != nil || result != expected {
		t.Errorf("Percentile %v of %v; expected %v, got %v, err: %v", percentile, values, expected, result, err)
	}
}

func TestPercentileUnsortedInputLowPercentile(t *testing.T) {
	values := []float64{10, 6, 7, 3, 2, 9, 8, 1, 4, 5} // Unsorted input
	percentile := 10.0
	expected := 1.9 // Expecting interpolation between 1 and 2
	result, err := Percentile(values, percentile)
	if err != nil || result != expected {
		t.Errorf("Percentile %v of %v; expected %v, got %v, err: %v", percentile, values, expected, result, err)
	}
}

func TestPercentileUnsortedInputHighPercentile(t *testing.T) {
	values := []float64{10, 6, 7, 3, 2, 9, 8, 1, 4, 5} // Unsorted input
	percentile := 90.0
	expected := 9.1 // Expecting interpolation between 9 and 10
	result, err := Percentile(values, percentile)
	if err != nil || result != expected {
		t.Errorf("Percentile %v of %v; expected %v, got %v, err: %v", percentile, values, expected, result, err)
	}
}

func TestFloatPercentileExactValueFromInput(t *testing.T) {
	values := []float64{1.5, 2.5, 3.5, 4.5, 5.5}
	percentile := 50.0 // Exact value from input array should be 3.5
	expected := 3.5
	result, err := FloatPercentile(values, percentile)
	if err != nil || result != expected {
		t.Errorf("FloatPercentile %v of %v; expected %v, got %v, err: %v", percentile, values, expected, result, err)
	}
}

func TestFloatPercentileInterpolatedValue(t *testing.T) {
	values := []float64{1.0, 2.0, 3.0, 4.0, 5.0}
	percentile := 87.5 // Interpolated value between 4.0 and 5.0
	expected := 4.5
	result, err := FloatPercentile(values, percentile)
	if err != nil || result != expected {
		t.Errorf("FloatPercentile %v of %v; expected %v, got %v, err: %v", percentile, values, expected, result, err)
	}
}

func TestFloatPercentileUnsortedInputExactValue(t *testing.T) {
	values := []float64{5.5, 1.5, 4.5, 2.5, 3.5} // Unsorted input
	percentile := 50.0
	expected := 3.5
	result, err := FloatPercentile(values, percentile)
	if err != nil || result != expected {
		t.Errorf("FloatPercentile %v of %v; expected %v, got %v, err: %v", percentile, values, expected, result, err)
	}
}

func TestFloatPercentileUnsortedInputInterpolatedValue(t *testing.T) {
	values := []float64{10.5, 6.5, 7.5, 3.5, 2.5, 9.5, 8.5, 1.5, 4.5, 5.5}
	percentile := 80.0 // Interpolated value between 4.0 and 5.0
	expected := 8.7
	result, err := FloatPercentile(values, percentile)
	if err != nil || math.Abs(result-expected) > 1e-9 {
		t.Errorf("FloatPercentile %v of %v; expected %v, got %v, err: %v", percentile, values, expected, result, err)
	}
}
