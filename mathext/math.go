package mathext

import "gogs.mikescher.com/BlackForestBytes/goext/langext"

func AvgFloat64(arr []float64) float64 {
	return SumFloat64(arr) / float64(len(arr))
}

func SumFloat64(arr []float64) float64 {
	sum := 0.0
	for _, v := range arr {
		sum += v
	}
	return sum
}

func Max[T langext.OrderedConstraint](v1 T, v2 T) T {
	if v1 > v2 {
		return v1
	} else {
		return v2
	}
}

func Max3[T langext.OrderedConstraint](v1 T, v2 T, v3 T) T {
	result := v1
	if v2 > result {
		result = v2
	}
	if v3 > result {
		result = v3
	}
	return result
}

func Max4[T langext.OrderedConstraint](v1 T, v2 T, v3 T, v4 T) T {
	result := v1
	if v2 > result {
		result = v2
	}
	if v3 > result {
		result = v3
	}
	if v4 > result {
		result = v4
	}
	return result
}

func Min[T langext.OrderedConstraint](v1 T, v2 T) T {
	if v1 < v2 {
		return v1
	} else {
		return v2
	}
}

func Min3[T langext.OrderedConstraint](v1 T, v2 T, v3 T) T {
	result := v1
	if v2 < result {
		result = v2
	}
	if v3 < result {
		result = v3
	}
	return result
}

func Min4[T langext.OrderedConstraint](v1 T, v2 T, v3 T, v4 T) T {
	result := v1
	if v2 < result {
		result = v2
	}
	if v3 < result {
		result = v3
	}
	if v4 < result {
		result = v4
	}
	return result
}

func Abs[T langext.NumberConstraint](v T) T {
	if v < 0 {
		return -v
	} else {
		return v
	}
}
