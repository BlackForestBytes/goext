package mathext

import "math"

func Float64EpsilonEq(v1 float64, v2 float64, eps float64) bool {
	return math.Abs(v1-v2) <= eps
}
