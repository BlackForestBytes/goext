package enums

type Enum interface {
	Valid() bool
	ValuesAny() []any
	ValuesMeta() []EnumMetaValue
	VarName() string
	TypeName() string
	PackageName() string
}

type StringEnum interface {
	Enum
	String() string
}

type DescriptionEnum interface {
	Enum
	Description() string
	DescriptionMeta() EnumDescriptionMetaValue
}

type EnumMetaValue struct {
	VarName     string  `json:"varName"`
	Value       Enum    `json:"value"`
	Description *string `json:"description"`
}

type EnumDescriptionMetaValue struct {
	VarName     string `json:"varName"`
	Value       Enum   `json:"value"`
	Description string `json:"description"`
}
