package termext

import (
	"gogs.mikescher.com/BlackForestBytes/goext/tst"
	"math/rand"
	"testing"
)

func init() {
	rand.Seed(0)
}

func TestSupportsColors(t *testing.T) {
	SupportsColors() // should not error
}

func TestColor(t *testing.T) {
	tst.AssertEqual(t, Red("test"), "\033[31mtest\u001B[0m")
	tst.AssertEqual(t, Green("test"), "\033[32mtest\u001B[0m")
	tst.AssertEqual(t, Yellow("test"), "\033[33mtest\u001B[0m")
	tst.AssertEqual(t, Blue("test"), "\033[34mtest\u001B[0m")
	tst.AssertEqual(t, Purple("test"), "\033[35mtest\u001B[0m")
	tst.AssertEqual(t, Cyan("test"), "\033[36mtest\u001B[0m")
	tst.AssertEqual(t, Gray("test"), "\033[37mtest\u001B[0m")
	tst.AssertEqual(t, White("test"), "\033[97mtest\u001B[0m")

	tst.AssertEqual(t, CleanString(Red("test")), "test")
	tst.AssertEqual(t, CleanString(Green("test")), "test")
	tst.AssertEqual(t, CleanString(Yellow("test")), "test")
	tst.AssertEqual(t, CleanString(Blue("test")), "test")
	tst.AssertEqual(t, CleanString(Purple("test")), "test")
	tst.AssertEqual(t, CleanString(Cyan("test")), "test")
	tst.AssertEqual(t, CleanString(Gray("test")), "test")
	tst.AssertEqual(t, CleanString(White("test")), "test")
}
