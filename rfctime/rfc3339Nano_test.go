package rfctime

import (
	"encoding/json"
	"gogs.mikescher.com/BlackForestBytes/goext/timeext"
	"gogs.mikescher.com/BlackForestBytes/goext/tst"
	"testing"
	"time"
)

func TestRoundtrip(t *testing.T) {

	type Wrap struct {
		Value RFC3339NanoTime `json:"v"`
	}

	val1 := NewRFC3339Nano(time.Unix(0, 1675951556820915171).In(timeext.TimezoneBerlin))
	w1 := Wrap{val1}

	jstr1, err := json.Marshal(w1)
	if err != nil {
		panic(err)
	}

	if string(jstr1) != "{\"v\":\"2023-02-09T15:05:56.820915171+01:00\"}" {
		t.Errorf(string(jstr1))
		t.Errorf("repr differs")
	}

	w2 := Wrap{}

	err = json.Unmarshal(jstr1, &w2)
	if err != nil {
		panic(err)
	}

	jstr2, err := json.Marshal(w2)
	if err != nil {
		panic(err)
	}

	tst.AssertEqual(t, string(jstr1), string(jstr2))

	if !w1.Value.Equal(&w2.Value) {
		t.Errorf("time differs")
	}

}
