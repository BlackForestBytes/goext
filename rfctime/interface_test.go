package rfctime

import (
	"gogs.mikescher.com/BlackForestBytes/goext/tst"
	"testing"
	"time"
)

func TestAnyTimeInterface(t *testing.T) {

	var v AnyTime

	v = NowRFC3339Nano()
	tst.AssertEqual(t, v.String(), v.String())

	v = NowRFC3339()
	tst.AssertEqual(t, v.String(), v.String())

	v = NowUnix()
	tst.AssertEqual(t, v.String(), v.String())

	v = NowUnixMilli()
	tst.AssertEqual(t, v.String(), v.String())

	v = NowUnixNano()
	tst.AssertEqual(t, v.String(), v.String())

	v = time.Now()
	tst.AssertEqual(t, v.String(), v.String())

}

func TestRFCTimeInterface(t *testing.T) {
	var v RFCTime

	v = NowRFC3339Nano()
	tst.AssertEqual(t, v.String(), v.String())

	v = NowRFC3339()
	tst.AssertEqual(t, v.String(), v.String())

	v = NowUnix()
	tst.AssertEqual(t, v.String(), v.String())

	v = NowUnixMilli()
	tst.AssertEqual(t, v.String(), v.String())

	v = NowUnixNano()
	tst.AssertEqual(t, v.String(), v.String())

}
