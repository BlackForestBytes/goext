package rfctime

import (
	"encoding/json"
	"errors"
	"fmt"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/bsoncodec"
	"go.mongodb.org/mongo-driver/bson/bsonrw"
	"go.mongodb.org/mongo-driver/bson/bsontype"
	"gogs.mikescher.com/BlackForestBytes/goext/langext"
	"reflect"
	"time"
)

type RFC3339NanoTime time.Time

func (t RFC3339NanoTime) Time() time.Time {
	return time.Time(t)
}

func (t RFC3339NanoTime) MarshalBinary() ([]byte, error) {
	return (time.Time)(t).MarshalBinary()
}

func (t *RFC3339NanoTime) UnmarshalBinary(data []byte) error {
	return (*time.Time)(t).UnmarshalBinary(data)
}

func (t RFC3339NanoTime) GobEncode() ([]byte, error) {
	return (time.Time)(t).GobEncode()
}

func (t *RFC3339NanoTime) GobDecode(data []byte) error {
	return (*time.Time)(t).GobDecode(data)
}

func (t *RFC3339NanoTime) UnmarshalJSON(data []byte) error {
	str := ""
	if err := json.Unmarshal(data, &str); err != nil {
		return err
	}
	t0, err := time.Parse(t.FormatStr(), str)
	if err != nil {
		return err
	}
	*t = RFC3339NanoTime(t0)
	return nil
}

func (t RFC3339NanoTime) MarshalJSON() ([]byte, error) {
	str := t.Time().Format(t.FormatStr())
	return json.Marshal(str)
}

func (t RFC3339NanoTime) MarshalText() ([]byte, error) {
	b := make([]byte, 0, len(t.FormatStr()))
	return t.Time().AppendFormat(b, t.FormatStr()), nil
}

func (t *RFC3339NanoTime) UnmarshalText(data []byte) error {
	var err error
	v, err := time.Parse(t.FormatStr(), string(data))
	if err != nil {
		return err
	}
	tt := RFC3339NanoTime(v)
	*t = tt
	return nil
}

func (t *RFC3339NanoTime) UnmarshalBSONValue(bt bsontype.Type, data []byte) error {
	if bt == bson.TypeNull {
		// we can't set nil in UnmarshalBSONValue (so we use default(struct))
		// Use mongoext.CreateGoExtBsonRegistry if you need to unmarsh pointer values
		// https://stackoverflow.com/questions/75167597
		// https://jira.mongodb.org/browse/GODRIVER-2252
		*t = RFC3339NanoTime{}
		return nil
	}
	if bt != bson.TypeDateTime {
		return errors.New(fmt.Sprintf("cannot unmarshal %v into RFC3339NanoTime", bt))
	}
	var tt time.Time
	err := bson.RawValue{Type: bt, Value: data}.Unmarshal(&tt)
	if err != nil {
		return err
	}
	*t = RFC3339NanoTime(tt)
	return nil
}

func (t RFC3339NanoTime) MarshalBSONValue() (bsontype.Type, []byte, error) {
	return bson.MarshalValue(time.Time(t))
}

func (t RFC3339NanoTime) DecodeValue(dc bsoncodec.DecodeContext, vr bsonrw.ValueReader, val reflect.Value) error {
	if val.Kind() == reflect.Ptr && val.IsNil() {
		if !val.CanSet() {
			return errors.New("ValueUnmarshalerDecodeValue")
		}
		val.Set(reflect.New(val.Type().Elem()))
	}

	tp, src, err := bsonrw.Copier{}.CopyValueToBytes(vr)
	if err != nil {
		return err
	}

	if val.Kind() == reflect.Ptr && len(src) == 0 {
		val.Set(reflect.Zero(val.Type()))
		return nil
	}

	err = t.UnmarshalBSONValue(tp, src)
	if err != nil {
		return err
	}

	if val.Kind() == reflect.Ptr {
		val.Set(reflect.ValueOf(&t))
	} else {
		val.Set(reflect.ValueOf(t))
	}

	return nil
}

func (t RFC3339NanoTime) Serialize() string {
	return t.Time().Format(t.FormatStr())
}

func (t RFC3339NanoTime) FormatStr() string {
	return time.RFC3339Nano
}

func (t RFC3339NanoTime) After(u AnyTime) bool {
	return t.Time().After(tt(u))
}

func (t RFC3339NanoTime) Before(u AnyTime) bool {
	return t.Time().Before(tt(u))
}

func (t RFC3339NanoTime) Equal(u AnyTime) bool {
	return t.Time().Equal(tt(u))
}

func (t RFC3339NanoTime) IsZero() bool {
	return t.Time().IsZero()
}

func (t RFC3339NanoTime) Date() (year int, month time.Month, day int) {
	return t.Time().Date()
}

func (t RFC3339NanoTime) Year() int {
	return t.Time().Year()
}

func (t RFC3339NanoTime) Month() time.Month {
	return t.Time().Month()
}

func (t RFC3339NanoTime) Day() int {
	return t.Time().Day()
}

func (t RFC3339NanoTime) Weekday() time.Weekday {
	return t.Time().Weekday()
}

func (t RFC3339NanoTime) ISOWeek() (year, week int) {
	return t.Time().ISOWeek()
}

func (t RFC3339NanoTime) Clock() (hour, min, sec int) {
	return t.Time().Clock()
}

func (t RFC3339NanoTime) Hour() int {
	return t.Time().Hour()
}

func (t RFC3339NanoTime) Minute() int {
	return t.Time().Minute()
}

func (t RFC3339NanoTime) Second() int {
	return t.Time().Second()
}

func (t RFC3339NanoTime) Nanosecond() int {
	return t.Time().Nanosecond()
}

func (t RFC3339NanoTime) YearDay() int {
	return t.Time().YearDay()
}

func (t RFC3339NanoTime) Add(d time.Duration) RFC3339NanoTime {
	return RFC3339NanoTime(t.Time().Add(d))
}

func (t RFC3339NanoTime) Sub(u AnyTime) time.Duration {
	return t.Time().Sub(tt(u))
}

func (t RFC3339NanoTime) AddDate(years int, months int, days int) RFC3339NanoTime {
	return RFC3339NanoTime(t.Time().AddDate(years, months, days))
}

func (t RFC3339NanoTime) Unix() int64 {
	return t.Time().Unix()
}

func (t RFC3339NanoTime) UnixMilli() int64 {
	return t.Time().UnixMilli()
}

func (t RFC3339NanoTime) UnixMicro() int64 {
	return t.Time().UnixMicro()
}

func (t RFC3339NanoTime) UnixNano() int64 {
	return t.Time().UnixNano()
}

func (t RFC3339NanoTime) Format(layout string) string {
	return t.Time().Format(layout)
}

func (t RFC3339NanoTime) GoString() string {
	return t.Time().GoString()
}

func (t RFC3339NanoTime) String() string {
	return t.Time().String()
}

func (t RFC3339NanoTime) Location() *time.Location {
	return t.Time().Location()
}

func NewRFC3339Nano(t time.Time) RFC3339NanoTime {
	return RFC3339NanoTime(t)
}

func NewRFC3339NanoPtr(t *time.Time) *RFC3339NanoTime {
	if t == nil {
		return nil
	}
	return langext.Ptr(RFC3339NanoTime(*t))
}

func NowRFC3339Nano() RFC3339NanoTime {
	return RFC3339NanoTime(time.Now())
}
