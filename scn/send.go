package scn

import (
	"bytes"
	"context"
	"gogs.mikescher.com/BlackForestBytes/goext/exerr"
	json "gogs.mikescher.com/BlackForestBytes/goext/gojson"
	"gogs.mikescher.com/BlackForestBytes/goext/langext"
	"io"
	"net/http"
	"time"
)

var (
	ErrAuthFailed        = exerr.NewType("GOEXT_SCN_AUTHFAILED", nil)
	ErrQuota             = exerr.NewType("GOEXT_SCN_QUOTAEXCEEDED", nil)
	ErrBadRequest        = exerr.NewType("GOEXT_SCN_BADREQUEST", nil)
	ErrInternalServerErr = exerr.NewType("GOEXT_SCN_INTERNALSERVER", nil)
	ErrOther             = exerr.NewType("GOEXT_SCN_OTHER", nil)
)

type MessageResponse struct {
	ErrHighlight int    `json:"errhighlight"`
	Error        int    `json:"error"`
	IsPro        bool   `json:"is_pro"`
	Message      string `json:"message"`
	Messagecount int    `json:"messagecount"`
	Quota        int    `json:"quota"`
	QuotaMax     int    `json:"quota_max"`
	SCNMessageID string `json:"scn_msg_id"`
	Success      bool   `json:"success"`
	SuppressSend bool   `json:"suppress_send"`
}

type MessageErrResponse struct {
	Errhighlight int    `json:"errhighlight"`
	Error        int    `json:"error"`
	Message      string `json:"message"`
	Success      bool   `json:"success"`
}

type MessageBuilder struct {
	conn       *Connection
	title      string
	content    *string
	channel    *string
	time       *time.Time
	sendername *string
	priority   *int
}

func (c *Connection) Message(title string) *MessageBuilder {
	return &MessageBuilder{conn: c, title: title}
}

func (c *MessageBuilder) Channel(channel string) *MessageBuilder {
	c.channel = &channel
	return c
}

func (c *MessageBuilder) Content(content string) *MessageBuilder {
	c.content = &content
	return c
}

func (c *MessageBuilder) Time(t time.Time) *MessageBuilder {
	c.time = &t
	return c
}

func (c *MessageBuilder) SenderName(sn string) *MessageBuilder {
	c.sendername = &sn
	return c
}

func (c *MessageBuilder) Priority(p int) *MessageBuilder {
	c.priority = &p
	return c
}

func (c *MessageBuilder) Send(ctx context.Context) (MessageResponse, error) {
	client := http.Client{Timeout: 5 * time.Second}

	body := langext.H{}

	body["user_id"] = c.conn.uid
	body["key"] = c.conn.token

	if c.channel != nil {
		body["channel"] = *c.channel
	}

	body["title"] = c.title

	if c.content != nil {
		body["content"] = *c.content
	}

	if c.sendername != nil {
		body["content"] = *c.sendername
	}

	if c.time != nil {
		body["timestamp"] = c.time.Unix()
	}

	if c.priority != nil {
		body["priority"] = *c.priority
	}

	body["msg_id"] = langext.MustHexUUID()

	rawbody, err := json.Marshal(body)
	if err != nil {
		return MessageResponse{}, err
	}

	req, err := http.NewRequestWithContext(ctx, "POST", "https://simplecloudnotifier.de/", bytes.NewBuffer(rawbody))
	if err != nil {
		return MessageResponse{}, err
	}
	req.Header.Set("Content-Type", "application/json")

	resp, err := client.Do(req)
	if err != nil {
		return MessageResponse{}, err
	}

	defer func() { _ = resp.Body.Close() }()

	if resp.StatusCode >= 200 && resp.StatusCode < 300 {

		raw, err := io.ReadAll(resp.Body)
		if err != nil {
			return MessageResponse{}, err
		}

		var mr MessageResponse
		err = json.Unmarshal(raw, &mr)
		if err != nil {
			return MessageResponse{}, err
		}

		return mr, nil
	} else {
		errMessage := resp.Status

		if raw, err := io.ReadAll(resp.Body); err == nil {
			var mr MessageErrResponse
			if err = json.Unmarshal(raw, &mr); err == nil {
				errMessage = mr.Message
			}
		}

		if resp.StatusCode == 400 {
			return MessageResponse{}, exerr.New(ErrBadRequest, errMessage).Build()
		}
		if resp.StatusCode == 401 {
			return MessageResponse{}, exerr.New(ErrAuthFailed, errMessage).Build()
		}
		if resp.StatusCode == 403 {
			return MessageResponse{}, exerr.New(ErrQuota, errMessage).Build()
		}
		if resp.StatusCode == 500 {
			return MessageResponse{}, exerr.New(ErrInternalServerErr, errMessage).Build()
		}

		return MessageResponse{}, exerr.New(ErrOther, errMessage).Build()
	}

}
