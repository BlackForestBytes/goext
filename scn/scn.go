package scn

type Connection struct {
	uid   string
	token string
}

func New(userid string, token string) *Connection {
	return &Connection{
		uid:   userid,
		token: token,
	}
}
