BFB goext library
=================

A collection of general & useful library methods

This should not have any heavy dependencies (gin, mongo, etc) and add missing basic language features...

Potentially needs `export GOPRIVATE="gogs.mikescher.com"`


## Packages:

| Name        | Maintainer | Description                                                                                                   |
|-------------|------------|---------------------------------------------------------------------------------------------------------------|
| langext     | Mike       | General uttility/helper functions, (everything thats missing from go standard library)                        |
| mathext     | Mike       | Utility/Helper functions for math                                                                             |
| cryptext    | Mike       | Utility/Helper functions for encryption                                                                       |
| syncext     | Mike       | Utility/Helper funtions for multi-threading / mutex / channels                                                |
| dataext     | Mike       | Various useful data structures                                                                                |
| zipext      | Mike       | Utility for zip/gzip/tar etc                                                                                  |
| reflectext  | Mike       | Utility for golang reflection                                                                                 |
| fsext       | Mike       | Utility for filesytem access                                                                                  |
| ctxext      | Mike       | Utility for context.Context                                                                                   |
|             |            |                                                                                                               |
| mongoext    | Mike       | Utility/Helper functions for mongodb (kinda abandoned)                                                        |
| cursortoken | Mike       | MongoDB cursortoken implementation                                                                            |
| pagination  | Mike       | Pagination implementation                                                                                     |
|             |            |                                                                                                               |
| totpext     | Mike       | Implementation of TOTP (2-Factor-Auth)                                                                        |
| termext     | Mike       | Utilities for terminals (mostly color output)                                                                 |
| confext     | Mike       | Parses environment configuration into structs                                                                 |
| cmdext      | Mike       | Runner for external commands/processes                                                                        |
|             |            |                                                                                                               |
| sq          | Mike       | Utility functions for sql based databases (primarily sqlite)                                                  |
| tst         | Mike       | Utility functions for unit tests                                                                              |
|             |            |                                                                                                               |
| rfctime     | Mike       | Classes for time seriallization, with different marshallign method for mongo and json                         |
| gojson      | Mike       | Same interface for marshalling/unmarshalling as go/json, except with proper serialization of null arrays/maps |
|             |            |                                                                                                               |
| bfcodegen   | Mike       | Various codegen tools (run via go generate)                                                                   |
|             |            |                                                                                                               |
| rext        | Mike       | Regex Wrapper, wraps regexp with a better interface                                                           |
| wmo         | Mike       | Mongo Wrapper, wraps mongodb with a better interface                                                          |
|             |            |                                                                                                               |
| scn         | Mike       | SimpleCloudNotifier                                                                                           |
|             |            |                                                                                                               |



## Usage:

### exerr

 - see **mongoext/builder.go** for full info

Short summary:
 - An better error package with metadata, listener, api-output and error-traces
 - Initialize with `exerr.Init()`
 - *Never* return `err` direct, always use exerr.Wrap(err, "...") - add metadata where applicable
 - at the end either Print(), Fatal() or Output() your error (print = stdout, fatal = panic, output = json API response)
 - You can add listeners with exerr.RegisterListener(), and save the full errors to a db or smth

### wmo

 - A typed wrapper around the official mongo-go-driver
 - Use `wmo.W[...](...)` to wrap the collections and type-ify them
 - The new collections have all the usual methods, but types
 - Also they have List() and Paginate() methods for paginated listings (witehr with a cursortoken or page/limit)
 - Register additional hooks with `WithDecodeFunc`, `WithUnmarshalHook`, `WithMarshalHook`, `WithModifyingPipeline`, `WithModifyingPipelineFunc`
 - List(), Paginate(), etc support filter interfaces
   - Rule(s) of thumb: 
     - filter the results in the filter interface
     - sort the results in the sort function of the filter interface
     - add joins ($lookup's) in the `WithModifyingPipelineFunc`/`WithModifyingPipeline`

#### ginext

 - A wrapper around gin-gonic/gin
 - create the gin engine with `ginext.NewEngine`
 - Add routes with `engine.Routes()...`
   - `.Use(..)` adds a middleware
   - `.Group(..)` adds a group
   - `.Get().Handle(..)` adds a handler
 - Handler return values (in contract to ginext) - values implement the `ginext.HTTPResponse` interface
 - Every handler starts with something like:
```go 
func (handler Handler) CommunityMetricsValues(pctx ginext.PreContext) ginext.HTTPResponse {
    type communityURI struct {
        Version     string             `uri:"version"`
        CommunityID models.CommunityID `uri:"cid"`
    }
    type body struct {
        UserID  models.UserID  `json:"userID"`
        EventID models.EventID `json:"eventID"`
    }

    var u uri
    var b body
    ctx, gctx, httpErr := pctx.URI(&u).Body(&b).Start() // can have more unmarshaller, like header, form, etc
    if httpErr != nil {
    	return *httpErr
    }
    defer ctx.Cancel()

    // do stuff
}
```

#### sq

 - TODO (like mongoext for sqlite/sql databases)