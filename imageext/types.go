package imageext

import "image"

type Rectangle struct {
	X float64
	Y float64
	W float64
	H float64
}

type PercentageRectangle struct {
	X float64 // [0..1]
	Y float64 // [0..1]
	W float64 // [0..1]
	H float64 // [0..1]
}

func (r PercentageRectangle) Of(ref Rectangle) Rectangle {
	return Rectangle{
		X: ref.X + r.X*ref.W,
		Y: ref.Y + r.Y*ref.H,
		W: r.W * ref.W,
		H: r.H * ref.H,
	}
}

func calcRelativeRect(inner image.Rectangle, outer image.Rectangle) PercentageRectangle {
	return PercentageRectangle{
		X: float64(inner.Min.X-outer.Min.X) / float64(outer.Dx()),
		Y: float64(inner.Min.Y-outer.Min.Y) / float64(outer.Dy()),
		W: float64(inner.Dx()) / float64(outer.Dx()),
		H: float64(inner.Dy()) / float64(outer.Dy()),
	}
}
