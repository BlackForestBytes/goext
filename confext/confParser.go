package confext

import (
	"errors"
	"fmt"
	"gogs.mikescher.com/BlackForestBytes/goext/timeext"
	"math/bits"
	"os"
	"reflect"
	"strconv"
	"strings"
	"time"
)

// ApplyEnvOverrides overrides field values from environment variables
//
// fields must be tagged with `env:"env_key"`
//
// only works on exported fields
//
// fields without an env tag are ignored
// fields with an `env:"-"` tag are ignore
//
// sub-structs are recursively parsed (if they have an env tag) and the env-variable keys are delimited by the delim parameter
// sub-structs with `env:""` are also parsed, but the delimited is skipped (they are handled as if they were one level higher)
func ApplyEnvOverrides[T any](prefix string, c *T, delim string) error {
	rval := reflect.ValueOf(c).Elem()

	return processEnvOverrides(rval, delim, prefix)
}

func processEnvOverrides(rval reflect.Value, delim string, prefix string) error {
	rtyp := rval.Type()

	for i := 0; i < rtyp.NumField(); i++ {

		rsfield := rtyp.Field(i)
		rvfield := rval.Field(i)

		if !rsfield.IsExported() {
			continue
		}

		envkey, found := rsfield.Tag.Lookup("env")
		if !found || envkey == "-" {
			continue
		}

		if rvfield.Kind() == reflect.Struct && rvfield.Type() != reflect.TypeOf(time.UnixMilli(0)) {

			subPrefix := prefix
			if envkey != "" {
				subPrefix = subPrefix + envkey + delim
			}

			err := processEnvOverrides(rvfield, delim, subPrefix)
			if err != nil {
				return err
			}

			continue
		}

		fullEnvKey := prefix + envkey

		envval, efound := os.LookupEnv(fullEnvKey)
		if !efound {
			continue
		}

		if rvfield.Type().Kind() == reflect.Pointer {

			newval, err := parseEnvToValue(envval, fullEnvKey, rvfield.Type().Elem())
			if err != nil {
				return err
			}

			// converts reflect.Value to pointer
			ptrval := reflect.New(rvfield.Type().Elem())
			ptrval.Elem().Set(newval)

			rvfield.Set(ptrval)

			fmt.Printf("[CONF] Overwrite config '%s' with '%s'\n", fullEnvKey, envval)

		} else {

			newval, err := parseEnvToValue(envval, fullEnvKey, rvfield.Type())
			if err != nil {
				return err
			}

			rvfield.Set(newval)
			fmt.Printf("[CONF] Overwrite config '%s' with '%s'\n", fullEnvKey, envval)

		}

	}

	return nil
}

func parseEnvToValue(envval string, fullEnvKey string, rvtype reflect.Type) (reflect.Value, error) {
	if rvtype == reflect.TypeOf("") {

		return reflect.ValueOf(envval), nil

	} else if rvtype == reflect.TypeOf(int(0)) {

		envint, err := strconv.ParseInt(envval, 10, bits.UintSize)
		if err != nil {
			return reflect.Value{}, errors.New(fmt.Sprintf("Failed to parse env-config variable '%s' to int (value := '%s')", fullEnvKey, envval))
		}

		return reflect.ValueOf(int(envint)), nil

	} else if rvtype == reflect.TypeOf(int64(0)) {

		envint, err := strconv.ParseInt(envval, 10, 64)
		if err != nil {
			return reflect.Value{}, errors.New(fmt.Sprintf("Failed to parse env-config variable '%s' to int64 (value := '%s')", fullEnvKey, envval))
		}

		return reflect.ValueOf(int64(envint)), nil

	} else if rvtype == reflect.TypeOf(int32(0)) {

		envint, err := strconv.ParseInt(envval, 10, 32)
		if err != nil {
			return reflect.Value{}, errors.New(fmt.Sprintf("Failed to parse env-config variable '%s' to int32 (value := '%s')", fullEnvKey, envval))
		}

		return reflect.ValueOf(int32(envint)), nil

	} else if rvtype == reflect.TypeOf(int8(0)) {

		envint, err := strconv.ParseInt(envval, 10, 8)
		if err != nil {
			return reflect.Value{}, errors.New(fmt.Sprintf("Failed to parse env-config variable '%s' to int32 (value := '%s')", fullEnvKey, envval))
		}

		return reflect.ValueOf(int8(envint)), nil

	} else if rvtype == reflect.TypeOf(time.Duration(0)) {

		dur, err := timeext.ParseDurationShortString(envval)
		if err != nil {
			return reflect.Value{}, errors.New(fmt.Sprintf("Failed to parse env-config variable '%s' to duration (value := '%s')", fullEnvKey, envval))
		}

		return reflect.ValueOf(dur), nil

	} else if rvtype == reflect.TypeOf(time.UnixMilli(0)) {

		tim, err := time.Parse(time.RFC3339Nano, envval)
		if err != nil {
			return reflect.Value{}, errors.New(fmt.Sprintf("Failed to parse env-config variable '%s' to time.time (value := '%s')", fullEnvKey, envval))
		}

		return reflect.ValueOf(tim), nil

	} else if rvtype.ConvertibleTo(reflect.TypeOf(int(0))) {

		envint, err := strconv.ParseInt(envval, 10, 8)
		if err != nil {
			return reflect.Value{}, errors.New(fmt.Sprintf("Failed to parse env-config variable '%s' to <%s, ,int> (value := '%s')", rvtype.Name(), fullEnvKey, envval))
		}

		envcvl := reflect.ValueOf(envint).Convert(rvtype)

		return envcvl, nil

	} else if rvtype.ConvertibleTo(reflect.TypeOf(false)) {

		if strings.TrimSpace(strings.ToLower(envval)) == "true" {
			return reflect.ValueOf(true).Convert(rvtype), nil
		} else if strings.TrimSpace(strings.ToLower(envval)) == "false" {
			return reflect.ValueOf(false).Convert(rvtype), nil
		} else if strings.TrimSpace(strings.ToLower(envval)) == "1" {
			return reflect.ValueOf(true).Convert(rvtype), nil
		} else if strings.TrimSpace(strings.ToLower(envval)) == "0" {
			return reflect.ValueOf(false).Convert(rvtype), nil
		} else {
			return reflect.Value{}, errors.New(fmt.Sprintf("Failed to parse env-config variable '%s' to <%s, ,bool> (value := '%s')", rvtype.Name(), fullEnvKey, envval))
		}

	} else if rvtype.ConvertibleTo(reflect.TypeOf("")) {

		envcvl := reflect.ValueOf(envval).Convert(rvtype)
		return envcvl, nil

	} else {
		return reflect.Value{}, errors.New(fmt.Sprintf("Unknown kind/type in config: [ %s | %s ]", rvtype.Kind().String(), rvtype.String()))
	}
}
